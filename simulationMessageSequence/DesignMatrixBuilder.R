library(data.table)
library(uuid)
library(futile.logger)
library(openxlsx)
library(Learning)
library(Hmisc)

#' This function returns the Design Matrix required for generating predictions for MSO Model
#'
#'@param learning_home_dir
#'@param mso_build_UID
getDesignMatrixForSimulation <- function(learning_home_dir, mso_build_UID, sim_run_uid, dbhost, dbuser, dbpassword, dbname, port)
{
  stopifnot(dir.exists(learning_home_dir))

  # Read the input parameters and put in the right form
  homedir = learning_home_dir
  BUILD_UID = mso_build_UID
  RUN_UID = sim_run_uid

  scoreDate <- Sys.Date()
  DAYS_CONSIDER_FOR_PRIORVISIT <- 30

  # Source the R script for database connections
  source(sprintf("%s/common/dbConnection/dbConnection.R", homedir))

  # Convert the port to numeric
  port <- as.numeric(port)

  # Create database connections
  dse_connection <- getDBConnection(dbuser, dbpassword, dbhost, dbname, port)
  con_l <- getDBConnectionLearning(dbuser, dbpassword, dbhost, dbname, port)
  con_stage <- getDBConnectionStage(dbuser, dbpassword, dbhost, dbname, port)
  con_cs <- getDBConnectionCS(dbuser, dbpassword, dbhost, dbname, port)

  # Read the configurations from learning.properties
  config <- initializeConfigurationNew(homedir, BUILD_UID)

  # Read product UID
  prods <- getConfigurationValueNew(config, "LE_MS_addPredictorsFromAccountProduct")         # need parameters to replicate design used to build
  NameUID <- getConfigurationValueNew(config, "productUID")
  productUID <- NameUID


  # Source R Script for loadMessageSequence(..)
  source(sprintf("%s/messageSequence/loadMessageSequenceData.R", homedir))

    # Call the loadMessageSequence(..) data
  dta <- loadMessageSequenceData(dse_connection, con_l, con_stage, con_cs, productUID, readDataList=c("interactions","accountProduct","products","emailTopicNames","messages","messageSetMessage","interactionsP","expiredMessages"))
  for(i in 1:length(dta))assign(names(dta)[i],dta[[i]])

  # Check if the data has new line characters which is indicator it should be prepared for old model.
  modelsaveSpreadsheetName <- sprintf('%s/builds/%s/messageSequenceDriver.r_%s.xlsx',homedir,BUILD_UID,BUILD_UID)
  isOldModel <- any(grepl("\n", read.xlsx(modelsaveSpreadsheetName,sheet=2)$namesOri))

  # Get the static design matrix
  AP <- buildStaticDesignMatrix(prods, accountProduct, emailTopicNames, logDropProcess=FALSE)[["AP"]]

  source(sprintf("%s/messageSequence/whiteList.r", homedir))
  # Get the dynamic design matrix
  whiteList <- whiteListNew(dse_connection, con_l, RUN_UID, BUILD_UID, config[["productUID"]], config[["LE_MS_WhiteListType"]], config[["LE_MS_WhiteList"]])$accountId


  source(sprintf("%s/messageSequence/scoreModel.R", homedir))
  # process interactions based on whiteList
  ints <- scoreModel.processInts(whiteList, interactions)
  if(is.null(ints)) {  # woops - nothing to do so return with no result
      flog.warn("No accounts in whitelist shared with product interactions")
      return(list(1,NULL,NULL))
  }

  # add dynamic features, priorVisit and build design matrix
  allModel <- scoreModel.prepareDesignMatrix(AP, ints, interactionsP, scoreDate, DAYS_CONSIDER_FOR_PRIORVISIT)

  return(allModel)
}

#TO-TEST::
# dbhost="127.0.0.1"
# dbuser="pfizerusadmin"
# dbpassword="njTB95MacVDLaMUAnKHdXubOkVPyCU"
# dbname="pfizerusdev"
# port=33066
# learning_home_dir = "/Users/anwar/code/data"
# mso_build_UID = "unit-test-mso-build"
# sim_run_uid = "sdhfsdhfksdfksfc"
#
# allModel <- getDesignMatrixForSimulation(learning_home_dir, mso_build_UID, sim_run_uid,dbhost, dbuser, dbpassword, dbname, port)
