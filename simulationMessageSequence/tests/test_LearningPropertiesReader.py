import unittest
import sys
import os

script_path = os.path.realpath(__file__)
script_dir = os.path.dirname(script_path)
module_dir = os.path.dirname(script_dir)
learning_dir = os.path.dirname(module_dir)
sys.path.append(learning_dir)

from simulationMessageSequence.LearningPropertiesReader import LearningPropertiesReader
from common.unitTest.pyunittestUtils import unitTestParent

class LearningPropertiesReaderTest(unitTestParent):

    def setUp(self):
        self.learning_prop_config = LearningPropertiesReader()
        super().setUp()

class read_learning_propertiesTest(LearningPropertiesReaderTest):

    def setUp(self):
        self.learning_prop_config = LearningPropertiesReader()
        super().setUp()

    def test_empty_file_path(self):
        self.assertRaises(AssertionError, self.learning_prop_config.read_learning_properties, "")

    def test_invalid_file_path(self):
        self.assertRaises(AssertionError, self.learning_prop_config.read_learning_properties, "dfsdf/dfsdf/sdfs")

    def test_valid_properties_file(self):
        basepath =os.path.dirname(__file__)
        learning_prop_file_path = os.path.abspath(os.path.join(basepath, "data", "builds", "unit-test-mso-build", "learning.properties"))

        self.learning_prop_config.read_learning_properties(learning_prop_file_path)

    def reqDBs(self):
        required_dse = [ ]
        required_cs = [ ]
        required_stage = [ ]
        required_learning = [ ]
        required_archive = [ ]
        required = {"dse" : required_dse, "cs" : required_cs, "stage" : required_stage, "learning" : required_learning, "archive" : required_archive}
        return required

class get_propertyTest(LearningPropertiesReaderTest):

    def test_get_properties(self):
        basepath =os.path.dirname(__file__)
        learning_prop_file_path = os.path.abspath(os.path.join(basepath, "data", "builds", "unit-test-mso-build", "learning.properties"))

        self.learning_prop_config.read_learning_properties(learning_prop_file_path)

        self.assertEqual(self.learning_prop_config.get_property("versionUID"), "b606af2b-6e7b-48ec-b014-106b8e96b50d")
        self.assertEqual(self.learning_prop_config.get_property("productUID"), "a00A0000000quNsIAI")
        self.assertEqual(self.learning_prop_config.get_property("configUID"), "00bad2df-bd27-48da-a37c-0e3b41e949e1")
        self.assertEqual(self.learning_prop_config.get_property("LE_MS_includeVisitChannel"), "1")
        self.assertEqual(self.learning_prop_config.get_property("LE_MS_removeMessageSends"), "0")
        self.assertEqual(self.learning_prop_config.get_property("LE_MS_newMessageStrategy"), "Conservative")
        self.assertEqual(self.learning_prop_config.get_property("LE_MS_RFtreeNo"), "100")
        self.assertEqual(self.learning_prop_config.get_property("LE_MS_rescoringType"), "Percentage")
        self.assertEqual(self.learning_prop_config.get_property("buildUID"), "unit-test-mso-build")
        self.assertEqual(self.learning_prop_config.get_property("LE_MS_messageAnalysisUseML"), "RF")
        self.assertEqual(self.learning_prop_config.get_property("LE_MS_flagRescoring"), "TRUE")
        self.assertEqual(self.learning_prop_config.get_property("LE_MS_rescoringLimit"), "2")
        self.assertEqual(self.learning_prop_config.get_property("LE_MS_removeMessageClicks"), "1")
        self.assertEqual(self.learning_prop_config.get_property("channelUID"), "SEND_CHANNEL")
        self.assertEqual(self.learning_prop_config.get_property("LE_MS_removeMessageOpens"), "0")
        self.assertEqual(self.learning_prop_config.get_property("LE_MS_RFtreeDepth"), "10")
        self.assertEqual(self.learning_prop_config.get_property("LE_MS_rescoringPeriod"), "25")
        self.assertEqual(self.learning_prop_config.get_property("LE_MS_messageAnalysisTargetType"), "OPEN")
        self.assertEqual(self.learning_prop_config.get_property("modelType"), "MSO")

    def reqDBs(self):
        required_dse = [ ]
        required_cs = [ ]
        required_stage = [ ]
        required_learning = [ ]
        required_archive = [ ]
        required = {"dse" : required_dse, "cs" : required_cs, "stage" : required_stage, "learning" : required_learning, "archive" : required_archive}
        return required
