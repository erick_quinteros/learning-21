#
#
# aktana-learning model building code for Aktana Learning Engines.
#
# description: Loading data for TTE
#
# created by : marc.cohen@aktana.com
# updated by : wendong.zhu@aktana.com
#
# updated on : 2019-04-01
#
# Copyright AKTANA (c) 2019.
#
##########################################################

messageTimingData <- function(sc, con, dbname_cs, dbname_learning, tteParams)
{
    library(data.table)
    library(RMySQL)
    library(futile.logger)
    library(sparklyr)
    library(dplyr)
    library(arrow)
    
    flog.info("Entered loadMessageTimingData")
    dictraw <- readV3dbFilter(con, productUID="All", prods=tteParams[["prods"]])
    
    for(i in 1:length(dictraw))assign(names(dictraw)[i],dictraw[[i]]) # assign variable name with corresponding data

    rm(dictraw)
    gc(); gc(); gc()

    EventTypes <- tteParams[["EventTypes"]]
    numPartitions <- tteParams[["NUM_PARTITIONS"]]
    predictRunDate <- tteParams[["predictRunDate"]]
    LOOKBACK <- tteParams[["LOOKBACK"]]

    events <- events %>% filter(eventTypeName %in% EventTypes) %>% select(accountId,eventDate,eventTypeName,physicalMessageUID,productName)

    events <- dplyr::rename(events, type = eventTypeName, date = eventDate)

    #events[,date:=as.Date(date)]
    #events <- events %>% mutate(date = as.Date(date))
    
    events <- sdf_copy_to(sc, events, "events", overwrite=TRUE, memory=FALSE, repartition=numPartitions)
    events <- events %>% dplyr::mutate(date = to_date(date))
    
    flog.info("caching events. number of partitions is %s", sdf_num_partitions(events))
    events <- sdf_register(events, "events")
    tbl_cache(sc, "events")
    events <- tbl(sc, "events")    
    flog.info("done caching events.")

    if (tteParams[["useDeliveredEmail"]]) {
        # filter data based on "Sent_Email_vod__c_arc" to get delivered emails only for design matrix
        # get delivered status information from DB
        # db_learning <- sprintf("%s_learning", dbname)

        deliveredInt <- dbGetQuery(con, sprintf("SELECT Id FROM %s.Sent_Email_vod__c_arc WHERE Status_vod__c = 'Delivered_vod';", dbname_learning))
        deliveredInt <- deliveredInt[1:dim(deliveredInt)[1],1] # convert dataframe to vector
        
        # filter the message delivered
        flog.info('filtering interaction data based on delivered_or_not information from Sent_Email_vod__c_arc')
        interactions <- interactions[externalId %in% deliveredInt | interactionTypeName == 'VISIT' | interactionTypeName == 'WEB_INTERACTION']
        interactions$externalId <- NULL
    }
    
    # filter out letters from interactions
    
    call2sample <- data.table(dbGetQuery(con, sprintf("select Id from %s.Call2_Sample_vod__c", dbname_cs)))
    #url <- sprintf("jdbc:mysql://%s:%s/%s?serverTimezone=UTC", dbhost, port, dbname_cs)

    #call2sample <- spark_read_jdbc(sc, "call2sample", options = list(
    #                    url = url, user = dbuser, password = dbpassword,
    #                    driver='com.mysql.jdbc.Driver',
    #                    dbtable = "(SELECT Id from Call2_Sample_vod__c) as my_query",
    #                    numPartitions = as.character(numPartitions)))
    
    # filter interactions data using LOOKBACK
    interactions <- interactions[date >= (predictRunDate - LOOKBACK)]
        
    interactions <- interactions[!externalId %in% call2sample$Id]
    #ids <- pull(call2sample, Id)
    #interactions <- interactions %>% filter(!externalId %in% ids)
    
    interactions <- interactions[, list(accountId,physicalMessageUID,productInteractionTypeName,date,productName)]
    #interactions <- interactions %>% select(accountId,physicalMessageUID,productInteractionTypeName,date,productName)
      
    setnames(interactions,c("productInteractionTypeName"),c("type"))
    #interactions <- dplyr::rename(interactions, type = productInteractionTypeName)

    interactions[, date := as.character(date)]

    flog.info("start sdf_copy_to on interactions.")
    interactions <- sdf_copy_to(sc, interactions, "interactions", overwrite=TRUE, memory=FALSE, repartition=480)
    interactions <- interactions %>% dplyr::mutate(date = to_date(date))
      
    flog.info("caching interactions. number of partitions is %s", sdf_num_partitions(interactions))
    interactions.t <- sdf_register(interactions, "interactions_before_bind_row")
    tbl_cache(sc, "interactions_before_bind_row")
    interactions.t <- tbl(sc, "interactions_before_bind_row")

    interactions <- sdf_bind_rows(interactions, events)

    products <- sdf_copy_to(sc, products, "products", overwrite=TRUE, memory=FALSE)
    
    accounts[,c("externalId","createdAt","updatedAt","facilityId","accountName","isDeleted"):=NULL]
    accountProduct[,c("createdAt","updatedAt"):=NULL]
    
    names(accounts) <- gsub("-", "_", names(accounts))  # remove -'s from names
    names(accounts) <- gsub('[\n|\r]', '', names(accounts))

    names(accountProduct) <- gsub("-", "_", names(accountProduct))  # remove -'s from names
    names(accountProduct) <- gsub('[\n|\r]', '', names(accountProduct)) 

    # remove \n, spark cannot have \n in column name
    colClass <- sapply(accounts, class)
    charColsAccounts <- names(colClass[colClass=="character"])
    accounts <- accounts[,(charColsAccounts):=lapply(.SD, function(x) {return(gsub('[\n|\r]','',x))}),.SDcols=charColsAccounts]  # spark dataframe cannot have \n in column name

    colClass <- sapply(accountProduct, class)
    charColsAccountProduct <- names(colClass[colClass=="character"])
    accountProduct <- accountProduct[,(charColsAccountProduct):=lapply(.SD, function(x) {return(gsub('\n','',x))}),.SDcols=charColsAccountProduct]  # spark dataframe cannot have \n in column name

    # find out charCols for later use before convert to spark
    charCols <- c(charColsAccounts,charColsAccountProduct)
    charCols <- charCols[charCols!="productName"]

    # convert to spark data frame
    accounts <- sdf_copy_to(sc, accounts, "accounts", overwrite=TRUE, memory=FALSE, repartition=numPartitions)

    flog.info("caching accounts")
    accounts <- sdf_register(accounts, "accounts_after_loadData")
    tbl_cache(sc, "accounts_after_loadData")
    accounts <- tbl(sc, "accounts_after_loadData")

    accountProduct <- sdf_copy_to(sc, accountProduct, "accountProduct", overwrite=TRUE, memory=FALSE, repartition=numPartitions)

    flog.info("caching accountProduct")
    accountProduct <- sdf_register(accountProduct, "accountProduct_after_loadData")
    tbl_cache(sc, "accountProduct_after_loadData")
    accountProduct <- tbl(sc, "accountProduct_after_loadData")

    pname.list <- pull(products, productName)
    
    pname <- pname.list[1]
    tA <- dplyr::mutate(accounts, productName = pname)
    
    for(i in 2:sdf_dim(products)[1])  
    {
        #accounts$productName <- products[i]$productName
        pname <- pname.list[i]
        accounts <- dplyr::mutate(accounts, productName = pname)
        
        #tA <- rbind(tA,accounts)
        tA <- sdf_bind_rows(tA, accounts)
    }
    
    #accountProduct <- merge(accountProduct,tA,by=c("accountId","productName"),all.x=TRUE,allow.cartesian=TRUE)
    accountProduct <- accountProduct %>% left_join(tA, by=c("accountId","productName"))
    
    flog.info("Remove bad characters")
####### remove bad characters from the accountProduct metrics
    if (!is.null(charCols)) {
        accountProduct <- accountProduct %>% mutate_at(.vars=charCols, funs(regexp_replace(.,'<|>|,|\\\\.| ','_')))
    }

    rm(call2sample)
    rm(events)
    rm(tA)
    gc(); gc(); gc()
    
    flog.info("Return from loadMessageTimingData")
    return(list(interactions,accountProduct,products,accounts))
}
