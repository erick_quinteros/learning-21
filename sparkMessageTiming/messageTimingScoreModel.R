##########################################################
##
##
## aktana-learning Install Aktana Learning Engines.
##
## description: Driver Code
## 1. 
##
## created by : marc.cohen@aktana.com
##
## created on : 2015-11-03
##
## Copyright AKTANA (c) 2015.
##
##
##########################################################

# TTE methods:
#   A - Covariates + Intervals + DoW (full model for DSE integration)
#   B - Covariates + Intervals (for reporting)
#   C - Covariates + DoW (for reporting)
METHODS <- c("A", "B", "C")  

messageTimingScoreModel <- function(sc, con, con_l, runSettings, tteParams, whiteListDB)
{
    library(h2o)
    library(lattice)
    library(openxlsx)
    library(data.table)
    library(Hmisc)
    library(Learning)
    library(sparkLearning)
    library(sparklyr)
    library(dplyr)
    library(arrow)
    
    runStamp <- tteParams[["runStamp"]]
    ProductInteractionTypes <- tteParams[["ProductInteractionTypes"]]
    VAL_NO_DATA <- tteParams[["VAL_NO_DATA"]]

    runDir <- runSettings[["runDir"]]

    for (m in METHODS) {
        fle <- sprintf("%s/models_%s_%s",runDir, m, runStamp)                  # find the model directory
        if(!dir.exists(fle))
        {
            flog.warn("Model Directory not found: %s",fle);
            CATALOGENTY <<- sprintf("Model directory not found %s ---- %s",fle,CATALOGENTRY)
            return(list(1,NULL))
        }
    }

    fle <- sprintf("%s/messageTimingDriver.r_%s.xlsx",runDir,runStamp)
    if(!file.exists(fle))
    {
        flog.warn("XLSX file not found: %s",fle);
        CATALOGENTY <<- sprintf("XLSX file not found %s ---- %s",fle,CATALOGENTRY)
        return(list(1,NULL))
    }

    # retrieve tte parameters
    isReportOnly <- tteParams[["isReportOnly"]]
    LOOKBACK <- tteParams[["LOOKBACK"]]
    LOOKBACK_MAX <- tteParams[["LOOKBACK_MAX"]]
    predictAhead <- tteParams[["predictAhead"]]
    EventTypes <- tteParams[["EventTypes"]]
    predictRunDate <- tteParams[["predictRunDate"]]
    channels <- tteParams[["channels"]]

    prods <- tteParams[["prods"]]
    if(length(prods)==0)prods <- ""  # this is a kludge to fix defect in getConfigurationValue
    if(prods=="") { prods <- "accountId" } else { prods <- c("accountId",prods) }
    
    pNameUID <- tteParams[["pNameUID"]]
    pName <- products %>% filter(externalId==pNameUID) %>% pull(productName)

    sheetname.A <- sprintf("%s_A_reference", pName)
    models.A <- data.table(read.xlsx(fle,sheet=sheetname.A))  # for predictions

    sheetname.B <- sprintf("%s_B_reference", pName)
    models.B <- data.table(read.xlsx(fle,sheet=sheetname.B))  # for reporting

    sheetname.C <- sprintf("%s_C_reference", pName)
    models.C <- data.table(read.xlsx(fle,sheet=sheetname.C))  # for DoW output

    flog.info("Build static design matrix ...")

    results <- buildStaticFeatures(prods, accountProduct)
    
    AP <- results[["AP"]]
    
##### clean up memory
    rm(accountProduct,envir=parent.frame())                       # delete the accountProduct table since it's big to free up some memory
    rm(accountProduct,envir=globalenv())
    gc()

    flog.info("Build Design Matrix")

#### read dynamic model
    dynamic.model <- fread(sprintf("%s/dynamic.model.csv", runDir))
    
    dynamic.model[, date := as.Date(date)]

    # pick the last row in each accountId group
    dm <- dynamic.model[, .SD[.N], by=accountId][, .(accountId, numS, numV, pre2S, pre2V, preT)]
    dm <- sdf_copy_to(sc, dm, "dm", overwrite=TRUE, memory=FALSE)
    
#### next prepare to build design for dynamic predictors

    whiteList <- whiteListDB$accountId       # contains the accounts in the whitelist
    if(length(whiteList)==0){
        whiteList <- unique(pull(interactions, accountId))
    } 
    
    ints <- interactions %>% filter(accountId %in% whiteList)
    dm <- dm %>% filter(accountId %in% whiteList)
    
    allTypes <- c(ProductInteractionTypes, EventTypes)
   
    ints <- ints %>% filter(type %in% allTypes)
    
    ints <- ints %>% dplyr::mutate(type = ifelse(type=="VISIT_DETAIL", "VISIT", type))
    ints <- ints %>% dplyr::mutate(type = ifelse(type=="APPOINTMENT_DETAIL", "APPOINTMENT", type))
    
    ints <- ints %>% select(-productName)
    
    flog.info("Preparing Design Matrix")
    flog.info("Size of ints %s %s", sdf_dim(ints)[1], sdf_dim(ints)[2])

    if(sdf_dim(ints)[1] == 0)
    {
        flog.error("No events in past %s days from %s.", LOOKBACK, predictRunDate)
        return(list(1,NULL))
    }
    
    ints <- dplyr::arrange(ints, accountId, date)    # sort interaction data by accountId and date
    
    ## Now prepare data for scoring
    
    #ints[, maxDate := max(date), by=c("accountId", "type")]
    ints <- ints %>% group_by(accountId, type) %>% dplyr::mutate(maxDate = max(date)) %>% ungroup() %>% select(-physicalMessageUID)
    
    # get latest SEND/VISIT date for each account
    ints <- ints %>% filter(date == maxDate) %>% distinct() %>% select(-maxDate)   # get only rows with latest date in interactions

    if (isReportOnly || (runmodel == "REPORT")) {  # runmodel is passed from Rundeck job param
        flog.info("Running reporting job...")
        
        ttePredictReport(sc, con, con_l, dynamic.model, segs, models.B, models.C, AP, ints,
                         RUN_UID, whiteList, tteParams)

        return (list(1, NULL))
      
    } else {
        # regular daily predictions on next 30 days
        #ints[, diff := as.integer(predictRunDate - date)]      
        ints <- ints %>% dplyr::mutate(diff = as.integer(datediff(predictRunDate, date))) # calculate diff between today and latest interaction date
      
        ints <- dplyr::arrange(ints, accountId, type, date)

        #ints[diff <=0, diff := -1]                 
        ints <- ints %>% dplyr::mutate(diff = ifelse(diff <= 0, -1L, diff))  # in case there are future dates (and today), set flag to -1
        
        ## prepare scoring data
        ints <- ints %>% select(-date)
        # now the data looks like:
        # accountId   type   diff
        #-------------------------
        # 1003        SEND    37
        # 1003        VISIT   58

        #ints <- as.data.table(dcast(ints, accountId~type, sum))
        ints <- ints %>% sdf_pivot(accountId~type, fun.aggregate = list(diff = "sum"))
        ints <- dplyr::arrange(ints, accountId) 

        ints <- ints %>% dplyr::mutate(VISIT = ifelse(is.na(VISIT), 0, VISIT))
        ints <- ints %>% dplyr::mutate(SEND  = ifelse(is.na(SEND), 0, SEND))
        # now the data looks like: (0 means no date info on the event; -1 means today)
        # accountId   SEND   VISIT
        #-------------------------
        # 1003        37      58
        # 1003        4       0
        
        #setnames(ints, "SEND", "preS")
        #setnames(ints, "VISIT", "preV")
        ints <- dplyr::rename(ints, preS = SEND, preV = VISIT)
        
        if ("WEBINAR" %in% tbl_vars(ints)) {
            ints <- dplyr::rename(ints, preW = WEBINAR)
        }

        if ("CONGRESSMTS" %in% tbl_vars(ints)) {
            ints <- dplyr::rename(ints, preC = CONGRESSMTS)
        }
        
        # replace 0 (meaning no date info) with LOOKBACK_MAX, which means the date is pretty old. 
        #ints[preS == 0, preS := LOOKBACK_MAX]
        ints <- ints %>% dplyr::mutate(preS = ifelse(preS == 0, LOOKBACK_MAX, preS))
        
        #ints[preV == 0, preV := LOOKBACK_MAX]
        ints <- ints %>% dplyr::mutate(preV = ifelse(preV == 0, LOOKBACK_MAX, preV))
        
        if ("preW" %in% tbl_vars(ints)) {
            #ints[preW == 0, preW := LOOKBACK_MAX]
            ints <- ints %>% dplyr::mutate(preW = ifelse(preW == 0, LOOKBACK_MAX, preW))
        }
        
        if ("preC" %in% tbl_vars(ints)) {
            #ints[preC == 0, preC := LOOKBACK_MAX]
            ints <- ints %>% dplyr::mutate(preC = ifelse(preC == 0, LOOKBACK_MAX, preC))
        }
        
        # replace -1 with 0
        #ints[preS == -1, preS := 0]
        ints <- ints %>% dplyr::mutate(preS = ifelse(preS == -1L, 0, preS))
          
        #ints[preV == -1, preV := 0]
        ints <- ints %>% dplyr::mutate(preV = ifelse(preV == -1L, 0, preV))
        
        # get the numS, numV from dynamic model
        #ints <- merge(ints, dm, by=c("accountId"), all.x = T)
        #ints <- semi_join(ints, dm, by = "accountId")
        ints <- left_join(ints, dm, by = c("accountId")) # given accountId, preS, preV, find numS, numV, etc.
                
       # ints <- ints %>% select(-c("TARGET", "date", "dow", "event")) # drop these columns
        
        # replace NA with default value for numS, numV
        #ints[is.na(numS), numS := 1]
        ints <- ints %>% dplyr::mutate(numS = ifelse(is.na(numS), 1L, numS))
        
        #ints[is.na(numV), numV := 1]
        ints <- ints %>% dplyr::mutate(numV = ifelse(is.na(numV), 1L, numV))
        
        #ints[is.na(pre2S), pre2S := VAL_NO_DATA]
        #ints[is.na(pre2V), pre2V := VAL_NO_DATA]
        ints <- ints %>% dplyr::mutate(pre2S = ifelse(is.na(pre2S), VAL_NO_DATA, pre2S))
        ints <- ints %>% dplyr::mutate(pre2V = ifelse(is.na(pre2V), VAL_NO_DATA, pre2V))
        
        #ints[is.na(avgST), avgST := 0]   # because numS=1
        #ints[is.na(avgVT), avgVT := 0]
        
        #ints[is.na(preT), preT := VAL_NO_DATA]
        ints <- ints %>% dplyr::mutate(preT = ifelse(is.na(preT), VAL_NO_DATA, preT))
          
        # copy the data for S and V
        ints <- ints %>% dplyr::mutate(ctr = 1L)
        
        dup <- copy(ints)
        #dup <- rbind(dup, ints[, ctr := 2])
        dup <- sdf_bind_rows(dup, dplyr::mutate(ints, ctr = 2L))

        ints <- dup
        
        #ints[ctr == 1, event := "S"]
        ints <- ints %>% dplyr::mutate(event = ifelse(ctr == 1L, "S", ctr))
        
        #ints[ctr == 2, event := "V"]
        ints <- ints %>% dplyr::mutate(event = ifelse(ctr == 2L, "V", event))

        ints <- ints %>% select(-ctr)
        
        ints <- dplyr::arrange(ints, accountId, event)
        
        # add a column of prediction date with today's date
        ints <- ints %>% dplyr::mutate(date = as.Date(predictRunDate))
        
        # repeat for next predictAhead days
        ints <- ints %>% collect() %>% data.table()
        dup  <- copy(ints)

        for(i in 1:(predictAhead-1)) {
          # Duplicate ints in order to predict future days
          # in each loop (one append), increase preS, preV, preT, etc. and date by 1
          if ("EVENT" %in% channels) {
              # handling event feature
              if ("preW" %in% names(ints) & ("preC" %in% names(ints))) {
                  dup <- rbind(dup, ints[, c("date", "preS", "preV", "pre2S", "pre2V", "preT", "preW", "preC") := 
                                       list(date+1, preS+1, preV+1, pre2S+1, pre2V+1, preT+1, preW+1, preC+1)])
              }
              else if ("preW" %in% names(ints)) {
                  dup <- rbind(dup, ints[, c("date", "preS", "preV", "pre2S", "pre2V", "preT", "preW") := 
                                         list(date+1, preS+1, preV+1, pre2S+1, pre2V+1, preT+1, preW+1)])
              }
              else if ("preC" %in% names(ints)) {
                  dup <- rbind(dup, ints[, c("date", "preS", "preV", "pre2S", "pre2V", "preT", "preC") := 
                                         list(date+1, preS+1, preV+1, pre2S+1, pre2V+1, preT+1, preC+1)])
              }
              else {
                dup <- rbind(dup, ints[, c("date", "preS", "preV", "pre2S", "pre2V", "preT") := 
                                         list(date+1, preS+1, preV+1, pre2S+1, pre2V+1, preT+1)])
              }
          } else {
              dup <- rbind(dup, ints[, c("date", "preS", "preV", "pre2S", "pre2V", "preT") := 
                                       list(date+1, preS+1, preV+1, pre2S+1, pre2V+1, preT+1)])
          }
        }
        
        setorder(dup, accountId, event, date)
        ints <- dup

        ## Method A for nightly predictions: Covariates + time between send & vist + DoW
        method <- "A"
        scores <- tteMethod(sc, models.A, AP, ints, RUN_UID, method)

        scores <- sdf_copy_to(sc, scores, "scores", overwrite=TRUE, memory=FALSE)
        scores <- scores %>% dplyr::mutate(date = to_date(date))
        scores <- scores %>% dplyr::mutate(method = "Covariates + Intervals + DoW")

        rm(dup)
        rm(ints)
        rm(AP)
        gc(); gc(); gc()

        flog.info("Return from messageTimingScoreModel")

        return(list(0, scores))
    }
}
