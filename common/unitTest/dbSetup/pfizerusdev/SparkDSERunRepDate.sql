CREATE TABLE `SparkDSERunRepDate` (
  `runRepDateId` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `runUID` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `repId` int(11) NOT NULL,
  `repUID` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `suggestedDate` date NOT NULL,
  PRIMARY KEY (`runRepDateId`),
  UNIQUE KEY `sparkdserunrepdate_AK1` (`runUID`,`repId`,`suggestedDate`),
  KEY `sparkdserunrepdate_parent` (`runUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
