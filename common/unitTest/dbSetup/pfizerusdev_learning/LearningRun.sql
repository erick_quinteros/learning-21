CREATE TABLE `LearningRun` (
  `learningRunUID` varchar(80) NOT NULL,
  `learningBuildUID` varchar(80) NOT NULL,
  `learningVersionUID` varchar(80) NOT NULL,
  `learningConfigUID` varchar(80) NOT NULL,
  `isPublished` tinyint(1) NOT NULL DEFAULT '0',
  `runType` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `executionStatus` varchar(20) DEFAULT NULL,
  `executionDateTime` datetime DEFAULT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`learningRunUID`),
  KEY `learningRun_fk_1_idx` (`learningConfigUID`),
  KEY `learningRun_fk_2_idx` (`learningVersionUID`),
  KEY `learningRun_fk_3_idx` (`learningBuildUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8