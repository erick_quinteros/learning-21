CREATE TABLE `RepEngagementCalculation` (
  `year` int(4) DEFAULT NULL,
  `month` int(2) DEFAULT NULL,
  `repUID` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `repName` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `repTeamUID` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `repTeamName` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seConfigId` int(11) DEFAULT NULL,
  `seConfigName` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `suggestionType` varchar(7) COLLATE utf8_unicode_ci DEFAULT NULL,
  `territoryId` varchar(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  `territoryName` varchar(101) COLLATE utf8_unicode_ci DEFAULT NULL,
  `engagedUniqueSuggestionsCount` bigint(21) NOT NULL DEFAULT '0',
  `totalSuggestionsDeliveredTimes` decimal(42,0) DEFAULT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `idx_repEngagement` (`year`,`month`,`repUID`,`repName`,`repTeamUID`,`repTeamName`,`seConfigId`,`suggestionType`,`territoryId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
