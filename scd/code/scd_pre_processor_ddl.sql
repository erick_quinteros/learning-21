-- SCD_IN_PRODUCT_CONFIG has the list of all products with Sales data for at least one metric
-- Could be an internal product (DIM_PRODUCT_KEY is not null) or external (DIM_EXTERNAL_PRODUCT_KEY is not null)
-- Internal products would have PRODUCT_UID populated
-- DSE products will also have PRODUCT_ID populated
-- IS_SCD_ENABLED and IS_MARKET_CALC_CUSTOM are flags managed via the Learning UI for SCD enabled products
-- What does IS_COMPETITOR mean?
create
or replace TABLE SCD.SCD_IN_PRODUCT_CONFIG (
    PRODUCT_CONFIG_KEY NUMBER(38, 0) NOT NULL autoincrement,
    DIM_PRODUCT_KEY NUMBER(38, 0),
    DIM_PRODUCT_ALL_KEY NUMBER(38, 0),
    PRODUCT_NAME VARCHAR(255),
    PRODUCT_UID VARCHAR(20),
    PRODUCT_ID NUMBER(38, 0),
    EXT_PRODUCT_IDENTIFIER VARCHAR(100),
    EXT_SOURCE_SYSTEM_NAME VARCHAR(100), 
    IS_COMPETITOR BOOLEAN,
    IS_SCD_ENABLED BOOLEAN,
    IS_MARKET_CALC_CUSTOM BOOLEAN DEFAULT '0',
    IS_ACTIVE BOOLEAN DEFAULT '1',
    CREATED_TS TIMESTAMP_NTZ(9) DEFAULT CAST(CURRENT_TIMESTAMP() AS TIMESTAMP_NTZ(9)),
    UPDATED_TS TIMESTAMP_NTZ(9),
    primary key (PRODUCT_CONFIG_KEY)
);

-- SCD_IN_SALES_FACT_SUMMARY captures the min/max sale dates for every product, metric for each source (BRICK/ACCOUNT or WEEKLY/MONTHLY)
create
or replace TABLE SCD.SCD_IN_SALES_FACT_SUMMARY (
    SCD_IN_SALES_FACT_SUMMARY_KEY NUMBER(38, 0) NOT NULL autoincrement,
    DIM_PRODUCT_KEY NUMBER(38, 0),
    DIM_PRODUCT_ALL_KEY NUMBER(38, 0),
    PRODUCT_UID VARCHAR(20),
    --PRODUCT_NAME VARCHAR(255),
    --PRODUCT_ID NUMBER(38, 0),
    EXT_PRODUCT_IDENTIFIER VARCHAR(100),
    EXT_SOURCE_SYSTEM_NAME VARCHAR(100), 
    PRODUCT_CONFIG_KEY NUMBER(38, 0),
    IS_COMPETITOR BOOLEAN,
    DIM_METRIC_KEY NUMBER(38, 0),
    DIM_FREQUENCY_KEY NUMBER(38, 0),
    REPORTING_LEVEL_KEY NUMBER(38, 0),
    MIN_SALE_DATE DATE,
    MAX_SALE_DATE DATE,
    IS_ACTIVE BOOLEAN DEFAULT '1',
    CREATED_TS TIMESTAMP_NTZ(9) DEFAULT CAST(CURRENT_TIMESTAMP() AS TIMESTAMP_NTZ(9)),
    UPDATED_TS TIMESTAMP_NTZ(9),
    primary key (SCD_IN_SALES_FACT_SUMMARY_KEY)
);

-- SCD_IN_PRODUCT_METRIC_SUMMARY captures the products and metrics that are useable. 
-- If DIM_MARKETBASKET_KEY is null, the useability flag is for Sales-change and Ordergap
-- Otherwise, the useability flag is for that market-basket for Market-basket and market-share use-cases.
-- Even though REPORTING_LEVEL_KEY and DIM_FREQUENCY_KEY are not displayed in the product-metric matrix, 
-- we need to store them as well to filter the dropdowns in the UI correctly
create
or replace TABLE SCD.SCD_IN_PRODUCT_METRIC_SUMMARY (
    SCD_IN_PRODUCT_METRIC_SUMMARY_KEY NUMBER(38, 0) NOT NULL autoincrement,
    PRODUCT_CONFIG_KEY NUMBER(38, 0),
    DIM_METRIC_KEY NUMBER(38, 0),
    DIM_FREQUENCY_KEY NUMBER(38, 0),
    REPORTING_LEVEL_KEY NUMBER(38, 0),
    DIM_MARKETBASKET_KEY NUMBER(38,0),
    IS_USEABLE BOOLEAN,
    REASON_NOT_USEABLE VARCHAR(255),
    IS_ACTIVE BOOLEAN DEFAULT '1',
    CREATED_TS TIMESTAMP_NTZ(9) DEFAULT CAST(CURRENT_TIMESTAMP() AS TIMESTAMP_NTZ(9)),
    UPDATED_TS TIMESTAMP_NTZ(9),
    primary key (SCD_IN_PRODUCT_METRIC_SUMMARY_KEY)
);

-- Table to capture the datapoints created in the Learning UI
create or replace TABLE SCD.SCD_OUT_DATAPOINT (
	DATAPOINT_KEY NUMBER(38,0) NOT NULL autoincrement,
    PRODUCT_CONFIG_KEY NUMBER(38, 0),
	DIM_METRIC_KEY NUMBER(38,0),
	DIM_FREQUENCY_KEY NUMBER(38,0) DEFAULT 3,
	DIM_MARKETBASKET_KEY NUMBER(38,0),
	USE_CASE_NAME_KEY NUMBER(38,0),
	REPORTING_LEVEL_KEY NUMBER(38,0),
	PRODUCT_NAME VARCHAR(255),
	METRIC_NAME VARCHAR(100),
	FREQUENCY_NAME VARCHAR(100),
	MARKET_BASKET_NAME VARCHAR(255),
	PERIOD_NUMBER NUMBER(38,0),
	USE_CASE_NAME VARCHAR(255),
	REPORTING_LEVEL_NAME VARCHAR(255),
	DATAPOINT_NAME VARCHAR(255),
	IS_ACTIVE BOOLEAN DEFAULT '1',
	CREATED_TS TIMESTAMP_NTZ(9) DEFAULT CAST(CURRENT_TIMESTAMP() AS TIMESTAMP_NTZ(9)),
	UPDATED_TS TIMESTAMP_NTZ(9),
	primary key (DATAPOINT_KEY)
);

-- View to see the list of products in each market-basket-definition
-- TODO: Rename DIM_ tables and primary keys after new tables are available
create
or replace view SCD.vw_MARKET_BASKET_PRODUCT AS (
    select
        bskt.MARKETBASKET_KEY,
        bskt.MARKETBASKET_NAME,
        COALESCE(sp1.PRODUCT_CONFIG_KEY, sp2.PRODUCT_CONFIG_KEY) PRODUCT_CONFIG_KEY,
        COALESCE(sp1.PRODUCT_NAME, sp2.PRODUCT_NAME) PRODUCT_NAME,
        p.PRODUCT_UID,
        sp1.EXT_PRODUCT_IDENTIFIER, 
        sp1.EXT_SOURCE_SYSTEM_NAME 
    from
        DW_CENTRAL.DIM_PRODUCT_MARKET_MAPPING map
        INNER JOIN DW_CENTRAL.DIM_MARKETBASKET bskt ON map.MARKETBASKET_KEY = bskt.MARKETBASKET_KEY
        LEFT JOIN DW_CENTRAL.DIM_PRODUCT_EXTERNAL pe ON map.EXTERNAL_PRODUCT_KEY = pe.EXTERNAL_PRODUCT_KEY
        LEFT JOIN SCD.SCD_IN_PRODUCT_CONFIG sp1 on sp1.EXT_PRODUCT_IDENTIFIER = pe.PRODUCT_NAME and sp1.EXT_SOURCE_SYSTEM_NAME = pe.SOURCE_SYSTEM_NAME
        LEFT JOIN DW_CENTRAL.DIM_PRODUCT p ON map.PRODUCT_KEY = p.PRODUCT_KEY
        LEFT JOIN SCD.SCD_IN_PRODUCT_CONFIG sp2 on sp2.PRODUCT_UID = p.PRODUCT_UID
);

--For each product, shows the mkt-baskets that it is part of and the list of other-products in each of those baskets
create
or replace view SCD.vw_OTHER_BASKET_PRODUCTS AS (
    select
        p.PRODUCT_CONFIG_KEY,
        p.PRODUCT_NAME,
        bskt.MARKETBASKET_KEY,
        bskt.MARKETBASKET_NAME,
        other_products.PRODUCT_NAME OTHER_PRODUCT_NAME,
        other_product_config.PRODUCT_CONFIG_KEY OTHER_PRODUCT_CONFIG_KEY
    from
        SCD.SCD_IN_PRODUCT_CONFIG p,
        SCD.vw_MARKET_BASKET_PRODUCT bskt,
        SCD.vw_MARKET_BASKET_PRODUCT other_products,
        SCD.SCD_IN_PRODUCT_CONFIG other_product_config
    where
        p.PRODUCT_CONFIG_KEY = bskt.PRODUCT_CONFIG_KEY
        and bskt.MARKETBASKET_KEY = other_products.MARKETBASKET_KEY 
        and other_products.PRODUCT_CONFIG_KEY = other_product_config.PRODUCT_CONFIG_KEY
        --and p.PRODUCT_NAME != other_products.PRODUCT_NAME
);

-- For each sales-dimension, shows whether it is useable or not (DIM_MARKETBASKET_KEY is null)
-- For products that are part of market-baskets, it shows whether the market-basket is useable or not (DIM_MARKETBASKET_KEY is not null)
-- TODO: remove the +1 after talking to Amol about why there is mismatch in DIM_FREQUENCY_KEY
create
or replace view SCD.vw_SCD_IN_USEABLE_SALES_DIM AS (
    select
        p.PRODUCT_CONFIG_KEY,
        p.PRODUCT_NAME,
        m.METRIC_KEY DIM_METRIC_KEY,
        m.METRIC_NAME,
        f.FREQUENCY_KEY DIM_FREQUENCY_KEY,
        f.FREQUENCY_NAME,
        r.REPORTING_LEVEL_KEY,
        r.REPORTING_LEVEL_NAME,
        mb.MARKETBASKET_KEY DIM_MARKETBASKET_KEY,
        mb.MARKETBASKET_NAME,
        MIN(pr.PERIOD_NUMBER) MIN_PERIOD_NUMBER,
        MAX(pr.PERIOD_NUMBER) MAX_PERIOD_NUMBER,
        IS_USEABLE,
        REASON_NOT_USEABLE
    from
        SCD.SCD_IN_PRODUCT_METRIC_SUMMARY pm
        INNER JOIN SCD.SCD_IN_PRODUCT_CONFIG p on pm.PRODUCT_CONFIG_KEY = p.PRODUCT_CONFIG_KEY
        INNER JOIN DW_CENTRAL.DIM_METRIC m on pm.DIM_METRIC_KEY = m.METRIC_KEY
        INNER JOIN DW_CENTRAL.DIM_FREQUENCY f on pm.DIM_FREQUENCY_KEY = f.FREQUENCY_KEY
        INNER JOIN DW_CENTRAL.DIM_PERIOD pr on pm.DIM_FREQUENCY_KEY = pr.FREQUENCY_KEY+1
        INNER JOIN SCD.SCD_IN_REPORTING_LEVEL r on pm.REPORTING_LEVEL_KEY = r.REPORTING_LEVEL_KEY
        LEFT JOIN DW_CENTRAL.DIM_MARKETBASKET mb on pm.DIM_MARKETBASKET_KEY = mb.MARKETBASKET_KEY
    group by
        p.PRODUCT_CONFIG_KEY,
        p.PRODUCT_NAME,
        m.METRIC_KEY,
        m.METRIC_NAME,
        f.FREQUENCY_KEY,
        f.FREQUENCY_NAME,
        r.REPORTING_LEVEL_KEY,
        r.REPORTING_LEVEL_NAME,
        mb.MARKETBASKET_KEY,
        mb.MARKETBASKET_NAME,
        IS_USEABLE,
        REASON_NOT_USEABLE
);    

-- For each product-metric, shows whether it is useable or not 
--(for sales-volume/order-gap detection when IS_MARKET is null, and for mkt-basket/mkt-share detection when IS_MARKET = 1)
create
or replace view SCD.vw_SCD_IN_USEABLE_PRODUCT_METRIC AS (
    SELECT
        P.PRODUCT_CONFIG_KEY,
        P.PRODUCT_NAME,
        P.IS_SCD_ENABLED,
        P.IS_MARKET_CALC_CUSTOM,
        P.IS_COMPETITOR,
        M.METRIC_KEY DIM_METRIC_KEY,
        M.METRIC_NAME,
        CASE
            WHEN PR.PERIOD_NUMBER = 1 THEN 0 ELSE 1
        END FOR_MARKET,
        COALESCE(pm.IS_USEABLE, FALSE) IS_USEABLE,
        COALESCE(pm.REASON_NOT_USEABLE, 'No data available') REASON_NOT_USEABLE
    FROM
        SCD.SCD_IN_PRODUCT_CONFIG P
        CROSS JOIN DW_CENTRAL.DIM_METRIC M -- explode for every metric
        CROSS JOIN DW_CENTRAL.DIM_PERIOD PR  -- to explode to 2 rows (one for product usability and 2nd for mkt-basket usability)
        LEFT JOIN (
            select
                PRODUCT_CONFIG_KEY,
                PRODUCT_NAME,
                DIM_METRIC_KEY,
                METRIC_NAME,
                CASE
                    WHEN DIM_MARKETBASKET_key IS null THEN 1 ELSE 2
                END FOR_MARKET,
                SUM(
                    CASE
                        WHEN IS_USEABLE = TRUE THEN 1
                        ELSE 0
                    END
                ) > 0 AS IS_USEABLE,
                LISTAGG(REASON_NOT_USEABLE, ',<b>') AS REASON_NOT_USEABLE
            from
                SCD.vw_SCD_IN_USEABLE_SALES_DIM
            group by
                DIM_METRIC_KEY,
                PRODUCT_NAME,
                PRODUCT_CONFIG_KEY,
                METRIC_NAME,
                CASE
                    WHEN DIM_MARKETBASKET_key IS null THEN 1 ELSE 2
                END
        ) pm on P.PRODUCT_CONFIG_KEY = pm.PRODUCT_CONFIG_KEY
        and M.METRIC_KEY = pm.DIM_METRIC_KEY
        and PR.PERIOD_NUMBER = pm.FOR_MARKET
    WHERE
        PR.PERIOD_NUMBER BETWEEN 1 AND 2 
        --and P.IS_COMPETITOR = FALSE
);

GRANT USAGE ON SCHEMA RPT_DWPREPROD.SCD TO ROLE ML_ADMIN_RL;
GRANT SELECT ON ALL TABLES IN SCHEMA RPT_DWPREPROD.SCD TO ROLE ML_ADMIN_RL;
GRANT INSERT,UPDATE,DELETE,TRUNCATE ON ALL TABLES IN SCHEMA RPT_DWPREPROD.SCD TO ROLE ML_ADMIN_RL;
GRANT SELECT ON ALL VIEWS IN SCHEMA RPT_DWPREPROD.SCD TO ROLE ML_ADMIN_RL;
GRANT USAGE ON SCHEMA RPT_DWPREPROD.SCD TO ROLE READ_ONLY_RL;
GRANT SELECT ON ALL TABLES IN SCHEMA RPT_DWPREPROD.SCD TO ROLE READ_ONLY_RL;
GRANT SELECT ON ALL VIEWS IN SCHEMA RPT_DWPREPROD.SCD TO ROLE READ_ONLY_RL;
