--------------------------------------------------
-- Script to initialize SCD_IN tables whenever new sales data is loaded.
-- This script will preserve the list of products (SCD_IN_PRODUCT_CONFIG table with user's updates)
-- It will delete and recreate the other 2 tables (SALES_FACT_SUMMARY and PRODUCT_METRIC_SUMMARY)
--------------------------------------------------

--------------------------------------------------
-- High level steps

-- 1. Summarize the raw facts to SCD_IN_SALES_FACT_SUMMARY
-- a. First, from BRICK_WEEKLY
-- b. Join with both (internal and external) product tables to find the latest key and natural-key for both
-- c. Then, from ACCOUNT_WEEKLY (need skip logic, if fact table does not exist and if DB design is not going to pre-create empty fact tables for it)
-- Note: Don't do this for BRICK_MONTHLY, ACOUNT_MONTHLY now.  To be added later if/when SCD model supports it.
-- d. Before using the sales-fact-summary data, update the latest DIM_PRODUCT_KEY, DIM_PRODUCT_ALL_KEY (and natural keys from ALL_PRODUCTS list) in case they have been versioned/updated to newer records

-- 2. Use the SALES_FACT_SUMMARY, to insert/update the unique list of products into SCD_IN_PRODUCT_CONFIG.
-- a. Update latest DIM_PRODUCT_KEY, PRODUCT_UID and DIM_PRODUCT_ALL_KEY for all products. Note: This update is being done with the natural key of the sales product to capture any product that was originally in sales-data and later moved to being part of DSE
-- b. Insert any new products. Again, the not-exists query below is based on the natural key of the sales product (this will capture all products in sales-data regardless of when they were converted to DSE products or when the mapping data was provided)

-- 3. Update the additional attributes for the products
-- a. Update the latest PRODUCT_NAME, IS_COMPETITOR for all products
-- b. Then, update PRODUCT_ID (for DSE) products
-- c. Finally, Auto-enable SCD for all internal products?

-- 4. Update SCD_IN_SALES_FACT_SUMMARY with PRODUCT_CONFIG_KEY for ease of joining (without having to rely on PRODUCT_UID or EXT_PRODUCT_IDENTIFIER)
-- 5. Compute SCD_IN_PRODUCT_METRIC_SUMMARY for useable metrics for each product.  The 2 rules are: x days of history (max_date - min_date) and most-recent sale within y days of tolerance
-- 6. For each product and mkt-basket that it is part of, find how many of the other-produts have this metric useable. If any of them are, then the mkt-basket is useable.  Else, not useable.
-- 7. Finally, mark any products for which no market-basket is defined or when no products exist for the basket as not useable with DIM_MARKETBASKET_KEY = -1

--------------------------------------------------

--------------------------------------------------
-- 1. Summarize the raw facts
---------------------------------------------------
DELETE FROM
    SCD.SCD_IN_SALES_FACT_SUMMARY;

-- 1a. First from BRICK_WEEKLY
-- Join with both (internal and external) product tables to find the latest key and natural-key for both
INSERT INTO
    SCD.SCD_IN_SALES_FACT_SUMMARY (
        DIM_METRIC_KEY,
        DIM_FREQUENCY_KEY,
        REPORTING_LEVEL_KEY,
        DIM_PRODUCT_KEY,
        DIM_PRODUCT_ALL_KEY,
        PRODUCT_UID,
        MIN_SALE_DATE,
        MAX_SALE_DATE,
        IS_ACTIVE,
        CREATED_TS,
        UPDATED_TS
    )
SELECT
    METRIC_KEY,
    FREQUENCY_KEY,
    1, -- 1 for BRICK and 2 for ACCOUNT tables
    f.PRODUCT_KEY AS DIM_PRODUCT_KEY,
    pe.EXTERNAL_PRODUCT_KEY AS DIM_PRODUCT_ALL_KEY,
    f.PRODUCT_UID,
    MIN(SALE_DATE) AS MIN_SALE_DATE,
    MAX(SALE_DATE) AS MAX_SALE_DATE,
    TRUE,
    CURRENT_TIMESTAMP,
    CURRENT_TIMESTAMP
FROM
    DW_CENTRAL.F_BRICK_WEEKLY f
        LEFT JOIN DW_CENTRAL.DIM_PRODUCT_EXTERNAL pe ON f.EXTERNAL_PRODUCT_KEY = pe.EXTERNAL_PRODUCT_KEY
GROUP BY
    METRIC_KEY,
    FREQUENCY_KEY,
    f.PRODUCT_KEY,
    pe.EXTERNAL_PRODUCT_KEY,
    f.PRODUCT_UID;
    
    
-- TODO: 1b. Then, from ACCOUNT_WEEKLY (need skip logic, if fact table does not exist and if DB design is not going to pre-create empty fact tables for it)
-- Don't do this for BRICK_MONTHLY, ACOUNT_MONTHLY until SCD model supports it

-- 1c. Before using the sales-fact-summary data, update the latest DIM_PRODUCT_KEY, DIM_PRODUCT_ALL_KEY (and natural keys from ALL_PRODUCTS list) in case they have been versioned/updated to newer records
-- TODO: Replace DIM_PRODUCT_EXTERNAL with new tablename, DIM_EXTERNAL_PRODUCT_KEY with DIM_PRODUCT_ALL_KEY and use PRODUCT_IDENTIFIER instead of PRODUCT_NAME once new tables are avaiable
UPDATE 
    SCD.SCD_IN_SALES_FACT_SUMMARY fs
SET
    DIM_PRODUCT_KEY = p.PRODUCT_KEY,
    DIM_PRODUCT_ALL_KEY = latest_pe.EXTERNAL_PRODUCT_KEY,
    EXT_PRODUCT_IDENTIFIER = latest_pe.PRODUCT_NAME,
    EXT_SOURCE_SYSTEM_NAME = latest_pe.SOURCE_SYSTEM_NAME
FROM
    SCD.SCD_IN_SALES_FACT_SUMMARY f
    LEFT JOIN DW_CENTRAL.DIM_PRODUCT p ON f.PRODUCT_UID = p.PRODUCT_UID and p.RECORD_END_DATE = to_date('9999-01-01', 'YYYY-MM-DD') and p.SOURCE_SYSTEM_NAME = 'dse'
    LEFT JOIN DW_CENTRAL.DIM_PRODUCT_EXTERNAL pe ON f.DIM_PRODUCT_ALL_KEY = pe.EXTERNAL_PRODUCT_KEY
    LEFT JOIN DW_CENTRAL.DIM_PRODUCT_EXTERNAL latest_pe on pe.PRODUCT_NAME = latest_pe.PRODUCT_NAME AND pe.SOURCE_SYSTEM_NAME = latest_pe.SOURCE_SYSTEM_NAME AND latest_pe.RECORD_END_DATE = TO_DATE('9999-01-01', 'YYYY-MM-DD')
WHERE 
 f.SCD_IN_SALES_FACT_SUMMARY_KEY = fs.SCD_IN_SALES_FACT_SUMMARY_KEY;
 
--------------------------------------------------
-- 2. Use the SALES_FACT_SUMMARY, to build the unique list of products into SCD_IN_PRODUCT_CONFIG
---------------------------------------------------
-- 2b. Do insert/update of products into SCD_IN_PRODUCT_CONFIG
-- Update latest DIM_PRODUCT_KEY, PRODUCT_UID and DIM_PRODUCT_ALL_KEY for all products
-- Note: This update is being done with the natural key of the sales product to capture any product that was originally in sales-data and later moved to being part of DSE
UPDATE 
    SCD.SCD_IN_PRODUCT_CONFIG p
SET
    p.DIM_PRODUCT_KEY = fs_latest_key.DIM_PRODUCT_KEY,
    p.DIM_PRODUCT_ALL_KEY = fs_latest_key.DIM_PRODUCT_ALL_KEY
FROM
    (select PRODUCT_UID, EXT_PRODUCT_IDENTIFIER, EXT_SOURCE_SYSTEM_NAME, MAX(f.DIM_PRODUCT_KEY) DIM_PRODUCT_KEY,MAX(f.DIM_PRODUCT_ALL_KEY) DIM_PRODUCT_ALL_KEY
    from SCD.SCD_IN_SALES_FACT_SUMMARY f 
    group by PRODUCT_UID, EXT_PRODUCT_IDENTIFIER, EXT_SOURCE_SYSTEM_NAME) fs_latest_key
WHERE
    fs_latest_key.EXT_PRODUCT_IDENTIFIER = p.EXT_PRODUCT_IDENTIFIER AND fs_latest_key.EXT_SOURCE_SYSTEM_NAME = p.EXT_SOURCE_SYSTEM_NAME;
  
-- temporary until DIM_PRODUCT_ALL is cleaned up.  Can be removed after that
UPDATE 
    SCD.SCD_IN_PRODUCT_CONFIG p
SET
    p.DIM_PRODUCT_KEY = fs_latest_key.DIM_PRODUCT_KEY,
    p.DIM_PRODUCT_ALL_KEY = fs_latest_key.DIM_PRODUCT_ALL_KEY
FROM
    (select PRODUCT_UID, EXT_PRODUCT_IDENTIFIER, EXT_SOURCE_SYSTEM_NAME, MAX(f.DIM_PRODUCT_KEY) DIM_PRODUCT_KEY,MAX(f.DIM_PRODUCT_ALL_KEY) DIM_PRODUCT_ALL_KEY
    from SCD.SCD_IN_SALES_FACT_SUMMARY f 
    group by PRODUCT_UID, EXT_PRODUCT_IDENTIFIER, EXT_SOURCE_SYSTEM_NAME) fs_latest_key
WHERE
    fs_latest_key.PRODUCT_UID = p.PRODUCT_UID;
  
-- 2c. Insert any new products
-- Again, the not-exists query below is based on the natural key of the sales product 
-- (this will capture all products in sales-data regardless of when they were converted to DSE products or when the mapping data was provided)
INSERT INTO
    SCD.SCD_IN_PRODUCT_CONFIG (
        PRODUCT_UID,
        EXT_PRODUCT_IDENTIFIER,
        EXT_SOURCE_SYSTEM_NAME,
        DIM_PRODUCT_KEY,
        DIM_PRODUCT_ALL_KEY,
        IS_ACTIVE,
        CREATED_TS,
        UPDATED_TS
    )
SELECT
    PRODUCT_UID,
    EXT_PRODUCT_IDENTIFIER,
    EXT_SOURCE_SYSTEM_NAME,
    DIM_PRODUCT_KEY,
    DIM_PRODUCT_ALL_KEY,
    TRUE,
    CURRENT_TIMESTAMP,
    CURRENT_TIMESTAMP
FROM
    (
        SELECT
            PRODUCT_UID,
            EXT_PRODUCT_IDENTIFIER,
            EXT_SOURCE_SYSTEM_NAME,
            DIM_PRODUCT_KEY,
            DIM_PRODUCT_ALL_KEY
        FROM
            SCD.SCD_IN_SALES_FACT_SUMMARY
        GROUP BY
            PRODUCT_UID,
            EXT_PRODUCT_IDENTIFIER,
            EXT_SOURCE_SYSTEM_NAME,
            DIM_PRODUCT_KEY,
            DIM_PRODUCT_ALL_KEY
    ) fs
WHERE
    NOT EXISTS (
        SELECT
            1
        FROM
            SCD.SCD_IN_PRODUCT_CONFIG p
        WHERE
            --(
            --    (p.EXT_PRODUCT_IDENTIFIER is NULL and p.EXT_SOURCE_SYSTEM_NAME is NULL)
            --    or 
                (p.EXT_PRODUCT_IDENTIFIER = fs.EXT_PRODUCT_IDENTIFIER and p.EXT_SOURCE_SYSTEM_NAME = fs.EXT_SOURCE_SYSTEM_NAME)
            --)
            --and (
            --    p.PRODUCT_UID is NULL
            --    or p.PRODUCT_UID = fs.PRODUCT_UID
            --)
    );

-- 3a. Update the latest PRODUCT_NAME, IS_COMPETITOR for all products
update
    SCD.SCD_IN_PRODUCT_CONFIG sp
set
    sp.PRODUCT_NAME = p.PRODUCT_NAME,
    sp.IS_COMPETITOR = COALESCE(p.IS_COMPETITOR, FALSE)
from
    DW_CENTRAL.DIM_PRODUCT_EXTERNAL p
where
    sp.DIM_PRODUCT_ALL_KEY = p.EXTERNAL_PRODUCT_KEY;

-- 3b. Then, update PRODUCT_ID (for DSE) products
-- Note: we are updating IS_COMPETITOR again here.  This is not really necessary since 
-- DIM_PRODUCT_ALL.IS_COMPETITOR = TRUE for any sales product for which mapping is not provided. But keep it until 3a query includes all products
update
    SCD.SCD_IN_PRODUCT_CONFIG sp
set
    sp.PRODUCT_NAME = p.PRODUCT_NAME,
    sp.PRODUCT_ID = p.PRODUCT_ID,
    sp.IS_COMPETITOR =  COALESCE(p.IS_COMPETITOR, FALSE) 
from
    DW_CENTRAL.DIM_PRODUCT p
where
    sp.DIM_PRODUCT_KEY = p.PRODUCT_KEY;

-- 3c. Finally, Auto-enable SCD for all internal products?
-- TODO: Need to see more data to decide if this is correct and worth doing
update
    SCD.SCD_IN_PRODUCT_CONFIG
set
    IS_SCD_ENABLED = 
        CASE
        WHEN IS_COMPETITOR = FALSE THEN TRUE
        ELSE FALSE
        END
where
    IS_SCD_ENABLED is null;

---------------------------------------------------
-- 4. Update SCD_IN_SALES_FACT_SUMMARY with PRODUCT_CONFIG_KEY for ease of joining (without having to rely on PRODUCT_UID or EXT_PRODUCT_IDENTIFIER)
---------------------------------------------------
UPDATE
    SCD.SCD_IN_SALES_FACT_SUMMARY fs
SET
    PRODUCT_CONFIG_KEY = p.PRODUCT_CONFIG_KEY
FROM
    SCD.SCD_IN_PRODUCT_CONFIG p
WHERE
    p.EXT_PRODUCT_IDENTIFIER = fs.EXT_PRODUCT_IDENTIFIER and p.EXT_SOURCE_SYSTEM_NAME = fs.EXT_SOURCE_SYSTEM_NAME
;

-- temporary until DIM_PRODUCT_ALL is cleaned up.  Can be removed after that
UPDATE
    SCD.SCD_IN_SALES_FACT_SUMMARY fs
SET
    PRODUCT_CONFIG_KEY = p.PRODUCT_CONFIG_KEY
FROM
    SCD.SCD_IN_PRODUCT_CONFIG p
WHERE
    p.PRODUCT_UID = fs.PRODUCT_UID
;


---------------------------------------------------
-- 5. Compute PRODUCT_METRIC_SUMMARY for useable metrics for each product (2 rules: x days of history (max_date - min_date) and most-recent sale within y days of tolerance)
---------------------------------------------------
DELETE FROM
    SCD.SCD_IN_PRODUCT_METRIC_SUMMARY;

INSERT INTO
    SCD.SCD_IN_PRODUCT_METRIC_SUMMARY (
        PRODUCT_CONFIG_KEY,
        DIM_METRIC_KEY,
        DIM_FREQUENCY_KEY,
        REPORTING_LEVEL_KEY,
        IS_USEABLE,
        REASON_NOT_USEABLE,
        IS_ACTIVE,
        CREATED_TS,
        UPDATED_TS
    )
SELECT
    fs.PRODUCT_CONFIG_KEY,
    DIM_METRIC_KEY,
    DIM_FREQUENCY_KEY,
    REPORTING_LEVEL_KEY,
    CASE
        WHEN MAX_SALE_DATE - MIN_SALE_DATE >= TO_NUMBER(p1.PARAM_VALUE) THEN CASE
            WHEN CURRENT_DATE - MAX_SALE_DATE <= TO_NUMBER(p2.PARAM_VALUE) THEN TRUE
            ELSE FALSE
        END
        ELSE FALSE
    END IS_USEABLE,
    CASE
        WHEN MAX_SALE_DATE - MIN_SALE_DATE >= TO_NUMBER(p1.PARAM_VALUE) THEN CASE
            WHEN CURRENT_DATE - MAX_SALE_DATE <= TO_NUMBER(p2.PARAM_VALUE) THEN NULL
            ELSE 'Most-recent-sale-date is ' || TO_CHAR(MAX_SALE_DATE) || '. Most recent sale should be within the last ' || p2.PARAM_VALUE || ' days'
        END
        ELSE 'MAX=' || TO_CHAR(MAX_SALE_DATE) || ' AND MIN=' || TO_CHAR(MIN_SALE_DATE) || ' found. ' || p1.PARAM_VALUE || ' days of history expected'
    END REASON_NOT_USEABLE,
    TRUE,
    CURRENT_TIMESTAMP,
    CURRENT_TIMESTAMP
FROM
    SCD.SCD_IN_SALES_FACT_SUMMARY fs,
    SCD.SCD_IN_PRODUCT_CONFIG p,
    SCD.SCD_IN_PARAM p1,
    SCD.SCD_IN_PARAM p2
WHERE
    fs.PRODUCT_CONFIG_KEY = p.PRODUCT_CONFIG_KEY
    and p1.PARAM_NAME = 'SCD_HISTORY_DAYS'
    and p2.PARAM_NAME = 'SCD_LATE_TOLERANCE_DAYS';

---------------------------------------------------
-- 6. For each product and mkt-basket that it is part of, find how many of the other-produts have this metric useable.  
-- If any of them are, then the mkt-basket is useable.  Else, not useable.
---------------------------------------------------
INSERT INTO
    SCD.SCD_IN_PRODUCT_METRIC_SUMMARY (
        PRODUCT_CONFIG_KEY,
        DIM_METRIC_KEY,
        DIM_FREQUENCY_KEY,
        REPORTING_LEVEL_KEY,
        DIM_MARKETBASKET_KEY,
        IS_USEABLE,
        REASON_NOT_USEABLE,
        IS_ACTIVE,
        CREATED_TS,
        UPDATED_TS
    )
SELECT
    PRODUCT_CONFIG_KEY,
    DIM_METRIC_KEY,
    DIM_FREQUENCY_KEY,
    REPORTING_LEVEL_KEY,
    MARKETBASKET_KEY,
    CASE WHEN USEABLE_PROD_COUNT > 0 THEN TRUE ELSE FALSE END,
    CASE WHEN USEABLE_PROD_COUNT > 0 THEN '' || USEABLE_PROD_COUNT || ' / ' || (TOTAL_PROD_COUNT-1) || ' products available' ELSE 'No competitor product data available for this metric' END,
    --CASE WHEN USEABLE_PROD_COUNT = TOTAL_PROD_COUNT-1 THEN TRUE ELSE FALSE END,
    --CASE WHEN USEABLE_PROD_COUNT = TOTAL_PROD_COUNT-1 THEN NULL ELSE 'Only ' || USEABLE_PROD_COUNT || ' out of ' || TOTAL_PROD_COUNT || ' products are useable' END,
    TRUE,
    CURRENT_TIMESTAMP,
    CURRENT_TIMESTAMP
FROM
    (SELECT
        others.PRODUCT_CONFIG_KEY,
        pm.DIM_METRIC_KEY,
        pm.DIM_FREQUENCY_KEY,
        pm.REPORTING_LEVEL_KEY,
        bskt_count.MARKETBASKET_KEY,
        SUM(
            CASE
                WHEN pm.IS_USEABLE THEN 1
                ELSE 0
            END
        ) AS USEABLE_PROD_COUNT,
        bskt_count.TOTAL_PROD_COUNT
    FROM
        SCD.vw_OTHER_BASKET_PRODUCTS others, SCD.SCD_IN_PRODUCT_METRIC_SUMMARY pm,
        (
            select
                MARKETBASKET_KEY,
                MARKETBASKET_NAME,
                COUNT(MARKETBASKET_KEY) TOTAL_PROD_COUNT
            from
                SCD.vw_MARKET_BASKET_PRODUCT
            group by
                MARKETBASKET_KEY,
                MARKETBASKET_NAME
        ) bskt_count -- total products in a basket
    WHERE
        others.OTHER_PRODUCT_CONFIG_KEY = pm.PRODUCT_CONFIG_KEY 
        and pm.DIM_MARKETBASKET_KEY is null 
        and others.MARKETBASKET_KEY = bskt_count.MARKETBASKET_KEY
        and others.PRODUCT_CONFIG_KEY != others.OTHER_PRODUCT_CONFIG_KEY -- leave out the product being compared and look at only the other products in the basket
    GROUP BY
        others.PRODUCT_CONFIG_KEY,
        bskt_count.MARKETBASKET_KEY,
        bskt_count.MARKETBASKET_NAME,
        pm.DIM_METRIC_KEY,
        pm.DIM_FREQUENCY_KEY,
        pm.REPORTING_LEVEL_KEY,
        bskt_count.TOTAL_PROD_COUNT
    ) other_prod_count;

---------------------------------------------------
-- 6b. Finally, mark any products for which no market-basket is defined or when no products exist for the basket as not useable with DIM_MARKETBASKET_KEY = -1
---------------------------------------------------
INSERT INTO
    SCD.SCD_IN_PRODUCT_METRIC_SUMMARY (
        PRODUCT_CONFIG_KEY,
        DIM_METRIC_KEY,
        DIM_FREQUENCY_KEY,
        REPORTING_LEVEL_KEY,
        DIM_MARKETBASKET_KEY,
        IS_USEABLE,
        REASON_NOT_USEABLE,
        IS_ACTIVE,
        CREATED_TS,
        UPDATED_TS
    )
SELECT
    PRODUCT_CONFIG_KEY,
    m.METRIC_KEY,
    -1,
    -1,
    -1,
    FALSE,
    'No basket defined or no sales data available for any other product in the basket',
    TRUE,
    CURRENT_TIMESTAMP,
    CURRENT_TIMESTAMP
FROM
    SCD.SCD_IN_PRODUCT_CONFIG p,
    DW_CENTRAL.DIM_METRIC m -- cross-join to get a row for all metrics
WHERE
    not exists (
        select
            1
        from
            SCD.vw_SCD_IN_USEABLE_SALES_DIM u
        where
            u.product_config_key = p.product_config_key
            and u.dim_marketbasket_key is not null
    ) -- no usability records created for marketbasket cases
    and not exists (
        select
            1
        from
            SCD.vw_MARKET_BASKET_PRODUCT bskt
        where
            bskt.PRODUCT_CONFIG_KEY = p.PRODUCT_CONFIG_KEY
    ) -- and no basket defined for this product
    ;

COMMIT;
