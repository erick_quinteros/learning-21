context('testing messageSequenceDriver script in spark MSO module')
print(Sys.time())

# load packages and scripts required to run tests
library(openxlsx)
library(Learning)
library(uuid)
source(sprintf("%s/common/dbConnection/dbConnection.R",homedir))
source(sprintf('%s/common/unitTest/customExpectFunc.r',homedir))
source(sprintf('%s/common/unitTest/utilsFunc.r',homedir))
library(sparkLearning)

# writing mock data to database (func in source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir)))
requiredMockDataList <- list(pfizerusdev=c('Product','RepTeam','Message','MessageSetMessage','Event','EventType','Interaction','InteractionType','ProductInteractionType','InteractionProduct','InteractionAccount','InteractionProductMessage','AccountProduct','Rep','RepTeamRep','RepAccountAssignment','Account','MessageSet'),pfizerusdev_learning=c('LearningFile','LearningBuild','LearningRun','AKT_Message_Topic_Email_Learned','Sent_Email_vod__c_arc'),pfizerprod_cs=c('Approved_Document_vod__c','RecordType'))
# LearningRun is for testing, not required for run messageSequenceDriver
resetMockData(homedir,dbuser,dbhost,port,dbpassword,dbname,dbname_cs,requiredMockDataList)

# get buildUID
BUILD_UID <<- readModuleConfig(homedir, 'messageSequence','buildUID')
# add mock build dir (func in source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir)))
setupMockBuildDir(homedir, 'messageSequence', BUILD_UID, files_to_copy='learning.properties')

# add entry to learningBuild (simulate what done by API before calling the R script)
config <- initializeConfigurationNew(homedir, BUILD_UID)
VERSION_UID <- config[["versionUID"]]
CONFIG_UID <- config[["configUID"]]
con <- getDBConnectionLearning(dbuser, dbpassword, dbhost, dbname, port)
now <- format(Sys.time(), "%Y-%m-%d %H:%M:%S")
SQL <- sprintf("INSERT INTO LearningBuild (learningBuildUID,learningVersionUID,learningConfigUID,isDeployed,executionStatus,executionDatetime,isDeleted) VALUES('%s','%s','%s',0,'running','%s',0);",BUILD_UID, VERSION_UID, CONFIG_UID, now)
dbClearResult(dbSendQuery(con,SQL))
dbDisconnect(con)

# run script
if(!exists("RUN_UID")) RUN_UID <<- UUIDgenerate()
targetNames <<- c('a3RA00000001MtAMAU', 'a3RA0000000e0u5MAA', 'a3RA0000000e0wBMAQ', 'a3RA0000000e486MAA', 'a3RA0000000e47gMAA')
print('start running messageSequenceDriver')
sc <<- initializeSpark(homedir)
source(sprintf('%s/sparkMessageSequence/messageSequenceDriver.r',homedir))

# test cases
# test file structure
test_that("build dir has correct struture", {
  expect_file_exists(sprintf('%s/builds/',homedir))
  expect_file_exists(sprintf('%s/builds/%s',homedir,BUILD_UID))
  expect_file_exists(sprintf('%s/builds/%s/learning.properties',homedir,BUILD_UID))
  expect_file_exists(sprintf('%s/builds/%s/log_%s.txt',homedir,BUILD_UID,paste("Build",RUN_UID,sep="_")))
  expect_file_exists(sprintf('%s/builds/%s/print_%s.txt',homedir,BUILD_UID,Rpaste("Build",RUN_UID,sep="_")))
  expect_file_exists(sprintf('%s/builds/%s/models_%s',homedir,BUILD_UID,BUILD_UID))
  expect_file_exists(sprintf('%s/builds/%s/messageSequenceDriver.r_%s.xlsx',homedir,BUILD_UID,BUILD_UID))
  expect_num_of_file(sprintf('%s/builds/%s/models_%s',homedir,BUILD_UID,BUILD_UID),2)
})

test_that("model reference saved in excel is correct", {
  # read excel sheet 2 - importance file
  importance <- read.xlsx(sprintf('%s/builds/%s/messageSequenceDriver.r_%s.xlsx',homedir,BUILD_UID, BUILD_UID),sheet=2)
  expect_equal(dim(importance),c(602,7))
  expect_equal(aggregate(cbind(names, OPEN...a3RA00000001MtAMAU,OPEN...a3RA0000000e486MAA) ~ predictorType, data = importance, FUN = function(x){sum(!is.na(x))}, na.action = NULL), data.frame(predictorType=c("Account","AccountProduct","EmailTopic","Message","Visit"),names=c(431,87,52,4,28), OPEN...a3RA00000001MtAMAU=c(368,87,23,0,23),OPEN...a3RA0000000e486MAA=c(190,82,46,4,21), stringsAsFactors=FALSE)) # check number of each type of predictor
  expect_equal(importance[!is.na(importance$expiredStatus),"expiredStatus"], c(TRUE,TRUE,TRUE,TRUE))
  expect_equal(unname(unlist(importance[!is.na(importance$messageName),c("names","messageName")])), c("OPEN...a3RA0000000e0wBMAQ", "SEND...a3RA0000000e0u5MAA", "SEND...a3RA0000000e0wBMAQ", "SEND...a3RA0000000e47gMAA", "Chantix Side Effects RTE", "Chantix Side Effects RTE", "Chantix Side Effects RTE", "NPS Label Update RTE"))
  # check predictorName output for UI same as saved
  # load(sprintf('%s/messageSequence/tests/data/from_saveBuildModel_predictorNamesForDisplay.RData', homedir))
  # expect_equal(importance$names,predictorNamesForDisplay)
  # test same as saved excel
  importance_saved <- read.xlsx(sprintf('%s/builds/%s/messageSequenceDriver.r_%s.xlsx',homedir,BUILD_UID, BUILD_UID),sheet=2)
  expect_equal(colnames(importance), colnames(importance_saved))
  expect_equal(importance[,c("names","namesOri","predictorType","expiredStatus","messageName")], importance_saved[,c("names","namesOri","predictorType","expiredStatus","messageName")])

  # read excel sheet 3 - accuracy file
  models <- read.xlsx(sprintf('%s/builds/%s/messageSequenceDriver.r_%s.xlsx',homedir,BUILD_UID, BUILD_UID),sheet=3)
  expect_equal(dim(models),c(2,15))
  expect_equal(models$target, c("OPEN...a3RA00000001MtAMAU","OPEN...a3RA0000000e486MAA"))
  expect_equal(models$targetOri, c("OPEN___a3RA00000001MtAMAU","OPEN___a3RA0000000e486MAA"))
  expect_equal(models$expiredStatus, c(FALSE,TRUE))
  expect_equal(models$messageName, c("Kentucky RTE","Chantix Side Effects RTE"))
  expect_equal(models$messageSetUID, c("1fa5c615-e3ba-ec6a-d032-628632dbd73;04c398ff-78e1-a0c5-13e7-e37df32f96c;12607bdf-b40d-7c5e-d099-94a5510be62;910d7849-5850-781f-de44-0b1611b0704","1fa5c615-e3ba-ec6a-d032-628632dbd73;12607bdf-b40d-7c5e-d099-94a5510be62;910d7849-5850-781f-de44-0b1611b0704"))
  # test same as saved excel
  models_saved <- read.xlsx(sprintf('%s/builds/%s/messageSequenceDriver.r_%s.xlsx',homedir,BUILD_UID, BUILD_UID),sheet=3)
  expect_equal(colnames(models), colnames(models_saved))
  expect_equal(models[,c("target","targetOri","expiredStatus","messageName","messageSetUID")], models_saved[,c("target","targetOri","expiredStatus","messageName","messageSetUID")])
  # test whether saved model exists
  for (file in models$modelName)
  {expect_file_exists(file)}

  # read excel sheet 4 - aggImportance file
  aggImportance <- read.xlsx(sprintf('%s/builds/%s/messageSequenceDriver.r_%s.xlsx',homedir,BUILD_UID, BUILD_UID),sheet=4)
  expect_equal(dim(aggImportance),c(224,9))
  expect_equal(aggregate(cbind(count = names) ~ predictorType, data = aggImportance, FUN = function(x){NROW(x)}), data.frame(predictorType=c("Account","AccountProduct","EmailTopic","Message","Visit"),count=c(38,166,16,3,1), stringsAsFactors=FALSE)) # check number of each type of predictor
  expect_equal(aggregate(cbind(count = names) ~ predictorType + dropReason, data = aggImportance[!is.na(aggImportance$dropReason),], FUN = function(x){length(x)}), data.frame(predictorType=c("Account","AccountProduct","AccountProduct"),dropReason=c("<2 unique","<2 unique","<5 unique numeric or <2 quantile>0"),count=c(6,145,1), stringsAsFactors=FALSE)) # check number of each type of predictor for dropped predictor
  expect_equal(aggImportance[!is.na(aggImportance$expiredStatus),"expiredStatus"], c(TRUE,TRUE,TRUE))
  expect_equal(aggregate(cbind(count_OPEN...a3RA00000001MtAMAU, count_OPEN...a3RA0000000e486MAA) ~ predictorType, data = aggImportance[is.na(aggImportance$dropReason),], FUN = sum), data.frame(predictorType=c("Account","AccountProduct","EmailTopic","Message","Visit"),count_OPEN...a3RA00000001MtAMAU=c(368,87,23,0,23), count_OPEN...a3RA0000000e486MAA=c(190,82,46,4,21), stringsAsFactors=FALSE)) # check number of non-expired count for each type of predictor
  expect_equal(sum(aggImportance[,c("count_OPEN...a3RA00000001MtAMAU","count_OPEN...a3RA0000000e486MAA")], na.rm=TRUE),sum(!is.na(importance[,c("OPEN...a3RA00000001MtAMAU","OPEN...a3RA0000000e486MAA")])))# check number of non-expired count match
  # test same as saved excel
  aggImportance_saved <- read.xlsx(sprintf('%s/builds/%s/messageSequenceDriver.r_%s.xlsx',homedir,BUILD_UID, BUILD_UID),sheet=4)
  expect_equal(colnames(aggImportance), colnames(aggImportance_saved))
  expect_equal(aggImportance[,c("names","predictorType","expiredStatus","messageName","dropReason")], aggImportance_saved[,c("names","predictorType","expiredStatus","messageName","dropReason")])
})

# test entry in DB is fine
test_that("correct entry write to learning DB", {
  con <- getDBConnectionLearning(dbuser, dbpassword, dbhost, dbname, port)
  learningRun <- dbGetQuery(con, "SELECT * from LearningRun;")
  SQL <- sprintf("SELECT * from LearningBuild where learningBuildUID='%s';",BUILD_UID)
  learningBuild <- dbGetQuery(con, SQL)
  SQL <- sprintf("SELECT * from LearningFile where learningBuildUID='%s';",BUILD_UID)
  learningFile <- dbGetQuery(con, SQL)
  dbDisconnect(con)
  expect_equal(dim(learningRun),c(0,10))
  expect_equal(dim(learningBuild), c(1,9))
  expect_equal(unname(unlist(learningBuild[,c('learningVersionUID','learningConfigUID','isDeployed','executionStatus','isDeleted')])), c(VERSION_UID,CONFIG_UID,0,'success',0))
  expect_equal(learningBuild$updatedAt>=learningBuild$createdAt, TRUE)
  expect_equal(dim(learningFile), c(5,9))
  expect_setequal(learningFile$fileName, c("accuracy", "importance", "log", "print","aggImportance"))
})

# run test cases on log file contents
# read log files for info (func in source(sprintf('%s/common/unitTest/utilsFunc.r',homedir)))
log_contents <- readLogFile(homedir,BUILD_UID,paste("Build",RUN_UID,sep="_"))

test_that("check log file general structure", {
  ind <- grep('Number of targets:',log_contents)
  expect_str_end_match(log_contents[ind],'5')
  expect_str_end_match(log_contents[ind+1],'CHANTIX')
})

test_that("check processing AccountProduct/Account predictors", {
  ind <- grep('Analyzing account product data',log_contents)
  expect_length(ind,1)
  expect_str_start_match(log_contents[ind+1],'151 of 204')  # number of predictors from AccountProduct/Account dropped before loop to build models as they have less than 2 unique value
  expect_str_start_match(log_contents[ind+2],'1 of 21 numeric predictors (53 all predictors)')  # number of numeric predictors from AccountProduct/Account dropped before loop to build models as there are either less than 5 unique values or having <2 quantile >0
  expect_str_start_match(log_contents[ind+3],'0 of 52')  # number of remaining predictors from AccountProduct/Account dropped before loop to build models as they have less than 2 unique values (2nd check after processing numeric and character variable separately
  expect_str_end_match(log_contents[ind+4],'52 to 533')  # check for dcast manupilation
  expect_str_end_match(log_contents[ind+5],'added 72 and becomes 605') # check EmailTopic Open Rates predictor added
})

test_that("check message a3RA00000001MtAMAU have the correct design matrix", {
  ind <- grep('OPEN___a3RA00000001MtAMAU',log_contents)
  expect_length(ind,1)
  expect_str_end_match(log_contents[ind+1],'29')  # number of TargetName records
  expect_str_end_match(log_contents[ind+2],'319')  # number of SendName records
  expect_str_end_match(log_contents[ind+4],'222') # Number of accounts considered for dynamic features
  expect_str_end_match(log_contents[ind+6],'(227,34)')  # dim of predictors from dynamic features to static features
  expect_str_end_match(log_contents[ind+7],'(315,638)')  # dimension of start design matrix
  expect_str_end_match(log_contents[ind+9],'20')  # number of positive target records
  expect_str_end_match(log_contents[ind+10],'(227,638)')  # dimension of design matrix with positive send records
  expect_str_start_match(log_contents[ind+11],'133 of 638')  # number of predictors from design matrix allModel(static + dynamic features) dropped in target loop as they have less than 2 unique values after subsetting with send and filling NA with 0
  expect_str_start_match(log_contents[ind+12],'3 of 505')  # number of remaining predictors in the design matrix dropped because of learning.properties config setting
  expect_str_end_match(log_contents[ind+13],'(227,502)')  # final design matrix dimension
})

test_that("check message a3RA0000000e0u5MAA would be skipped as no targetName records", {
  ind <- grep('OPEN___a3RA0000000e0u5MAA',log_contents)
  expect_length(ind,1)
  expect_str_end_match(log_contents[ind+1],'0')  # number of TargetName records
  expect_str_end_match(log_contents[ind+2],'4')  # number of SendName records
  expect_str_start_match(log_contents[ind+3],'Finish modeling, Nothing to model as there are no targets in the data.')
})

test_that("check message a3RA0000000e0wBMAQ would be skipped as less than 5 positive OPEN", {
  ind <- grep('OPEN___a3RA0000000e0wBMAQ',log_contents)
  expect_length(ind,1)
  expect_str_end_match(log_contents[ind+1],'1')  # number of TargetName records
  expect_str_end_match(log_contents[ind+2],'7')   # number of SendName records
  expect_str_end_match(log_contents[ind+4],'7') # Number of accounts considered for dynamic features
  expect_str_end_match(log_contents[ind+6],'(7,37)')  # dim of predictors from dynamic features to static features
  expect_str_end_match(log_contents[ind+7],'(309,641)')  # dimension of start design matrix
  expect_str_end_match(log_contents[ind+9],'1')  # number of positive target records
  expect_str_start_match(log_contents[ind+10],'Finish modeling, Nothing to model as there are not enough positive target records in design matrix to build model.')
})

test_that("check message a3RA0000000e486MAA have the correct design matrix", {
  ind <- grep('OPEN___a3RA0000000e486MAA',log_contents)
  expect_length(ind,1)
  expect_str_end_match(log_contents[ind+1],'24')  # number of TargetName records
  expect_str_end_match(log_contents[ind+2],'76')   # number of SendName records
  expect_str_end_match(log_contents[ind+4],'68') # Number of accounts considered for dynamic features
  expect_str_end_match(log_contents[ind+6],'(72,38)')  # dim of predictors from dynamic features to static features
  expect_str_end_match(log_contents[ind+7],'(313,642)')  # dimension of start design matrix
  expect_str_end_match(log_contents[ind+9],'21')  # number of positive target records
  expect_str_end_match(log_contents[ind+10],'(71,642)')  # dimension of design matrix with positive send records
  expect_str_start_match(log_contents[ind+11],'282 of 642')  # number of predictors from design matrix allModel(static + dynamic features) dropped in target loop as they have less than 2 unique values after subsetting with send and filling NA with 0
  expect_str_start_match(log_contents[ind+12],'16 of 360')  # number of remaining predictors in the design matrix dropped because of learning.properties config setting
  expect_str_end_match(log_contents[ind+13],'(71,344)')  # final design matrix dimension
})

test_that("check message a3RA0000000e47gMAA would be skipped as less than 5 positive OPEN", {
  ind <- grep('OPEN___a3RA0000000e47gMAA',log_contents)
  expect_length(ind,1)
  expect_str_end_match(log_contents[ind+1],'1')  # number of TargetName records
  expect_str_end_match(log_contents[ind+2],'14')   # number of SendName records
  expect_str_end_match(log_contents[ind+4],'14') # Number of accounts considered for dynamic features
  expect_str_end_match(log_contents[ind+6],'(14,36)')  # dim of predictors from dynamic features to static features
  expect_str_end_match(log_contents[ind+7],'(309,640)')  # dimension of start design matrix
  expect_str_end_match(log_contents[ind+9],'1')  # number of positive target records
  expect_str_start_match(log_contents[ind+10],'Finish modeling, Nothing to model as there are not enough positive target records in design matrix to build model.')
})

# ###################  test for the case build fail #########################################
# rm(BUILD_UID)
# BUILD_UID <<- readModuleConfig(homedir, 'messageSequence','buildUID_fail')
# # not copy learning properties file so that it will file
# buildUIDDirpath <- paste(homedir,'/builds/',BUILD_UID,sep='')
# if (! dir.exists(buildUIDDirpath)) { # generate buildUID folder
#   dir.create(buildUIDDirpath)
# } else { # clean buildUID folder
#   unlink(paste(buildUIDDirpath,'/*',sep=''),recursive = TRUE, force = TRUE)
# }
# # insert into learningBUild to prepare for run
# drv <- dbDriver("MySQL")
# con <- dbConnect(drv,user=dbuser,host=dbhost,dbname=dbname_learning,port=port,password=dbpassword)
# now <- format(Sys.time(), "%Y-%m-%d %H:%M:%S")
# SQL <- sprintf("INSERT INTO LearningBuild (learningBuildUID,learningVersionUID,learningConfigUID,isDeployed,executionStatus,executionDatetime,isDeleted) VALUES('%s','%s','%s',0,'running','%s',0);",BUILD_UID, VERSION_UID, CONFIG_UID, now)
# dbClearResult(dbSendQuery(con,SQL))
# dbDisconnect(con)
# # run script
# RUN_UID <<- UUIDgenerate()
#
# print('start running messageSequenceDriver with failure expectation')
# expect_error(source(sprintf('%s/messageSequence/messageSequenceDriver.r',homedir)))
#
# # test entry in DB is fine
# test_that("correct entry write to learning DB", {
#   drv <- dbDriver("MySQL")
#   con <- dbConnect(drv,user=dbuser,host=dbhost,dbname=dbname_learning,port=port,password=dbpassword)
#   SQL <- sprintf("SELECT * from LearningBuild WHERE learningBuildUID ='%s';",BUILD_UID)
#   learningBuild <- dbGetQuery(con, SQL)
#   SQL <- sprintf("SELECT * from LearningFile WHERE learningBuildUID = '%s';",BUILD_UID)
#   learningFile <- dbGetQuery(con, SQL)
#   print(learningFile)
#   dbDisconnect(con)
#   expect_equal(dim(learningBuild), c(1,9))
#   expect_equal(learningBuild$executionStatus, 'failure')
#   expect_equal(dim(learningFile), c(2,9))
#   expect_equal(sort(learningFile$fileName), c("log", "print"))
# })
#
# # run test cases on log file contents
# test_that("check log file contents", {
#   log_file_path <- sprintf('%s/builds/%s/log_%s.txt',homedir,BUILD_UID,RUN_UID)
#   ff <- file(log_file_path,'r')
#   log_contents <- readLines(ff, warn=FALSE)
#   close(ff)
#   expect_length(log_contents, 2)
# })
