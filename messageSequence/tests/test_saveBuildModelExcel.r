context('test the saveBuildModel func in the messageSequence module')
print(Sys.time())

# loading library, common source script
library(Learning)
library(uuid)
library(openxlsx)
source(sprintf("%s/messageSequence/saveBuildModel.R",homedir))
source(sprintf('%s/common/unitTest/customExpectFunc.r',homedir))

# setup build folder for saving
BUILD_UID <- readModuleConfig(homedir, 'messageSequence','buildUID')
setupMockBuildDir(homedir, 'messageSequence', BUILD_UID, files_to_copy='learning.properties') # add mock build dir (func in source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir)))
DRIVERMODULE <- "messageSequenceDriver.r"
RUN_UID <- UUIDgenerate()
runSettings <- initializeClientNew(DRIVERMODULE, homedir, BUILD_UID, RUN_UID, errorFunc=NULL, createExcel=TRUE, createPlotDir=FALSE, createModelsaveDir=FALSE)
runSettings[["spreadsheetName"]] <- sprintf("%s/builds/%s/%s_%s.xlsx", homedir, BUILD_UID, DRIVERMODULE, BUILD_UID)

# load requried input
load(sprintf('%s/messageSequence/tests/data/from_messageSequence_models.RData', homedir))
load(sprintf('%s/messageSequence/tests/data/from_messageSequence_output.RData', homedir))
load(sprintf('%s/common/unitTest/data/from_buildStaticDesignMatrix_APpredictors.RData',homedir))
load(sprintf('%s/messageSequence/tests/data/from_messageSequence_pName.RData', homedir))
load(sprintf('%s/common/unitTest/data/from_buildStaticDesignMatrix_predictorNamesAPColMap.RData',homedir))
load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_messageSetMessage.RData', homedir))
load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_messageSet.RData', homedir))
load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_emailTopicNames.RData', homedir))
load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_messages.RData', homedir))
load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_messageTopic.RData', homedir))
load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_expiredMessages.RData', homedir))
load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_accountPredictorNames.RData', homedir))
load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_emailTopicNameMap.RData', homedir))
WORKBOOK <- runSettings[["WORKBOOK"]]
spreadsheetName <- runSettings[["spreadsheetName"]]
# call functions
saveBuildModelExcel(homedir, WORKBOOK, spreadsheetName, models, output, APpredictors, pName, messages, messageSet, messageSetMessage, messageTopic, expiredMessages, accountPredictorNames, emailTopicNameMap, predictorNamesAPColMap)

# test cases
test_that("model reference saved in excel is correct", {
  # read excel sheet 2 - importance file
  importance <- read.xlsx(sprintf('%s/builds/%s/messageSequenceDriver.r_%s.xlsx',homedir,BUILD_UID, BUILD_UID),sheet=2)
  expect_equal(dim(importance),c(602,7))
  expect_equal(aggregate(cbind(names, OPEN...a3RA00000001MtAMAU,OPEN...a3RA0000000e486MAA) ~ predictorType, data = importance, FUN = function(x){sum(!is.na(x))}, na.action = NULL), data.frame(predictorType=c("Account","AccountProduct","EmailTopic","Message","Visit"),names=c(431,87,52,4,28), OPEN...a3RA00000001MtAMAU=c(368,87,23,0,23),OPEN...a3RA0000000e486MAA=c(190,82,46,4,21), stringsAsFactors=FALSE)) # check number of each type of predictor
  expect_equal(importance[!is.na(importance$expiredStatus),"expiredStatus"], c(TRUE,TRUE,TRUE,TRUE))
  expect_equal(unname(unlist(importance[!is.na(importance$messageName),c("names","messageName")])), c("OPEN...a3RA0000000e0wBMAQ", "SEND...a3RA0000000e0u5MAA", "SEND...a3RA0000000e0wBMAQ", "SEND...a3RA0000000e47gMAA", "Chantix Side Effects RTE", "Chantix Side Effects RTE", "Chantix Side Effects RTE", "NPS Label Update RTE"))
  # check predictorName output for UI same as saved
  load(sprintf('%s/messageSequence/tests/data/from_saveBuildModel_predictorNamesForDisplay.RData', homedir))
  expect_equal(sort(importance$names),sort(predictorNamesForDisplay))
  # test same as saved excel
  importance_saved <- read.xlsx(sprintf('%s/builds/%s/messageSequenceDriver.r_%s.xlsx',homedir,BUILD_UID, BUILD_UID),sheet=2)
  expect_equal(colnames(importance), colnames(importance_saved))
  expect_equal(importance[,c("names","namesOri","predictorType","expiredStatus","messageName")], importance_saved[,c("names","namesOri","predictorType","expiredStatus","messageName")])
  
  # read excel sheet 3 - accuracy file
  models <- read.xlsx(sprintf('%s/builds/%s/messageSequenceDriver.r_%s.xlsx',homedir,BUILD_UID, BUILD_UID),sheet=3)
  expect_equal(dim(models),c(2,15))
  expect_equal(models$target, c("OPEN...a3RA00000001MtAMAU","OPEN...a3RA0000000e486MAA"))
  expect_equal(models$targetOri, c("OPEN___a3RA00000001MtAMAU","OPEN___a3RA0000000e486MAA"))
  expect_equal(models$expiredStatus, c(FALSE,TRUE))
  expect_equal(models$messageName, c("Kentucky RTE","Chantix Side Effects RTE"))
  expect_equal(models$messageSetUID, c("1fa5c615-e3ba-ec6a-d032-628632dbd73;04c398ff-78e1-a0c5-13e7-e37df32f96c;12607bdf-b40d-7c5e-d099-94a5510be62;910d7849-5850-781f-de44-0b1611b0704","1fa5c615-e3ba-ec6a-d032-628632dbd73;12607bdf-b40d-7c5e-d099-94a5510be62;910d7849-5850-781f-de44-0b1611b0704"))
  # test same as saved excel
  models_saved <- read.xlsx(sprintf('%s/builds/%s/messageSequenceDriver.r_%s.xlsx',homedir,BUILD_UID, BUILD_UID),sheet=3)
  expect_equal(colnames(models), colnames(models_saved))
  expect_equal(models[,c("target","targetOri","expiredStatus","messageName","messageSetUID")], models_saved[,c("target","targetOri","expiredStatus","messageName","messageSetUID")])
  # for (file in models$modelName)
  # {expect_file_exists(file)}
  
  # read excel sheet 4 - aggImportance file
  aggImportance <- read.xlsx(sprintf('%s/builds/%s/messageSequenceDriver.r_%s.xlsx',homedir,BUILD_UID, BUILD_UID),sheet=4)
  expect_equal(dim(aggImportance),c(224,9))
  expect_equal(aggregate(cbind(count = names) ~ predictorType, data = aggImportance, FUN = function(x){NROW(x)}), data.frame(predictorType=c("Account","AccountProduct","EmailTopic","Message","Visit"),count=c(38,166,16,3,1), stringsAsFactors=FALSE)) # check number of each type of predictor
  expect_equal(aggregate(cbind(count = names) ~ predictorType + dropReason, data = aggImportance[!is.na(aggImportance$dropReason),], FUN = function(x){length(x)}), data.frame(predictorType=c("Account","AccountProduct","AccountProduct"),dropReason=c("<2 unique","<2 unique","<5 unique numeric or <2 quantile>0"),count=c(6,145,1), stringsAsFactors=FALSE)) # check number of each type of predictor for dropped predictor
  expect_equal(aggImportance[!is.na(aggImportance$expiredStatus),"expiredStatus"], c(TRUE,TRUE,TRUE))
  expect_equal(aggregate(cbind(count_OPEN...a3RA00000001MtAMAU, count_OPEN...a3RA0000000e486MAA) ~ predictorType, data = aggImportance[is.na(aggImportance$dropReason),], FUN = sum), data.frame(predictorType=c("Account","AccountProduct","EmailTopic","Message","Visit"),count_OPEN...a3RA00000001MtAMAU=c(368,87,23,0,23), count_OPEN...a3RA0000000e486MAA=c(190,82,46,4,21), stringsAsFactors=FALSE)) # check number of non-expired count for each type of predictor
  expect_equal(sum(aggImportance[,c("count_OPEN...a3RA00000001MtAMAU","count_OPEN...a3RA0000000e486MAA")], na.rm=TRUE),sum(!is.na(importance[,c("OPEN...a3RA00000001MtAMAU","OPEN...a3RA0000000e486MAA")])))# check number of non-expired count match
  # test same as saved excel
  aggImportance_saved <- read.xlsx(sprintf('%s/builds/%s/messageSequenceDriver.r_%s.xlsx',homedir,BUILD_UID, BUILD_UID),sheet=4)
  expect_equal(colnames(aggImportance), colnames(aggImportance_saved))
  expect_equal(aggImportance[,c("names","predictorType","expiredStatus","messageName","dropReason")], aggImportance_saved[,c("names","predictorType","expiredStatus","messageName","dropReason")])
})
