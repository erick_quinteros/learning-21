##########################################################
#
#
# aktana-learning Anchor estimates Aktana Learning Engines.
#
# description: estimate anchor locations for reps
#
#
# created by : marc.cohen@aktana.com
# updated by : shirley.xu@aktana.com
#
# created on : 2015-10-27
# updated on : 2018-07-31
#
# Copyright AKTANA (c) 2015.
#
#
####################################################################################################################
library(data.table)
library(markovchain)
library(Rsolnp)
library(geosphere)
library(lattice)
library(foreach)
library(doMC)
library(doRNG)
library(futile.logger)
library(bizdays)


#########################
# func: calcualteAnchor.Predict
########################
calculateAnchor.Predict <- function(calendarAdherenceOn, calAdprobThreshold, repAvgDayLoc, repLoc, predictAheadDayList, calendarLocs, repP, logPrefix, maxNearDistance, maxFarDistance, WithinDayDistance, centers, withDOW=TRUE, sourceSuffix=NA, tryFitHigherOrder=FALSE) {
  
  # create suffix for logging purporse
  if (!withDOW & is.na(sourceSuffix)) { sourceSuffix <- 'noDOW' }
  if (!is.na(sourceSuffix)) {
    logPrefix <- sprintf("%s (%s)", logPrefix, sourceSuffix)
    source <- sprintf("history_%s", sourceSuffix)
  } else {
    source <- "history"
  }
  
  # create result holder
  predictResult <- data.table()
  predictedStates <- rep(NA,length(predictAheadDayList))
  names(predictedStates) <- as.character(predictAheadDayList)
  lastSinInts <- ''
  
  # calculate cluster center
  nodeLocs <- repAvgDayLoc[,.(aveLat=mean(aveLat),aveLong=mean(aveLong)),by=state]
  
  # calculate local probability
  # calcuate number of times a facility is visited on cerntain dow in the history
  if (withDOW) {
    repLoc[,visitCount:=.N, by=c("facilityId","dow")]
  } else {
    repLoc[,visitCount:=.N, by=c("facilityId")]
  }
  # calculate local probability
  repLoc[,localProb:=visitCount/sum(visitCount),by="state"]
  # remove duplicate facilityId for the rep in one day (that exists because of they visit multiple accounts in one facility)
  
  # this code fits a order 3 markov chain based on the sequence of states visited by the rep and defined by the centers of the clusters 
  # sort the locations centers by date for use in the markov chain fit logic
  setkey(repAvgDayLoc,date)
  
  # try fitting higher order marchov chain if desired
  if (tryFitHigherOrder) {
    # fit model
    fit2 = ""
    fit2 <- tryCatch({fitHigherOrder(repAvgDayLoc$state,order=3)},warning=function(w){flog.warn("%s: Fit2 Warning %s",logPrefix,w)},error=function(e){flog.error("%s: Fit2 Error %s",logPrefix,e)})
    
    if(class(fit2)=="list")
    {
      n <- repAvgDayLoc[,.I[.N]]
      n <- repAvgDayLoc[seq(n,n-2,-1)]$state
      
      lambda <- fit2$lambda
      if(lambda[1]<.95)
      {
        # TM matrix
        Q1 <- as.matrix(lambda[1]*fit2$Q[[1]])
        Q2 <- as.matrix(lambda[2]*fit2$Q[[2]])
        Q3 <- as.matrix(lambda[3]*fit2$Q[[3]])
        
        # Predict
        for(predictDate in as.list(predictAheadDayList))
        {
          # get predict day of week for logging
          predDOW <- weekdays(predictDate)
          
          d <- dim(Q1)[1]
          v1 <- rep(0,1,d)
          v1[which(colnames(Q1)==n[1])] <- 1
          v2 <- rep(0,1,d)
          v2[which(colnames(Q1)==n[2])] <- 1
          v3 <- rep(0,1,d)
          v3[which(colnames(Q1)==n[3])] <- 1
          
          TM <- Q1%*%v1 + Q2%*%v2 + Q3%*%v3
          
          nxtState <- colnames(Q1)[which(TM==max(TM))[1]]
          maxProb <- max(TM)
          
          if (maxProb==0) {
            flog.debug("%s predictDay:%s(%s)(history) having prob=0 for all %s->%s_cluster, skip to next date", logPrefix, as.character(predictDate),predDOW,currentS,predDOW)
            predictedStates[[as.character(predictDate)]] <- "NA(history, all states prob=0)"
            next
          }
          
          # append result
          predictLocs <- repLoc[state==nxtState,]
          predictLocs$facilityProb <- predictLocs$localProb * maxProb
          
          predictResult <- rbind(predictResult, data.table(repId=repP, date=predictDate, clusterProb=maxProb, maxNearDistance=maxNearDistance, maxFarDistance=maxFarDistance, accountId=NA, predictLocs[,c("facilityId","latitude","longitude","state","facilityProb")], source=sprintf("%s_MC03",source)))
          
          # update state & logging
          predictedStates[[as.character(predictDate)]] <- sprintf("%s(history_MC03_lambda: %.3f %.3f %.3f",nxtState,lambda[1],lambda[2],lambda[3])
          n[3] <- n[2]
          n[2] <- n[1]
          n[1] <- nxtState
        }
        flog.info("%s finish prediction, maxNearDistance=%s, maxFarDistance=%s, withinDayDistance=%s, clustercenters=%s->%s, intsCount=%s->%s, predicted cluster center:[%s]->%s", logPrefix,maxNearDistance, maxFarDistance, WithinDayDistance, centers, nrow(nodeLocs), nrow(repLoc), nrow(repAvgDayLoc), lastSinInts, paste(predictedStates,collapse="->"))
        return (list(predictResult=predictResult, predictedStates=predictedStates))
      }
    }
  }
  
  # preventPrint <- plotData(sprintf("rep=%s clusters=%s within day distance=%.2f mi",repP,centers,withinDayClusterRatio*WithinDayDistance),x,Hfit)  # in case a plot of the centers is desired
  fit0 = ""
  fit0 <- tryCatch({markovchainFit(data=repAvgDayLoc$state)},warning=function(w){flog.debug(sprintf("%s: Fit0 Warning %s rep %s",logPrefix,w))},error=function(e){flog.debug(sprintf("%s: Fit0 Error %s",logPrefix,e))})
  
  # check fit success
  hasFit <- class(fit0)=="list"
  if (hasFit) {
    TM <- as.data.table(slot(fit0$estimate,"transitionMatrix"))  # get the transition matrix from the fit
    
    # for each day in the future use the transition matrix to estimate the next likely state
    currentS <- tail(repAvgDayLoc,n=1)$state
    lastSinInts <- copy(currentS)
  }
  
  flog.debug("%s: finish fitting markov chain, start looping to predict", logPrefix)
  
  # for each day in the future where predictions are required (offLoopSize days)
  for(predictDate in as.list(predictAheadDayList)) # need to loop over list otherwise will not give data object
  {
    # get predict day of week
    predDOW <- weekdays(predictDate)
    
    ff <- calendarLocs[date==predictDate]
    flog.debug("%s predictDay:%s has %s calendar events before check calAdprobThreshold", logPrefix, as.character(predictDate), nrow(ff))
    # if calendarAdherenceOn further check if prob greater than threshold
    if (nrow(ff)>0 & calendarAdherenceOn) {
      ff <- ff[facilityProb>=calAdprobThreshold]
      flog.debug("%s predictDay:%s has %s calendar events after check calAdprobThreshold", logPrefix, as.character(predictDate), nrow(ff))
    }
    if(dim(ff)[1]>0) {# if there are planned visits for this rep in a future day
      flog.debug("%s predictDay:%s(%s)(calendar) use calendar events to predict", logPrefix, as.character(predictDate),predDOW)
      if (withDOW) {
        dy <- grep(predDOW,nodeLocs$state)
      } else {
        dy <- seq(1,dim(nodeLocs)[1],1)
      }
      if(length(dy)>0) { # if there are state on this on this weekday
        # this code is for planned visits choose node closest to average location planned
        planLocs <- data.table(aveLong=mean(ff$longitude),aveLat=mean(ff$latitude),state=0)
        # nodeLocs <- x[!duplicated(x$state),list(aveLong,aveLat,state)]
        locs <- rbind(planLocs,nodeLocs[dy])
        dd <- distm(locs[,list(aveLong,aveLat)])
        s <- nodeLocs$state[dy[which(min(dd[1,2:dim(dd)[2]])==dd[1,2:dim(dd)[2]])[1]]]
        predictLocs <- repLoc[state==s & !(facilityId %in% ff$facilityId),c("facilityId","latitude","longitude","state","localProb"),with=F]
        setnames(predictLocs,"localProb","facilityProb")
        # add result
        if (nrow(predictLocs)>0) {
          predictResult <- rbind(predictResult, data.table(repId=repP, date=predictDate, clusterProb=1, maxNearDistance=maxNearDistance, maxFarDistance=maxFarDistance, accountId=NA, predictLocs[,c("facilityId","latitude","longitude","state","facilityProb")], source=source))
        } else {
          flog.debug("%s predictDay:%s(%s)(calendar) no additioanl facility predicted using calendar event, skip to next date", logPrefix, as.character(predictDate),predDOW)
        }
        # update currentS
        currentS <- s
        predictedStates[[as.character(predictDate)]] <- sprintf("%s(calendar)",currentS)
      } else {
        flog.debug("%s predictDay:%s(%s)(calendar) not have states on the same weekday markov chain, skip to next date", logPrefix, as.character(predictDate),predDOW)
        predictedStates[[as.character(predictDate)]] <- "NA(calendar, no states on the same weekday)"
      }
      next
      
    } else if (hasFit) { # if there is no planned visit
      flog.debug("%s predictDay:%s(%s)(calendar) no calendar event, use history clustering to predict", logPrefix, as.character(predictDate),predDOW)
      tm <- TM[which(colnames(TM)==currentS)[1],]
      # these are the states in MC from the current state
      if (withDOW) {
        dy <- grep(predDOW,colnames(tm))
      } else {
        dy <- seq(1,dim(tm)[2],1)
      }
      if(length(dy)==0) {
        flog.debug("%s predictDay:%s(%s)(history) not have states on the same weekday markov chain, skip to next date", logPrefix, as.character(predictDate),predDOW)
        predictedStates[[as.character(predictDate)]] <- "NA(history, no states on the same weekday)"
        next  # if the desired day of prediction is not one of states in MC move on
      }
      
      nxtState <- names(which.max(tm[,dy,with=F]))
      maxProb <- as.numeric(tm[,nxtState,with=F])
      
      if (maxProb==0) {
        flog.debug("%s predictDay:%s(%s)(history) having prob=0 for all %s->%s_cluster, skip to next date", logPrefix, as.character(predictDate),predDOW,currentS,predDOW)
        predictedStates[[as.character(predictDate)]] <- "NA(history, all states prob=0)"
        next
      }
      
      predictLocs <- repLoc[state==nxtState,]
      predictLocs$facilityProb <- predictLocs$localProb * maxProb
      
      predictResult <- rbind(predictResult, data.table(repId=repP, date=predictDate, clusterProb=maxProb, maxNearDistance=maxNearDistance, maxFarDistance=maxFarDistance, accountId=NA, predictLocs[,c("facilityId","latitude","longitude","state","facilityProb")], source=source))
      
      currentS <- nxtState
      predictedStates[[as.character(predictDate)]] <- sprintf("%s(history)",currentS)
    } else {
      flog.debug("%s predictDay:%s not have calender events also no fitted markov chain, cannot predict", logPrefix, as.character(predictDate))
      predictedStates[[as.character(predictDate)]] <- "NA(history, no fitted markov chain)"
    }
  }
  
  flog.info("%s finish prediction, maxNearDistance=%s, maxFarDistance=%s, withinDayDistance=%s, clustercenters=%s->%s, intsCount=%s->%s, predicted cluster center:[%s]->%s", logPrefix,maxNearDistance, maxFarDistance, WithinDayDistance, centers, nrow(nodeLocs), nrow(repLoc), nrow(repAvgDayLoc), lastSinInts, paste(predictedStates,collapse="->"))
  
  return (list(predictResult=predictResult, predictedStates=predictedStates))
}

#########################
# func: calcualteAnchor.addCalendarAdherence
########################
calcualteAnchor.addCalendarAdherence <- function(calendarAdherenceOn, calendarLocs, repCalendarAdherence, repAccountCalendarAdherence, calAdconfThreshold, logPrefix=""){
  
  # add default calendar adherence prob
  DEFAULT_COMPLETE_PROB <- 1
  calendarLocs[,`:=`(facilityProb=DEFAULT_COMPLETE_PROB, clusterProb=DEFAULT_COMPLETE_PROB)]
  if (!calendarAdherenceOn) {
    return(calendarLocs)
  }
  
  # get repId from calendarLocs
  repid <- calendarLocs$repId[1]
  
  # check if repCalendarAdherence
  if (nrow(repCalendarAdherence)<=0) {
    flog.info("repCalendarAdherence is empty, cannot get calcualted rep calendar adherence, use default")
    return(calendarLocs)
  }
  
  # subset repCalendarAdherence & repAccountCalendarAdherence use repid
  repCalAdherence <- repCalendarAdherence[repId==repid]
  repAccountCalAdherence <- repAccountCalendarAdherence[repId==repid]
  if (nrow(repCalAdherence)<=0) {
    flog.debug("%s: no calculated calendar adherence for this rep, use default", logPrefix)
    return(calendarLocs)
  }
  
  # get rep-level calendar adherence & its confidence
  repCalAd <- repCalAdherence$completeProb
  repCalAdConf <- repCalAdherence$completeConfidence
  # check if rep-level calendar adherence above threhold, if not set to default value
  if (repCalAdConf<calAdconfThreshold) {repCalAd <- DEFAULT_COMPLETE_PROB}
  
  # check rep+account level calendar adherence first
  if (nrow(repAccountCalendarAdherence)>0) {
    calendarLocs <- merge(calendarLocs, repAccountCalAdherence[,c("repId","accountId","completeProb","completeConfidence")], by=c("repId","accountId"), all.x=TRUE)
    # set those confidence level lowerthan threhold back to NA
    calendarLocs[completeConfidence<calAdconfThreshold, `:=`(completeProb=NA, completeConfidence=NA)]
    # fill NA ones with rep level calendar adhernce
    calendarLocs[is.na(completeConfidence), `:=`(completeProb=repCalAd, completeConfidence=repCalAdConf)]
  } else {
    # directly add rep level calendar adhernce
    calendarLocs[,`:=`(completeProb=repCalAd, completeConfidence=repCalAdConf)]
  }
  
  # add calendar ahereence as clusterProb & facilityProb & cleanup
  calendarLocs[, facilityProb:=completeProb]
  calendarLocs[, c("completeProb", "completeConfidence"):=NULL]
  calendarLocs[, clusterProb:=mean(facilityProb)]
  
  flog.debug("%s: finish add calendar adherence", logPrefix)
  
  return(calendarLocs)
}

########################
# main fucntion
########################

calculateAnchor <- function(INTERACTIONS, future, predictRundate, predictAheadDayList, numberCores, withinDayClusterRatio, maxFarPercentile, minIntsRequire, calendarAdherenceOn, repCalendarAdherence, repAccountCalendarAdherence, calAdprobThreshold, calAdconfThreshold) {
  
  flog.info("Start calculateAnchor:")
  
  # result default
  resultList <- list(result=data.table(), newRepList=data.table())
  
  # process INTERACTIONS
  flog.info("calculate day average latitude and longitude")
  # INTERACTIONS[,c("aveLat","aveLong"):=list(mean(latitude),mean(longitude)),by=list(repId,date)]
  INTERACTIONS_day <- INTERACTIONS[,.(aveLat=mean(latitude),aveLong=mean(longitude)),by=list(repId,date)]
  setkey(INTERACTIONS,repId)
  setkey(INTERACTIONS_day,repId)
  
  flog.info("filter out newRep")
  # get rep interactions record count
  repIntsCount <- INTERACTIONS[, .(cnt=.N), by="repId"]
  # new rep list
  newReps <- repIntsCount[cnt<=minIntsRequire]$repId
  if (length(newReps)>0) {
    newRepListNoEnoughInts <- data.table(repId=newReps, source="territory_noEnoughInts", predictAheadDayList=as.character(NA), stringsAsFactors=FALSE)
  } else {
    newRepListNoEnoughInts <- data.table()
  }
  flog.info("adding %s new reps defined as interactions<=%s to newRepList", nrow(newRepListNoEnoughInts), minIntsRequire)
  
  # check if can go through regular anchor prediction
  if (nrow(repIntsCount[cnt>minIntsRequire]) <= 0) {
    flog.info("no reps have more than %s interactions, skip regular anchor prediction", minIntsRequire)
    
    resultList <- list(result=data.table(), newRepList=newRepListNoEnoughInts)
    flog.info("Return from calculateAnchor: dim(result)=(%s), nrow(newRepList)=%s", paste(dim(resultList[['result']]),collapse=","), nrow(resultList[['newRepList']]))
    return(resultList)
  }
  
  # parallel setup
  # for anchor to predict, it should have more than 50 records
  numberCores <- min(numberCores, nrow(repIntsCount[cnt>minIntsRequire]))
  registerDoMC(numberCores)  # setup the number of cores to use
  N <- data.table(reps=repIntsCount[cnt>minIntsRequire]$repId,group=1:numberCores)
  print("print processed rep list for each core:")
  print(paste(as.list(N), collapse=','))
  flog.info("Predict for %s/%s Reps (new rep deined as <= %s ints record)", nrow(N), nrow(repIntsCount), minIntsRequire)
  # add custom combine function
  combine_custom <- function(...) {
    result <- rbindlist(lapply(list(...), function(x) x$result))
    newRepList <- rbindlist(lapply(list(...), function(x) x$newRepList))
    return(list(result = result, newRepList = newRepList))
  }
  
  flog.info('start parallel processing')
  
  resultList <- foreach(nc = 1:numberCores , .combine=combine_custom, .multicombine=TRUE, .init=list(result=data.table(), newRepList=newRepListNoEnoughInts)) %dorng%                   # loop for each core
  {
    interactions <- INTERACTIONS[repId %in% N[group==nc]$reps]
    interactions_day <- INTERACTIONS_day[repId %in% N[group==nc]$reps]
    
    # allocate the result table
    result <- list()
    newRepList <- list()
    # tmpl <- length(unique(interactions$repId))
    # for(k in 1:tmpl)result[[k]] <- list(NULL)
    resultPointer <<- 1
    resultPointerNewRep <<-1
    
    # replist to be looped
    reps <- unique(interactions$repId)
    
    # loop for each unique rep in the interactions table
    for(repP in reps)
    {
      flog.debug("Core %s: Start processing for repId=%s",nc,repP)
      logPrefix <- sprintf("Core %s: repId=%s", nc, repP)
      
      # pull out the current reps data
      repLoc <- interactions[repId==repP,list(latitude,longitude,date,facilityId)]
      repLoc[,dow:=weekdays(date)]
      repAvgDayLoc <- interactions_day[repId==repP & !is.na(aveLat),list(aveLong,aveLat,date)]
      repAvgDayLoc[,dow:=weekdays(date)]
      
      # calculate the aveMaxDailyTravel and withinDayDistance
      temp <- sapply(repAvgDayLoc$date,function(y){ux <- unique(repLoc[date==y,list(longitude,latitude)]);return(c(max(distm(ux)), sum(distm(ux))/dim(ux)[1])) })
      maxFarDistance <- quantile(temp[1,], maxFarPercentile)
      maxNearDistance <- mean(temp[1,][temp[1,] <= maxFarDistance])
      if(maxNearDistance == maxFarDistance) maxFarDistance <- NA
      WithinDayDistance <- median(temp[2,])/withinDayClusterRatio
      flog.debug("%s maxNearDistance=%s, maxFarDistnace=%s, withinDayDistance=%s", logPrefix, maxNearDistance, maxFarDistance, WithinDayDistance)
      
      # check whether has future planned visits, and write to results if any
      calendarLocs <- future[repId==repP & date %in% predictAheadDayList]
      if(dim(calendarLocs)[1]>0) {# if there are planned visits for this rep in a future day
        # compose calendar planned visits output
        calendarLocs <- calendarLocs[,c("repId","date","accountId","facilityId","latitude","longitude")]
        calendarLocs$state <- NA
        calendarLocs$source <- 'calendar'
        calendarLocs$maxNearDistance <- maxNearDistance
        calendarLocs$maxFarDistance <- maxFarDistance
        flog.debug("%s has %s planned visit in the predict ahead time frame", logPrefix, dim(calendarLocs)[1])
        # add calendar adherence
        calendarLocs <- calcualteAnchor.addCalendarAdherence(calendarAdherenceOn, calendarLocs, repCalendarAdherence, repAccountCalendarAdherence, calAdconfThreshold, logPrefix)
        
        # add to result list
        calendarLocs <- calendarLocs[,c("repId","date","clusterProb","maxNearDistance","maxFarDistance","accountId","facilityId","latitude","longitude","state","facilityProb","source")]
        result[[resultPointer]] <- calendarLocs
        resultPointer <<- resultPointer+1
      }
      
      # cluster
      repAvgDayLocClustering <- copy(repAvgDayLoc)
      repAvgDayLocClustering[,date:=NULL]
      
      # these functions are for mapping the height in hierarchical clustering to miles
      dist <- distm(repAvgDayLocClustering[,c("aveLong","aveLat"),with=F])  # use geosphere to come up with distance between each of the daily centroids
      centers <- 1
      Hfit <- ""
      if(dim(dist)[1]>2)
      {
        Hfit <- hclust(as.dist(dist)) # hierarchical cluster of distance matrix
        for(root in rev(Hfit$height))       # loop through the heights that define the clusters in the hclust() output
        {
          repAvgDayLocClustering$state <- cutree(Hfit,h=root)
          if(median(sapply(unique(repAvgDayLocClustering$state),function(x)max(distm(repAvgDayLocClustering[state==x,list(aveLong,aveLat)]))))<WithinDayDistance)break
        }
        centers <- max(repAvgDayLocClustering$state) 
      }
      if(centers==1)repAvgDayLocClustering$state <- 1
      
      flog.debug("%s finished clustering got %s centers", logPrefix, centers)
      
      # append the states to the individual records
      repAvgDayLoc <- cbind(repAvgDayLoc,repAvgDayLocClustering[,"state"])
      # add dow to state
      repAvgDayLoc[,stateNoDOW:=state]
      repAvgDayLoc[,state:=paste(state,dow,sep="_")]
      # append the states to individual facility results
      repLoc <- merge(repLoc,repAvgDayLoc[,c("date","state","stateNoDOW")],by="date",all.x=TRUE)
      flog.debug("%s after addind dayOfWeek, cluster centers = %s", logPrefix, length(unique(repAvgDayLoc$state)))
      
      # predict
      resultList <- calculateAnchor.Predict(calendarAdherenceOn, calAdprobThreshold, repAvgDayLoc, repLoc, predictAheadDayList, calendarLocs, repP, logPrefix, maxNearDistance, maxFarDistance, WithinDayDistance, centers, withDOW=TRUE, sourceSuffix=NA)
      predictedStates <- resultList[['predictedStates']]
      # add to result list
      if (nrow(resultList[['predictResult']])>0) {
        result[[resultPointer]] <- resultList[['predictResult']] 
        resultPointer <<- resultPointer+1
      } else {
        flog.debug("%s has no prediction with DOW", logPrefix)
      }
      
      
      ################ check prediciton result, if missing any prediction for one day, try predicting without DOW
      noPredictInd <- startsWith(predictedStates, 'NA') | is.na(predictedStates)
      if (!any(noPredictInd)) {
        flog.debug("%s has all predictions with DOW, not run without DOW mode", logPrefix)
      } else {
        # log no prediction date
        noPredictDays <- as.Date(names(predictedStates[noPredictInd]))
        flog.debug("%s (noDOW) use marchov chain without DOW mode to fill %s missing prediction for: %s", logPrefix, length(noPredictDays), paste(noPredictDays, collapse=', '))
        # change state to original state without DOW
        repLoc[,state:=stateNoDOW]
        repAvgDayLoc[,state:=stateNoDOW]
        # predict
        resultListNoDOW <- calculateAnchor.Predict(calendarAdherenceOn, calAdprobThreshold, repAvgDayLoc, repLoc, predictAheadDayList, calendarLocs, repP, logPrefix, maxNearDistance, maxFarDistance, WithinDayDistance, centers, withDOW=FALSE, sourceSuffix='noDOW')
        # add to result list
        if (nrow(resultListNoDOW[['predictResult']])>0) {
          predictResultNoDOW <- resultListNoDOW[['predictResult']][date %in% noPredictDays, ]
          if (nrow(predictResultNoDOW)>0) {
            result[[resultPointer]] <- predictResultNoDOW
            resultPointer <<- resultPointer+1
          } else {
            flog.debug("%s no additional prediction with noDOW as no prediction in the missing date list", logPrefix)
          }
        } else {
          flog.debug("%s no additional prediction with noDOW as no prediction", logPrefix)
        }
        
        ################ check prediciton result, if missing any prediction for one day, try predicting with K-means
        predictedStatesNoDOW <- resultListNoDOW[['predictedStates']]
        noPredictIndNoDOW <- startsWith(predictedStatesNoDOW, 'NA') | is.na(predictedStatesNoDOW)
        noPredictInd <- noPredictInd & noPredictIndNoDOW
        if (any(noPredictInd)) {
          noPredictDays <- as.Date(names(predictedStates[noPredictInd]))
          flog.info("%s (noDOW) still missing %s prediction for %s", logPrefix, length(noPredictDays), paste(noPredictDays, collapse=', '))
          
          # run prediciton with k-means clustering
          # clean previous clustering result
          repLoc <- interactions[repId==repP,list(latitude,longitude,date,facilityId)]
          repAvgDayLoc <- interactions_day[repId==repP & !is.na(aveLat),list(aveLong,aveLat,date)]
          repAvgDayLocClustering <- copy(repAvgDayLoc)
          repAvgDayLocClustering[,date:=NULL]
          repAvgDayLocClustering <- unique(repAvgDayLocClustering)
          # k means clustering
          centers <- min(15,trunc(dim(repAvgDayLocClustering)[1]/3))
          repAvgDayLocClustering$state <- ifelse(centers>2,kmeans(repAvgDayLocClustering[,1:2,with=FALSE],centers=centers),data.table(cluster=1))
          flog.debug("%s finished K-Means clustering with %s centers", logPrefix, centers)
          # append clustering result
          repAvgDayLoc <- merge(repAvgDayLoc,repAvgDayLocClustering, by=c("aveLong","aveLat"), all.x=TRUE)
          # append the states to individual facility results
          repLoc <- merge(repLoc,repAvgDayLoc[,c("date","state")],by="date",all.x=TRUE)
          
          # predict
          resultListKmeans <- calculateAnchor.Predict(calendarAdherenceOn, calAdprobThreshold, repAvgDayLoc, repLoc, predictAheadDayList, calendarLocs, repP, logPrefix, maxNearDistance, maxFarDistance, WithinDayDistance, centers, withDOW=FALSE, sourceSuffix='kmeans_noDOW', tryFitHigherOrder=TRUE)
          # add to result list
          if (nrow(resultListKmeans[['predictResult']])>0) {
            predictResultKmeans <- resultListKmeans[['predictResult']][date %in% noPredictDays, ]
            if (nrow(predictResultKmeans)>0) {
              result[[resultPointer]] <- predictResultKmeans
              resultPointer <<- resultPointer+1
            } else {
              flog.debug("%s no additional prediction with K-means clustering as no prediction in the missing date list", logPrefix)
            }
          } else {
            flog.debug("%s no additional prediction with K-means clustering as no prediction", logPrefix)
          }
          # log still missing result
          predictedStatesKmeans <- resultListKmeans[['predictedStates']]
          noPredictIndKmeans <- startsWith(predictedStatesKmeans, 'NA') | is.na(predictedStatesKmeans)
          noPredictInd <- noPredictInd & noPredictIndKmeans
          if (any(noPredictInd)) {
            noPredictDays <- as.Date(names(predictedStates[noPredictInd]))
            newRepList[[resultPointerNewRep]] <- data.table(repId=repP, source='territory_clusterNoPred', predictAheadDayList=paste(noPredictDays,collapse=','))
            resultPointerNewRep <<- resultPointerNewRep + 1
            flog.info("%s (kmeans_noDOW) still missing %s prediction for %s", logPrefix, length(noPredictDays), paste(noPredictDays, collapse=', '))
          }
        }
        
      }
    }
    
    # compose result
    if(resultPointer>1) result <- rbindlist(result[1:(resultPointer-1)]) else result <- data.table()
    if(resultPointerNewRep>1) newRepList <- rbindlist(newRepList[1:(resultPointerNewRep-1)]) else newRepList <- data.table()
    flog.info("Core %s: finish processing, dim(result)=(%s), nrow(newRepList)=%s", nc, paste(dim(result),collapse=","), nrow(newRepList))
    return(list(result=result, newRepList=newRepList))
  }
  
  flog.info("Return from calculateAnchor: dim(result)=(%s), nrow(newRepList)=%s", paste(dim(resultList[['result']]),collapse=","), nrow(resultList[['newRepList']]))
  return(resultList)
}
