from common.DataAccessLayer.DataAccessLayer import MessageAccessor
from common.DataAccessLayer.DataAccessLayer import SegmentAccessor
import logging


DEFAULT_SEGMENT_MESSAGE_SEQ_UID = 'NONE'
DEFAULT_SEGMENT_SEQ_SCORE = 0.0

logger = None

class SimulationScoreAggregator:
    """
    This class is responsible for generating simulation scores for segments by aggregating the results for accounts.
    """

    mso_build_uid = None
    sim_run_uid = None

    def __init__(self, sim_run_uid, mso_build_uid, simulation_logger_name):

        global logger
        logger = logging.getLogger(simulation_logger_name)

        self.sim_run_uid = sim_run_uid
        self.mso_build_uid = mso_build_uid

    def __aggregate_account_scores_for_segment(self, segment_uid):
        """
        This function will calcuate and aggregated score the specified segment UID and write the results back into
        database table.
        :param segment_uid:
        :return:
        """
        # Get the database accessor objects
        segment_accessor = SegmentAccessor()

        message_accessor = MessageAccessor()

        # Get all the accounts in the segment in the build
        account_uids = segment_accessor.get_accounts_for_segment_uid(segment_uid)

        # TO-TEST::
        #account_uids = account_uids[:10]

        # Average of all the account scores in segment
        average_score = 0

        # Select the max score. Current strategy is select the sequence with maximum score in the segment which can be updated
        # Probabilities will be in between [0-1] hence -1 initialization works
        max_score = -1
        max_score_seq_uid = -1

        # TO-DO:: Update to get scores for all accountUIDs at once

        account_message_seq_df = message_accessor.get_finalized_sequence_and_score_for_accounts(self.sim_run_uid, self.mso_build_uid)
        logger.info("Fetched Account Message Sequence Dataframe")
        logger.debug(account_message_seq_df.head())


        max_score_seq_uid = account_message_seq_df.loc[account_message_seq_df['probability'].idmax()]['messageSeqUID']
        average_score = account_message_seq_df['probability'].mean()

        # Get all the finalized sequences for the build
        for account_uid in account_uids:

            # Check if the dataframe contains the account UID
            if any(account_message_seq_df.accountUID == account_uid):

                # Get the sequence and score for each account
                message_seq_uid = account_message_seq_df.loc[account_message_seq_df['accountUID'] == account_uid, 'messageSeqUID'].iloc[0]
                score = account_message_seq_df.loc[account_message_seq_df['accountUID'] == account_uid]['probability'].iloc[0]

                if message_seq_uid:
                    # Check for max score
                    if max_score < score:
                        max_score = score
                        max_score_seq_uid = message_seq_uid

                    # Accumulate average score
                    average_score = average_score + score

        # Calculate the score for the segment
        average_score = average_score / len(account_uids)

        # Write sequence and score back to database
        segment_accessor.write_simulation_segment_result(self.sim_run_uid, self.mso_build_uid, segment_uid, max_score_seq_uid, average_score)

    def calculate_segment_message_sequence(self):
        """
        This will fetch the segments in the database and start calculation for the segments
        :return:
        """
        logger.info("Aggregating Account Scores for Segments...")
        # Retrieve the list of segment for build uid
        segment_accessor = SegmentAccessor()
        segment_uids = segment_accessor.get_segment_uids(self.sim_run_uid, self.mso_build_uid)

        # Check if there are segment uid exists before processing
        if segment_uids:

            message_accessor = MessageAccessor()
            account_message_seq_df = message_accessor.get_finalized_sequence_and_score_for_accounts(self.sim_run_uid,
                                                                                                    self.mso_build_uid)
            logger.info("Fetched Account Message Sequence Dataframe")
            logger.debug(account_message_seq_df.head())

            logger.info("Fetching Segment-Account Dataframe")
            # Segment accessor from Data access layer
            segment_accessor = SegmentAccessor()

            # Get all the accounts for the segment in the build
            account_uids_df = segment_accessor.get_account_segment_df(self.mso_build_uid,self.sim_run_uid)

            # Iterate through the segments
            for segment_uid in segment_uids:

                logger.info("Aggregating for Segment- " + segment_uid)

                # Set default uid and score
                max_score_seq_uid = DEFAULT_SEGMENT_MESSAGE_SEQ_UID
                average_score = DEFAULT_SEGMENT_SEQ_SCORE

                try:

                    account_uids = account_uids_df[account_uids_df['segmentUID'] == segment_uid]['accountUID']

                    # Check if account ids exists in the segment
                    if not account_uids.empty:

                        # Subset the dataframe
                        account_message_seq_subset_df = account_message_seq_df[account_message_seq_df['accountUID'].isin(account_uids)]

                        # Check if the message sequence dataframe for the accounts is not empty
                        if not account_message_seq_df.empty:
                            # Get the max score message seq id
                            max_score_seq_uid = account_message_seq_subset_df.loc[account_message_seq_subset_df['probability'].idxmax()][
                                'messageSeqUID']

                            # Get the average score of the probabilities
                            average_score = account_message_seq_subset_df['probability'].mean()

                except ValueError as value_error:
                    logger.warn("Incorrect value while aggregating segment sequences- " + str(value_error))

                except Exception as e:
                    logger.error("Exception while aggregating segment sequences- " + str(e))

                # Write sequence and score back to database
                segment_accessor.write_simulation_segment_result(self.sim_run_uid, self.mso_build_uid, segment_uid,
                                                                 max_score_seq_uid, average_score)

            logger.info("Aggregating Account Scores for Segment Completed!")

        else:

            logger.info("There are no segment UIDs to process")


# TO-TEST::
#agg = SimulationScoreAggregator('21e3042c-0a4a-4acf-b908-a88d0afabb78')
#agg.calculate_segment_message_sequence()

