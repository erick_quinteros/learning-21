####################################################################################################################
#
#
# aktana- engagement estimates estimates Aktana Learning Engines.
#
# description: save the result in the appropriate DB
#
# created by : marc.cohen@aktana.com
#
# created on : 2016-11-01
#
# Copyright AKTANA (c) 2016.
#
#
####################################################################################################################

saveEngagementResult <- function(con, con_l, result, BUILD_UID, RUN_UID, configUID, versionUID, isNightly)
{
    library(futile.logger)

    flog.info("Now saveEngagementResult ...")

    if (is.null(result) || nrow(result) == 0)
    {
       flog.info("engagement result is empty. Nothing to save!")
       return (0)
    }

    if (isNightly) # nightly run will update DSE table
    {
      FIELDS <- list(repActionTypeId="integer",repId="integer",accountId="integer",suggestionType="varchar(45)",learningRunUID="varchar(80)",date="date",probability="double",runDate="date",createdAt="datetime", updatedAt="datetime")

      flog.info("Rows in engagement result: %s", nrow(result))

      # remove column from result
      result$learningBuildUID <- NULL

      # rename columns
      setnames(result, c("repUID", "accountUID"), c("repId", "accountId"))

      # convert data types
      result[, c("repId", "accountId") := list(as.integer(repId), as.integer(accountId))] 

      nowTime <- format(Sys.time(), "%Y-%m-%d %H:%M:%S")
      result$runDate   <- today+1
      result$createdAt <- nowTime
      result$updatedAt <- nowTime

      # write back to table (overwrite)
      startTimer <- Sys.time()
      tryCatch(dbGetQuery(con, "TRUNCATE TABLE RepAccountEngagement;"),
                 error = function(e) {
                   flog.error('Error in truncate RepAccountEngagement in DSE: %s', e, name='error')
                   dbDisconnect(con)
                   quit(save = "no", status = 67, runLast = FALSE) # user-defined error code 67 for failure of writing back to database table
                 })   
        
      flog.info("Delete old scores Time = %s", Sys.time()-startTimer)

      tryCatch(dbWriteTable(con,name="RepAccountEngagement", value=as.data.frame(result), overwrite=FALSE, append=TRUE, row.names=FALSE, field.types=FIELDS),
               error = function(e) {
                 flog.error('Error in writing back to table RepAccountEngagement in DSE!', name='error')
                 dbDisconnect(con)
                 quit(save = "no", status = 67, runLast = FALSE) # user-defined error code 67 for failure of writing back to database table
               })
      
    }
    else # manual run will update Learning DB table
    {
      repIdMap <- data.table(dbGetQuery(con,"SELECT r.repId, r.externalId FROM Rep as r JOIN RepTeamRep using (repId) JOIN RepTeam rtr using (repTeamId) WHERE isActivated=1;"))

      accountIdMap <- data.table(dbGetQuery(con,"SELECT accountId, externalId FROM Account WHERE isDeleted=0;"))

      setnames(result, old = c("accountUID", "repUID"), new = c("accountId", "repId"))

      # replace accountId with accountUID
      result <- merge(result, accountIdMap, by="accountId")
      setnames(result, "externalId", "accountUID")
      result$accountId  <- NULL

      # replace repId with repUID
      result <- merge(result, repIdMap, by="repId")
      setnames(result, "externalId", "repUID")
      result$repId  <- NULL

      # delete old data first
      SQL <- sprintf("DELETE FROM RepAccountEngagement WHERE learningBuildUID='%s'", BUILD_UID)
      dbGetQuery(con_l, SQL)  # remove old build records in Learning DB

      FIELDS <- list(learningRunUID="varchar(80)",learningBuildUID="varchar(80)",repActionTypeId="integer",repUID="varchar(80)",accountUID="varchar(80)",suggestionType="varchar(45)",date="date",probability="double", createdAt="datetime", updatedAt="datetime")

      nowTime <- format(Sys.time(), "%Y-%m-%d %H:%M:%S")
      result$createdAt <- nowTime
      result$updatedAt <- nowTime

      # save data into RepAccountEngagement table in Learning DB.
      tryCatch(dbWriteTable(con_l,name="RepAccountEngagement", value=as.data.frame(result), overwrite=FALSE, append=TRUE, row.names=FALSE, field.types=FIELDS),
               error = function(e) {
                 flog.error('Error in writing back to table RepAccountEngagement in Learning DB!', name='error')
                 dbDisconnect(con_l)
                 quit(save = "no", status = 67, runLast = FALSE) # user-defined error code 67 for failure of writing back to database table
               })  
    }

}
