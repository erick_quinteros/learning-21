from datetime import datetime

def validate_date_format(date, date_description):
    # check that dates are correct format
    try:
        datetime.strptime(date, '%Y-%m-%d')
    except ValueError:
        raise ValueError("Incorrect format for " + date_description + ", should be: YYYY-MM-DD but input is: " + date)
