from awsglue.transforms import *
from pyspark.sql.functions import *
from pyspark.sql import SQLContext

from pyspark.sql.types import *
from pyspark.sql import Row, functions as F
from pyspark.sql.window import Window
from pyspark.sql.functions import when, lit, col
import pandas as pd
from pyspark.sql.types import IntegerType
from pyspark.sql.functions import col, expr, when
from utils import Utils
import logging
import sys

logger = None
# Needs to be read with a an argument from the job

class StRecordClassTransCri:
    def __init__(self, glue_context, customer, s3_destination, target):
        self.glueContext = glue_context
        self.customer = customer
        self.s3_destination = s3_destination
        self.utils = Utils(glue_context, customer)
        self.spark = self.glueContext.spark_session
        self.target = target

        global logger
        logger = logging.getLogger("ADL_DEV_LOGGER")
        pass

    def run(self):
        try:
            target = self.target

            sourcetable = "recordclasses_dse"
            df_rc = self.utils.dataLoadDelta(self.s3_destination + "data/silver/" + sourcetable)
            df_rc = df_rc.withColumn('All_effectivedate', to_date(df_rc.All_effectivedate, 'yyyy-MM-dd'))
            df_rc.cache()
            df_rc.createOrReplaceTempView("rc")
            logger.info("rc cached")

            # get all the rep accounts from the dataset
            reps_accounts = df_rc.select("Interaction_rep_account").distinct()
            reps_accounts.count()
            # get all months
            months = df_rc.filter(df_rc.Interaction_YearMonth.between(201101, 204001)).select(
                "Interaction_YearMonth").distinct().orderBy(["Interaction_YearMonth"], ascending=[1])
            master = reps_accounts.select('Interaction_rep_account').distinct().crossJoin(
                months.select('Interaction_YearMonth')).distinct()
            # master=master.withColumn("Interaction_YearMonth", master["Interaction_YearMonth"].cast(IntegerType()))

            # ----------------------------------------------------------------------------------------------------------------------------
            # metric one
            # Email Open score

            # For each Rep_Account, calculate the total Email sent Windows F.count(recordclass)
            # For each Rep_Account, Month sum the Email Open using Windows F.sum(Email_open)

            emails = df_rc.where((col("Email_Status_vod__c").isin({"Delivered_vod", "Sent_vod"})) & (
                        col("interactionTypeId") == "11")).select("recordclass", "interactionTypeId", "Email_Opened_vod__c",
                                                                  "Interaction_repid", "Interaction_accountId",
                                                                  "Interaction_rep_account", "Email_Product_vod__c",
                                                                  "interactionmonth", "interactionyear",
                                                                  "Interaction_YearMonth", "Interaction_startDateLocal",
                                                                  F.count("recordclass").over(
                                                                      Window.partitionBy("Interaction_rep_account",
                                                                                         "Interaction_YearMonth")).alias(
                                                                      "sent_year_month"), F.sum("Email_Opened_vod__c").over(
                    Window.partitionBy("Interaction_rep_account").orderBy('Interaction_YearMonth').rangeBetween(
                        Window.unboundedPreceding, 0)).alias("Open_Total").cast(IntegerType()))

            # Before calculating the Email Open Score Choose last entry in the month as final value
            w1 = Window.partitionBy("Interaction_rep_account", "Interaction_YearMonth").orderBy(
                F.desc("Interaction_startDateLocal"))
            emails2 = emails.withColumn("rn", F.row_number().over(w1)).filter("rn=1").select("recordclass",
                                                                                             "interactionTypeId",
                                                                                             "Email_Opened_vod__c",
                                                                                             "Interaction_repid",
                                                                                             "Interaction_accountId",
                                                                                             "Interaction_rep_account",
                                                                                             "Email_Product_vod__c",
                                                                                             "interactionmonth",
                                                                                             "interactionyear",
                                                                                             "Interaction_YearMonth",
                                                                                             "sent_year_month",
                                                                                             "Open_Total")

            # number of emails sent by every rep_account [Total_Emails_Sent] usinf F.sum("sent_year_month")
            emails2 = emails2.select("recordclass", "interactionTypeId", "Email_Opened_vod__c", "Interaction_repid",
                                     "Interaction_accountId", "Interaction_rep_account", "Email_Product_vod__c",
                                     "interactionmonth", "interactionyear", "Interaction_YearMonth", "sent_year_month",
                                     "Open_Total", F.sum("sent_year_month").over(
                    Window.partitionBy("Interaction_rep_account").orderBy('Interaction_YearMonth').rangeBetween(
                        Window.unboundedPreceding, 0)).alias("Sent_Total"))

            # Finally Email Open score which is Total Emails Opened/ Total Emails Sent
            emails2 = emails2.withColumn("Email_open_score", when(col("Sent_Total") >= 3,
                                                                  F.round(emails2.Open_Total / emails2.Sent_Total,
                                                                          2)).otherwise(lit(None)))

            # update the probabilities down through time
            # look back the last Email_Open_Score which was not null for that rep_account
            emails2 = emails2.withColumn("Email_open_score", F.last('Email_open_score', True).over(
                Window.partitionBy('Interaction_rep_account').orderBy('Interaction_YearMonth').rowsBetween(-sys.maxsize,
                                                                                                           0)))

            emails_final = emails2.select("Interaction_rep_account", "Interaction_YearMonth",
                                          "Email_open_score").drop_duplicates()

            master = master.join(emails_final, (master.Interaction_rep_account == emails_final.Interaction_rep_account) & (
                        master.Interaction_YearMonth == emails_final.Interaction_YearMonth), 'left_outer').select(
                master["*"], emails_final.Email_open_score).dropDuplicates()
            master = master.withColumn("Email_open_score", F.last('Email_open_score', True).over(
                Window.partitionBy('Interaction_rep_account').orderBy('Interaction_YearMonth').rowsBetween(-sys.maxsize,
                                                                                                           0)))

            # ----------------------------------------------------------------------------------------------------------------------------

            # Metric 2: Product Tenure
            df_rc = df_rc.withColumn('Interaction_startDateLocal_dt',
                                     to_date(df_rc.Interaction_startDateLocal, 'yyyy-MM-dd'))
            df_rc = df_rc.orderBy(["Interaction_rep_account", 'Interaction_startDateLocal_dt'], ascending=[1, 1])

            # Find time stamps for first and last interactions
            # min,max,previous event
            tenure = df_rc.select("recordclass", "interactionId", "Interaction_repid", "Interaction_accountId",
                                  "Interaction_rep_account", "interactionTypeId", "repActionTypeId",
                                  "Interaction_startDateTime", "Interaction_startDateLocal_dt", "Interaction_isCompleted",
                                  "Interaction_YearMonth", F.min("Interaction_startDateLocal_dt").over(
                    Window.partitionBy("Interaction_rep_account")).alias("eventFirst"),
                                  F.max("Interaction_startDateLocal_dt").over(
                                      Window.partitionBy("Interaction_rep_account", "Interaction_YearMonth")).alias(
                                      "eventLast"), F.lag("Interaction_startDateLocal_dt").over(
                    Window.partitionBy("Interaction_rep_account").orderBy("Interaction_startDateLocal_dt")).alias(
                    "eventPrevious"))

            # calculate tenure score by subtracting lastevent from Firstevent
            tenure = tenure.withColumn("tenure_score", datediff(tenure.eventLast, tenure.eventFirst))

            tenure2 = tenure.select("Interaction_rep_account", "tenure_score", "Interaction_YearMonth").distinct()

            # update tenure score down the time by looking at previous null value for that rep_account
            tenure2.withColumn("tenure_score", F.last('tenure_score', True).over(
                Window.partitionBy('Interaction_rep_account').orderBy('Interaction_YearMonth').rowsBetween(-sys.maxsize,
                                                                                                           0)))

            tenure2 = tenure2.select("Interaction_rep_account", "Interaction_YearMonth", "tenure_score").drop_duplicates()

            master = master.join(tenure2, (master.Interaction_rep_account == tenure2.Interaction_rep_account) & (
                        master.Interaction_YearMonth == tenure2.Interaction_YearMonth), 'left_outer').select(master["*"],
                                                                                                             tenure2.tenure_score).dropDuplicates()
            master = master.withColumn("tenure_score", F.last('tenure_score', True).over(
                Window.partitionBy('Interaction_rep_account').orderBy('Interaction_YearMonth').rowsBetween(-sys.maxsize,
                                                                                                           0)))

            # ----------------------------------------------------------------------------------------------------------------------------

            # metric 3 completed visits only
            # filter visit records based on interactiontype='4'
            visits = df_rc.where(col("interactionTypeId") == '4').select("interactionId", "interactionTypeId",
                                                                         "Interaction_repid", "Interaction_accountId",
                                                                         "Interaction_rep_account",
                                                                         "Interaction_startDateLocal_dt",
                                                                         "Interaction_isCompleted", "Interaction_isDeleted",
                                                                         "Interaction_YearMonth")
            # get only compeletd visits
            visits = visits.where(col("Interaction_isCompleted") == 1).distinct()

            # choose first in group where more than one visit to the same HCP is logged for the day
            visits = visits.select(visits["*"], F.row_number().over(
                Window.partitionBy("Interaction_rep_account", "Interaction_startDateLocal_dt").orderBy(
                    "Interaction_rep_account", "Interaction_startDateLocal_dt")).alias(
                "visits_sameday_sameRepAcct")).filter(col("visits_sameday_sameRepAcct") == 1)

            # find total count of all visits for that rep_account and total count of all visits per month
            visits = visits.select(visits["*"],
                                   F.count('interactionId').over(Window.partitionBy("Interaction_rep_account")).alias(
                                       "visit_count"), F.count('interactionId').over(
                    Window.partitionBy("Interaction_rep_account", "Interaction_YearMonth")).alias("rep_account_count"))

            # test
            # visits.where((col("Interaction_rep_account")=="1057_53146") & (col("Interaction_startDateLocal_dt")=="2019-02-08")).show()

            visits2 = visits.select("Interaction_rep_account", "Interaction_YearMonth", "interactionTypeId", "visit_count",
                                    "rep_account_count").distinct()
            visits2 = visits2.withColumn("Interaction_YearMonth", visits2["Interaction_YearMonth"].cast(IntegerType()))

            # for every rep_account get the first_month and last_month interaction
            visits2 = visits2.select(visits2["*"], F.min("Interaction_YearMonth").over(
                Window.partitionBy("Interaction_rep_account")).alias("first_month"), F.max("Interaction_YearMonth").over(
                Window.partitionBy("Interaction_rep_account")).alias("last_month"))

            # join visits with reps table
            visits3 = master.join(visits2, (visits2.Interaction_rep_account == master.Interaction_rep_account) & (
                        visits2.Interaction_YearMonth == master.Interaction_YearMonth), 'left_outer').select(
                master.Interaction_rep_account, master.Interaction_YearMonth, "interactionTypeId", visits2.visit_count,
                visits2.rep_account_count, visits2.first_month, visits2.last_month)

            # define the window for each first_month, if it's null look bacl previous month to assign it
            window = Window.partitionBy('Interaction_rep_account').orderBy('Interaction_YearMonth')
            # define the forward-filled column
            filled_column_first = last(visits3['first_month'], ignorenulls=True).over(window)
            # do the fill
            visits3 = visits3.withColumn('first_month', filled_column_first)

            # define the for last-filled column
            filled_column_last = last(visits3['last_month'], ignorenulls=True).over(window)
            visits3 = visits3.withColumn('last_month', filled_column_last)

            # test
            # visits3.where(col("Interaction_rep_account")=='4328_47027').show()

            # check the condition with [Total visits for that rep_account for that month] and interactionmonth>first_month and Interactionmonth<=last_month then 0 else [Total visits for that rep_account for that month]
            visits3 = visits3.withColumn("rep_account_count", when(
                (col("rep_account_count").isNull()) & (visits3.Interaction_YearMonth >= visits3.first_month) & (
                            visits3.Interaction_YearMonth <= visits3.last_month), 0).otherwise(visits3.rep_account_count))

            # visits3.where(col("Interaction_rep_account")=="1013_1015").show()
            # final calculation, avergae cummulative mean for that rep_account by Interaction_year_month
            visits4 = visits3.where(col("visit_count") >= 3).withColumn('visit_score', F.round(F.avg(
                when(col("rep_account_count").isNull(), 0).otherwise(
                    (visits3.rep_account_count) + visits3.rep_account_count * 0)).over(
                Window.partitionBy("Interaction_rep_account").orderBy("Interaction_YearMonth").rangeBetween(
                    Window.unboundedPreceding, 0)), 2)).select("interactionTypeId", "Interaction_rep_account",
                                                               "Interaction_YearMonth", "visit_score").distinct()

            visits4 = visits4.select("Interaction_rep_account", "Interaction_YearMonth", "visit_score").drop_duplicates()

            master = master.join(visits4, (master.Interaction_rep_account == visits4.Interaction_rep_account) & (
                        master.Interaction_YearMonth == visits4.Interaction_YearMonth), 'left_outer').select(master["*"],
                                                                                                             visits4.visit_score).dropDuplicates()
            master = master.withColumn("visit_score", F.last('visit_score', True).over(
                Window.partitionBy('Interaction_rep_account').orderBy('Interaction_YearMonth').rowsBetween(-sys.maxsize,
                                                                                                           0)))

            # ----------------------------------------------------------------------------------------------------------------------------

            # Metric 4: Product Rep-Account Visit Cadence
            df_rc = df_rc.withColumn('Interaction_startDateLocal', to_date(df_rc.Interaction_startDateLocal, 'yyyy-MM-dd'))
            df_rc = df_rc.withColumn("Interaction_YearMonth", df_rc["Interaction_YearMonth"].cast(IntegerType()))
            cadence = df_rc.where((col("interactionTypeId") == '4') & (col('Interaction_isCompleted') == "1")).select(
                "interactionId", "interactionTypeId", "Interaction_repid", "Interaction_accountId",
                "Interaction_rep_account", "Interaction_isCompleted", "Interaction_startDateLocal", "Interaction_isDeleted",
                "Interaction_YearMonth")

            cadence = cadence.select(cadence["*"], F.min("Interaction_startDateLocal").over(
                Window.partitionBy("Interaction_rep_account")).alias("first_date"),
                                     F.max("Interaction_startDateLocal").over(
                                         Window.partitionBy("Interaction_rep_account", "Interaction_YearMonth")).alias(
                                         "last_date")).select(["*"]).withColumn("tenure",
                                                                                datediff("last_date", "first_date"))

            cadence = cadence.select(cadence["*"], F.lag("Interaction_startDateLocal").over(
                Window.partitionBy("Interaction_rep_account").orderBy("Interaction_startDateLocal")).alias("next_date"))
            cadence = cadence.select(cadence["*"],
                                     datediff("Interaction_startDateLocal", "next_date").alias("days_btw_visits"))

            cadence = cadence.select(cadence["*"], F.round(F.avg("days_btw_visits").over(
                Window.partitionBy("Interaction_rep_account").orderBy("Interaction_YearMonth").rangeBetween(
                    Window.unboundedPreceding, 0)), 2).alias("avg_days_btw_visits"), F.round(
                F.stddev("days_btw_visits").over(
                    Window.partitionBy("Interaction_rep_account").orderBy("Interaction_YearMonth").rowsBetween(
                        Window.unboundedPreceding, 0)), 2).alias("std_days_btw_visits"))

            columns = ['avg_days_btw_visits', 'std_days_btw_visits']
            for column in columns:
                cadence = cadence.withColumn(column, F.when(F.isnan(F.col(column)), None).otherwise(F.col(column)))

            # test
            # cadence.where(col("Interaction_rep_account")=="2308_198078").show()
            # z.show(cadence.where(col("Interaction_rep_account")=="1018_5726").select("avg_days_btw_visits","std_days_btw_visits"))

            cadence2 = cadence.withColumn("cadence_score",
                                          F.round((cadence.avg_days_btw_visits + cadence.std_days_btw_visits) / 2,
                                                  2)).select("interactionTypeId", "Interaction_rep_account",
                                                             "Interaction_YearMonth", "Interaction_startDateLocal",
                                                             "cadence_score").distinct()

            w1 = Window.partitionBy("Interaction_rep_account", "Interaction_YearMonth").orderBy(
                "Interaction_startDateLocal")
            cadence2 = cadence2.withColumn("countdays", F.max("Interaction_startDateLocal").over(w1))
            w3 = Window.partitionBy("Interaction_rep_account", "Interaction_YearMonth").orderBy(F.desc("countdays"))
            cadence3 = cadence2.withColumn("rn", F.row_number().over(w3)).filter("rn=1").select("interactionTypeId",
                                                                                                "Interaction_rep_account",
                                                                                                "Interaction_YearMonth",
                                                                                                "cadence_score")

            cadence3 = cadence3.select("Interaction_rep_account", "Interaction_YearMonth",
                                       "cadence_score").drop_duplicates()

            master = master.join(cadence3, (master.Interaction_rep_account == cadence3.Interaction_rep_account) & (
                        master.Interaction_YearMonth == cadence3.Interaction_YearMonth), 'left_outer').select(master["*"],
                                                                                                              cadence3.cadence_score).dropDuplicates()
            master = master.withColumn("cadence_score", F.last('cadence_score', True).over(
                Window.partitionBy('Interaction_rep_account').orderBy('Interaction_YearMonth').rowsBetween(-sys.maxsize,
                                                                                                           0)))

            # ----------------------------------------------------------------------------------------------------------------------------

            sourcetable = "suggestions"
            df_suggestions = self.utils.dataLoadDelta(self.s3_destination + "data/bronze/" + sourcetable)
            # df_suggestions.cache()
            df_suggestions.createOrReplaceTempView("suggestions")
            logger.info("df_suggestions cached")

            spark_str = '''
            select suggestionReferenceId,detailRepActionTypeId,actionTaken,suggestedDate,repId,productId, accountId,concat(left(SUBSTRING_INDEX(suggestedDate,'-',2),4),right(SUBSTRING_INDEX(suggestedDate,'-',2),2)) suggestion_YearMonth  from suggestions'''

            suggestions = self.spark.sql(spark_str)
            suggestions = suggestions.withColumn("repId", suggestions.repId.cast("String"))
            suggestions = suggestions.withColumn("accountId", suggestions.accountId.cast("String"))
            suggestions = suggestions.withColumn("Suggestion_rep_account",
                                                 F.concat(F.col('repId'), F.lit('_'), F.col('accountId')))
            suggestions_visit = suggestions.where(col("detailRepActionTypeId") == '4').select("suggestionReferenceId",
                                                                                              "detailRepActionTypeId",
                                                                                              "actionTaken",
                                                                                              "suggestedDate", "repId",
                                                                                              "accountId",
                                                                                              "Suggestion_rep_account",
                                                                                              "suggestion_YearMonth")

            suggestions_visit = suggestions_visit.withColumn("lastFlag", when(col("suggestedDate") == (
                F.max("suggestedDate").over(
                    Window.partitionBy("Suggestion_rep_account", "suggestion_YearMonth", "suggestionReferenceId"))),
                                                                              1).otherwise(0))

            suggestions_visit = suggestions_visit.select(suggestions_visit["*"], F.count("suggestionReferenceId").over(
                Window.partitionBy("Suggestion_rep_account", "suggestion_YearMonth", "suggestionReferenceId")).alias(
                "unique_suggestion_visit_count")).filter("lastFlag==1")

            suggestions_visit = suggestions_visit.select(suggestions_visit["*"], F.count("suggestionReferenceId").over(
                Window.partitionBy("Suggestion_rep_account", "suggestion_YearMonth")).alias("total_suggestion_mo"),
                                                         when((col("actionTaken") == "Suggestions Completed"), 1).otherwise(
                                                             0).alias("suggestionComplete"))

            suggestions_visit = suggestions_visit.select(suggestions_visit["*"], F.sum("suggestionComplete").over(
                Window.partitionBy("Suggestion_rep_account").orderBy("suggestion_YearMonth").rangeBetween(
                    Window.unboundedPreceding, 0)).alias("complete_total"))

            last_visit2 = suggestions_visit.select("Suggestion_rep_account", "detailRepActionTypeId",
                                                   "suggestion_YearMonth", "suggestedDate", "total_suggestion_mo",
                                                   "complete_total").distinct()

            w3 = Window.partitionBy("Suggestion_rep_account", "suggestion_YearMonth").orderBy(F.desc("suggestedDate"))
            last_visit2 = last_visit2.withColumn("rn", F.row_number().over(w3)).filter("rn=1")

            last_visit2 = last_visit2.select(last_visit2["*"], F.sum("total_suggestion_mo").over(
                Window.partitionBy("Suggestion_rep_account").rowsBetween(Window.unboundedPreceding, 0)).alias(
                "suggestion_total"))

            last_visit2 = last_visit2.withColumn("suggestion_visit_score", when(col("suggestion_total") >= 3, F.round(
                last_visit2.complete_total / last_visit2.suggestion_total, 2)).otherwise(lit(None)))
            last_visit2 = last_visit2.select("detailRepActionTypeId", "Suggestion_rep_account", "suggestion_YearMonth",
                                             "suggestion_visit_score").distinct()

            last_visit2 = last_visit2.select("Suggestion_rep_account", "suggestion_YearMonth",
                                             "suggestion_visit_score").drop_duplicates()

            master = master.join(last_visit2, (master.Interaction_rep_account == last_visit2.Suggestion_rep_account) & (
                        master.Interaction_YearMonth == last_visit2.suggestion_YearMonth), 'left_outer').select(master["*"],
                                                                                                                last_visit2.suggestion_visit_score).dropDuplicates()
            master = master.withColumn("suggestion_visit_score", F.last('suggestion_visit_score', True).over(
                Window.partitionBy('Interaction_rep_account').orderBy('Interaction_YearMonth').rowsBetween(-sys.maxsize,
                                                                                                           0)))

            # ----------------------------------------------------------------------------------------------------------------------------

            # metric 6
            suggestions_email = suggestions.where(col("detailRepActionTypeId") == '8').select("suggestionReferenceId",
                                                                                              "detailRepActionTypeId",
                                                                                              "actionTaken",
                                                                                              "suggestedDate", "repId",
                                                                                              "accountId",
                                                                                              "Suggestion_rep_account",
                                                                                              "suggestion_YearMonth")
            suggestions_email = suggestions_email.withColumn("lastFlag", when(col("suggestedDate") == (
                F.max("suggestedDate").over(
                    Window.partitionBy("Suggestion_rep_account", "suggestion_YearMonth", "suggestionReferenceId"))),
                                                                              1).otherwise(0))

            suggestions_email = suggestions_email.select(suggestions_email["*"], F.count("suggestionReferenceId").over(
                Window.partitionBy("Suggestion_rep_account", "suggestion_YearMonth", "suggestionReferenceId")).alias(
                "unique_suggestion_email_count")).filter("lastFlag==1")

            suggestions_email = suggestions_email.select(suggestions_email["*"], F.count("suggestionReferenceId").over(
                Window.partitionBy("Suggestion_rep_account", "suggestion_YearMonth")).alias("total_suggestion_mo"),
                                                         when((col("actionTaken") == "Suggestions Completed"), 1).otherwise(
                                                             0).alias("suggestionComplete"))

            suggestions_email = suggestions_email.select(suggestions_email["*"], F.sum("suggestionComplete").over(
                Window.partitionBy("Suggestion_rep_account").orderBy("suggestion_YearMonth").rangeBetween(
                    Window.unboundedPreceding, 0)).alias("complete_total"))

            last_email2 = suggestions_email.select("Suggestion_rep_account", "detailRepActionTypeId",
                                                   "suggestion_YearMonth", "suggestedDate", "total_suggestion_mo",
                                                   "complete_total").distinct()

            w5 = Window.partitionBy("Suggestion_rep_account", "suggestion_YearMonth").orderBy(F.desc("suggestedDate"))
            last_email2 = last_email2.withColumn("rn", F.row_number().over(w5)).filter("rn=1")

            last_email2 = last_email2.select(last_email2["*"], F.sum("total_suggestion_mo").over(
                Window.partitionBy("Suggestion_rep_account").rowsBetween(Window.unboundedPreceding, 0)).alias(
                "suggestion_total"))

            last_email2 = last_email2.withColumn("suggestion_email_score", when(col("suggestion_total") >= 3, F.round(
                last_email2.complete_total / last_email2.suggestion_total, 2)).otherwise(lit(None)))

            last_email2 = last_email2.select("Suggestion_rep_account", "suggestion_YearMonth",
                                             "suggestion_email_score").distinct()

            master = master.join(last_email2, (master.Interaction_rep_account == last_email2.Suggestion_rep_account) & (
                        master.Interaction_YearMonth == last_email2.suggestion_YearMonth), 'left_outer').select(master["*"],
                                                                                                                last_email2.suggestion_email_score).dropDuplicates()
            master = master.withColumn("suggestion_email_score", F.last('suggestion_email_score', True).over(
                Window.partitionBy('Interaction_rep_account').orderBy('Interaction_YearMonth').rowsBetween(-sys.maxsize,
                                                                                                           0)))

            # ----------------------------------------------------------------------------------------------------------------------------

            # check if we have to add target

            if target == 'yes':
                sourcetable = "strategy_target"
                df_st = self.utils.dataLoadDelta(self.s3_destination + "data/bronze/" + sourcetable)
                df_st.cache()
                df_st.createOrReplaceTempView("st")
                logger.info("st cached")

                sourcetable = "rep_account_assignment"
                df_ra = self.utils.dataLoadDelta(self.s3_destination + "data/bronze/" + sourcetable)
                df_ra.cache()
                df_ra.createOrReplaceTempView("ra")
                logger.info("ra cached")

                sourcetable = "rep_team_rep"
                df_rt = self.utils.dataLoadDelta(self.s3_destination + "data/bronze/" + sourcetable)
                df_rt.cache()
                df_rt.createOrReplaceTempView("rt")
                logger.info("df_rt cached")

                # get target and process a bit

                spark_str = """
                select Target_Account,RepTeamID_Associated,
                case when targetingLevelId=6 then 'VISIT' 
                when targetingLevelId=5 then 'SEND_ANY' end as Interaction_targetingLevelId,
                case when targetingLevelId=6 then 'VISIT_DETAIL' 
                when targetingLevelId=5 then 'SEND' end as Suggestion_targetingLevelId
                ,t.strategyTargetId,t.targetsPeriodId,t.target,t.startDate,t.endDate,concat(left(SUBSTRING_INDEX(t.startDate,'-',2),4),right(SUBSTRING_INDEX(t.startDate,'-',2),2)) startDateMonth,concat(left(SUBSTRING_INDEX(t.endDate,'-',2),4),right(SUBSTRING_INDEX(t.endDate,'-',2),2)) endDateMonth
                from st t 
                """

                df_st_2 = self.spark.sql(spark_str)
                df_st_2 = df_st_2.withColumn("RepTeamID_Associated", df_st_2.RepTeamID_Associated.cast("String"))
                df_st_2 = df_st_2.withColumn("Target_Account", df_st_2.Target_Account.cast("String"))
                # df_st_2=self.spark.sql(spark_str)
                logger.info(df_st_2.printSchema())
                df_st_2.createOrReplaceTempView("st_2")

                spark_str = """
                select * from (
                select rd.*,t1.Interaction_targetingLevelId,t1.Suggestion_targetingLevelId,t1.targetsPeriodId,t1.strategyTargetId,t1.target from rc rd 
                left join st_2 t1 
                on 
                All_accountId=t1.Target_Account and rd.repTeamId=t1.RepTeamID_Associated
                and COALESCE(rd.interactionTypeName,rd.suggestion_detailRepActionName)=COALESCE(t1.Interaction_targetingLevelId,t1.Suggestion_targetingLevelId)
                and rd.All_effectivedate between t1.startDate and t1.endDate
                ) t 
                """

                df_rc_2 = self.spark.sql(spark_str)

                df_rc_2.createOrReplaceTempView("rc_v2")
                # df_rc_2.repartition("recordclassid").write.mode('overwrite').partitionBy("recordclassid").parquet(s3_destination+customer+'/'+environment+"/recordclasses_st/")
                if df_rc_2.count() != 0:
                    df_rc_2.repartition(40).write.mode('overwrite').partitionBy("All_year", "recordclassId").format(
                        "delta").save(self.s3_destination + "data/silver/recordclasses_st/")
                else:
                    # df_rc_2.parquet(s3_destination+customer+'/'+environment+"/recordclasses_st/")
                    df_rc_2.repartition(40).write.option("mergeSchema", "true").mode("append").partitionBy("All_year", "recordclassId").format("delta").save(
                        self.s3_destination + "data/silver/recordclasses_st/")
                # metric 7
                pt = df_rc_2.select("interactionId", "Interaction_rep_account", "targetsPeriodId", "interactionTypeId",
                                    "target").distinct()

                pt = pt.select(pt["*"], F.count("interactionId").over(
                    Window.partitionBy("Interaction_rep_account", "targetsPeriodId", "interactionTypeId")).alias(
                    "target_count"))
                pt = pt.select("Interaction_rep_account", "targetsPeriodId", "interactionTypeId", "target",
                               "target_count").distinct()
                pt = pt.select(pt["*"], F.sum("target").over(
                    Window.partitionBy("Interaction_rep_account", "targetsPeriodId", "interactionTypeId")).alias(
                    "target_total"))

                pt = pt.withColumn("target_achievement",
                                   when(col("target") != 0, F.round(pt.target_count / pt.target_total, 2)).otherwise(0))

                pt3 = pt.groupby('Interaction_rep_account', 'targetsPeriodId', 'target_count').pivot("interactionTypeId").agg(
                    sum("target_achievement"))

                pt3 = pt3.withColumnRenamed("11", "emails")
                pt3 = pt3.withColumnRenamed("4", "visits")

                pt3 = pt3.withColumn("visit_score", when(pt3.visits.isNotNull(), F.round(pt3.visits * pt3.target_count, 2)))
                pt3 = pt3.withColumn("email_score", when(pt3.visits.isNotNull(), F.round(pt3.emails * pt3.target_count, 2)))
                pt3 = pt3.select("Interaction_rep_account", "targetsPeriodId", "visit_score", "email_score").distinct()
                pt3 = pt3.replace(float('nan'), None)

                pt3 = pt3.withColumn("email_score", F.last('email_score', True).over(
                    Window.partitionBy('Interaction_rep_account', 'targetsPeriodId').rowsBetween(-sys.maxsize, 0)))
                pt3 = pt3.withColumn("email_score", F.last('email_score', True).over(
                    Window.partitionBy('Interaction_rep_account', 'targetsPeriodId').rowsBetween(-sys.maxsize, 1)))
                pt3 = pt3.withColumn("visit_score", F.last('visit_score', True).over(
                    Window.partitionBy('Interaction_rep_account', 'targetsPeriodId').rowsBetween(-sys.maxsize, 0)))
                pt3 = pt3.withColumn("visit_score", F.last('visit_score', True).over(
                    Window.partitionBy('Interaction_rep_account', 'targetsPeriodId').rowsBetween(-sys.maxsize, 1)))

                w5 = Window.partitionBy("Interaction_rep_account", "targetsPeriodId").orderBy(F.desc("targetsPeriodId"))
                pt3 = pt3.withColumn("rn", F.row_number().over(w5)).filter("rn=1")

                w6 = Window.partitionBy("Interaction_rep_account", "targetsPeriodId").orderBy(F.desc("targetsPeriodId"))
                pt3 = pt3.withColumn("rn", F.row_number().over(w6)).filter("rn=1")
                ##forward fill email and visit score

                pt3 = pt3.select(pt3["*"], when((pt3.visit_score.isNotNull()) & (pt3.email_score.isNotNull()),
                                                (pt3.visit_score + pt3.email_score) / 2).when((pt3.visit_score.isNotNull()),
                                                                                              pt3.visit_score).otherwise(
                    pt3.email_score).alias("target_achievement_score"))
                pt3 = pt3.select("Interaction_rep_account", "targetsPeriodId", "target_achievement_score").distinct()

                pt3 = pt3.withColumn("targetsPeriodId", pt3["targetsPeriodId"].cast(IntegerType()))
                pt3 = pt3.join(df_st_2, (pt3.targetsPeriodId == df_st_2.targetsPeriodId), 'left_outer').select(pt3["*"],
                                                                                                               df_st_2.startDateMonth).dropDuplicates()

                master = master.join(pt3, (master.Interaction_rep_account == pt3.Interaction_rep_account) & (
                            master.Interaction_YearMonth == pt3.startDateMonth), 'left_outer').select(master["*"],
                                                                                                      pt3.target_achievement_score).dropDuplicates()
                master = master.withColumn("target_achievement_score", F.last('target_achievement_score', True).over(
                    Window.partitionBy('Interaction_rep_account').orderBy('Interaction_YearMonth').rowsBetween(-sys.maxsize,
                                                                                                               0)))

                # metric 8

                ct = df_rc_2.select("recordclass", "interactionId", "Interaction_rep_account", "targetsPeriodId",
                                    "interactionTypeId", "target").distinct()
                ct = ct.where((col("interactionTypeId").isin({"11", "4"})) & ((col("Interaction_isCompleted") == "1")))

                ct = ct.withColumn("channel_count_period", F.count("recordclass").over(
                    Window.partitionBy("Interaction_rep_account", "targetsPeriodId", "interactionTypeId")))

                ct = ct.withColumn("channel_target",
                                   when(col("target") != 0, F.round(ct.channel_count_period / ct.target, 2)).otherwise(
                                       ct.channel_count_period))
                ct = ct.select("Interaction_rep_account", "targetsPeriodId", "interactionTypeId", "target",
                               "channel_target").distinct()

                ct = ct.withColumn("channel_utilization", F.avg("channel_target").over(
                    Window.partitionBy("Interaction_rep_account", "interactionTypeId", "targetsPeriodId")))
                ct = ct.select("Interaction_rep_account", "targetsPeriodId", "interactionTypeId", "target",
                               "channel_utilization").distinct()

                ct3 = ct.groupby('Interaction_rep_account', 'targetsPeriodId').pivot(
                    "interactionTypeId").agg(sum("channel_utilization"))

                ct3 = ct3.withColumnRenamed("11", "emails")
                ct3 = ct3.withColumnRenamed("4", "visits")

                ct3 = ct3.withColumn("emails", F.last('emails', True).over(
                    Window.partitionBy('Interaction_rep_account', 'targetsPeriodId').rowsBetween(-sys.maxsize, 0)))
                ct3 = ct3.withColumn("emails", F.last('emails', True).over(
                    Window.partitionBy('Interaction_rep_account', 'targetsPeriodId').rowsBetween(-sys.maxsize, 1)))
                ct3 = ct3.withColumn("visits", F.last('visits', True).over(
                    Window.partitionBy('Interaction_rep_account', 'targetsPeriodId').rowsBetween(-sys.maxsize, 0)))
                ct3 = ct3.withColumn("visits", F.last('visits', True).over(
                    Window.partitionBy('Interaction_rep_account', 'targetsPeriodId').rowsBetween(-sys.maxsize, 1)))

                w5 = Window.partitionBy("Interaction_rep_account", "targetsPeriodId").orderBy(F.desc("targetsPeriodId"))
                ct3 = ct3.withColumn("rn", F.row_number().over(w5)).filter("rn=1")

                w6 = Window.partitionBy("Interaction_rep_account", "targetsPeriodId").orderBy(F.desc("targetsPeriodId"))
                ct3 = ct3.withColumn("rn", F.row_number().over(w6)).filter("rn=1")

                ct3 = ct3.select(ct3["*"], when((~isnan(ct3.visits)) & (~isnan(ct3.emails)) & (ct3.emails >= ct3.visits),
                                                (F.round(ct3.visits / ct3.emails, 2))).when(
                    (~isnan(ct3.visits)) & (~isnan(ct3.emails)) & (ct3.visits >= ct3.emails),
                    (F.round(ct3.emails / ct3.visits, 2))).alias("channel_quarter"))

                ct3 = ct3.withColumn('channel_score', F.round(F.avg(ct3.channel_quarter).over(
                    Window.partitionBy("Interaction_rep_account", "targetsPeriodId").orderBy(
                        "targetsPeriodId").rangeBetween(Window.unboundedPreceding, 0)), 2))

                ct3 = ct3.withColumn("channel_score", F.last('channel_score', True).over(
                    Window.partitionBy('Interaction_rep_account').rowsBetween(-sys.maxsize, 0)))

                ct3 = ct3.select("Interaction_rep_account", "targetsPeriodId", "channel_score").distinct()

                ct3 = ct3.withColumn("targetsPeriodId", ct3["targetsPeriodId"].cast(IntegerType()))

                ct3 = ct3.join(df_st_2, (ct3.targetsPeriodId == df_st_2.targetsPeriodId), 'left_outer').select(ct3["*"],
                                                                                                               df_st_2.startDateMonth).dropDuplicates()

                master = master.join(ct3, (master.Interaction_rep_account == ct3.Interaction_rep_account) & (
                            master.Interaction_YearMonth == ct3.startDateMonth), 'left_outer').select(master["*"],
                                                                                                      ct3.channel_score).dropDuplicates()
                master = master.withColumn("channel_score", F.last('channel_score', True).over(
                    Window.partitionBy('Interaction_rep_account').orderBy('Interaction_YearMonth').rowsBetween(-sys.maxsize,
                                                                                                               0)))
                master.write.mode('overwrite').partitionBy("Interaction_YearMonth").format("delta").save(
                    self.s3_destination + "data/silver/cri_scores/")

            else:
                master = master.withColumn("target_achievement_score", lit(None).cast(StringType()))
                master = master.withColumn("channel_score", lit(None).cast(StringType()))
                master = master.withColumn("target_achievement_score",
                                           when(master.target_achievement_score.isNull(), 0).otherwise(0))
                master = master.withColumn("channel_score", when(master.channel_score.isNull(), 0).otherwise(0))
                master.write.mode('overwrite').partitionBy("Interaction_YearMonth").format("delta").save(
                    self.s3_destination + "data/silver/cri_scores/")
        except Exception as e:
            logger.error("Error in stRecordClassTransCri: {}".format(e))
            raise

# ----------------------------------------------------------------------------------------------------------------------------
