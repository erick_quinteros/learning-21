import string
import logging
import boto3

logger = None
class Utils:
    def __init__(self, glue_context, customer):
        self.glueContext = glue_context
        self.customer = customer
        self.spark = self.glueContext.spark_session

        global logger
        logger = logging.getLogger("ADL_DEV_LOGGER")
        pass

    def df_to_partitions(self,df, partition_columns_list, s3_destination):
        '''
        :param df:
        :param partition_columns_list:
        :param s3_destination:
        :return:
        '''
        try:
            df.repartition(*partition_columns_list).write.mode('overwrite').partitionBy(*partition_columns_list).parquet(
            s3_destination)
        except Exception as e:
            logger.error("Error in df_to_partition: {}".format(e))
            raise
        return True

    def df_to_partitions_append(self,df, partition_columns_list, s3_destination):
        '''

        :param df:
        :param partition_columns_list:
        :param s3_destination:
        :return:
        '''
        try:
            df.repartition(*partition_columns_list).write.mode('append').partitionBy(*partition_columns_list).parquet(
            s3_destination)
        except Exception as e:
            logger.error("Error in df_to_partition_append: {}".format(e))
            raise
        return True

    # storedfwithunpartition
    def df_to_unpartition(self,df, s3_destination):
        '''

        :param df:
        :param s3_destination:
        :return:
        '''
        try:
            df.write.mode('overwrite').parquet(s3_destination)
        except Exception as e:
            logger.error("Error in df_to_unpartition: {}".format(e))
            raise
        return True

    def data_load_parquet(self,glueContext, s3_load_parquet):
        '''

        :param glueContext:
        :param s3_load_parquet:
        :return:
        '''
        # read the dataloading sript and convert it to dynamic data frame. It's a pipe delimited csv file with partitions
        try:
            dynamic_frame0 = glueContext.create_dynamic_frame_from_options('s3', connection_options={'paths': [s3_load_parquet],
                                                                                                 "partitionKeys": [
                                                                                                     'InteractionYear']},
                                                                       format="parquet",
                                                                       transformation_ctx="dynamic_frame0")
        # convert dynamic dataframe to pandas dataframe for faster operation
            df = dynamic_frame0.toDF().distinct()
        except Exception as e:
            logger.error("Error in data_load_parquet: {}".format(e))
            raise
        return df

    # load the csv scrip by sending glue context and path to csv file[Pipe delimited, with headers]. returns pandas dataframe
    def data_load_csv(self, glueContext, s3_load_csv):
        '''
        :param glueContext:
        :param s3_load_csv:
        :return:
        '''
        # read the dataloading sript and convert it to dynamic data frame. It's a pipe delimited csv file with partitions
        try:
            dynamic_frame0 = glueContext.create_dynamic_frame_from_options('s3', connection_options={'paths': [s3_load_csv], },
                                                                       format_options={"withHeader": True,
                                                                                       "separator": "|"}, format="csv",
                                                                       transformation_ctx="dynamic_frame0")
        # convert dynamic dataframe to pandas dataframe for faster operation
            df = dynamic_frame0.toDF().toPandas()
        except Exception as e:
            logger.error("Error in data_load_csv: {}".format(e))
            raise
        return df

    def df_topartition_delta(self,df, partition, location):
        '''

        :param df:
        :param partition:
        :param location:
        :return:
        '''
        try:
            df.repartition(40, partition).write.format("delta").partitionBy(partition).save(location)
        except Exception as e:
            logger.error("Error in df_topartition_delta: {}".format(e))
            raise

    def df_to_delta(self, df, location):
        '''

        :param df:
        :param location:
        :return:
        '''
        try:
            df.write.format("delta").mode("overwrite").save(location)
        except Exception as e:
            logger.error("Error in df_to_delta: {}".format(e))
            raise


    def dataLoadDelta(self, s3_table_location):
        '''

        :param s3_table_location:
        :return:
        '''
        df = None
        try:
            df = self.spark.read.format("delta").load(s3_table_location)
        except Exception as e:
            logger.error("Error in dataLoadDelta: {}".format(e))
            raise
        return df

    def sql_to_template(self,sqlcommand, values):
        template = string.Template('% s' % (sqlcommand))
        sqlstr = template.substitute(values)
        sqlstr = sqlstr.replace('\n', ' ');
        sqlstr = sqlstr.strip()
        return sqlstr
    ##########################################################################

    def getMaxPartitionData_Interaction(glue_client, glueContext, databasename, sourcetable):
        ### using boto client to read partitions
        response = glue_client.get_partitions(DatabaseName=databasename, TableName=sourcetable)
        partitions = [int(p['Values'][0]) for p in response['Partitions']]
        set_partitions = list(set(partitions))
        set_partitions.sort()
        set_partitions = set_partitions[0]
        logger.info(set_partitions)
        loaddate = str(set_partitions)
        push_down_predicate = "(interactionyear = " + loaddate + ")"
        # source_DF = glueContext.create_dynamic_frame.from_catalog(database = databasename, table_name = sourcetable, transformation_ctx = "main_query", push_down_predicate = push_down_predicate)
        source_DF = glueContext.create_dynamic_frame.from_catalog(database=databasename, table_name=sourcetable,
                                                                  transformation_ctx="main_query")
        responseGetTable = glue_client.get_table(DatabaseName=databasename, Name=sourcetable)
        logger.info('\n after getting responseGetTable')
        responseTable = responseGetTable['Table']
        logger.info('\n after getting responseTable')
        responseStorage = responseTable['StorageDescriptor']
        logger.info('\n after getting responseStorage')
        responseColumns = responseStorage['Columns']
        logger.info('\n after getting responseColumns')
        responseMapping = []
        logger.info('\n building responseMapping')
        return source_DF

    def getMaxPartitionData_Suggestions(glue_client, glueContext, databasename, sourcetable):
        ### using boto client to read partitions
        response = glue_client.get_partitions(DatabaseName=databasename, TableName=sourcetable)
        partitions = [int(p['Values'][0]) for p in response['Partitions']]
        set_partitions = list(set(partitions))
        set_partitions.sort()
        set_partitions = set_partitions[0]
        logger.info(set_partitions)
        loaddate = str(set_partitions)
        push_down_predicate = "(suggestedyear = " + loaddate + ")"
        # source_DF = glueContext.create_dynamic_frame.from_catalog(database = databasename, table_name = sourcetable, transformation_ctx = "main_query", push_down_predicate = push_down_predicate)
        source_DF = glueContext.create_dynamic_frame.from_catalog(database=databasename, table_name=sourcetable,
                                                                  transformation_ctx="main_query")
        responseGetTable = glue_client.get_table(DatabaseName=databasename, Name=sourcetable)
        logger.info('\n after getting responseGetTable')
        responseTable = responseGetTable['Table']
        logger.info('\n after getting responseTable')
        responseStorage = responseTable['StorageDescriptor']
        logger.info('\n after getting responseStorage')
        responseColumns = responseStorage['Columns']
        logger.info('\n after getting responseColumns')
        responseMapping = []
        logger.info('\n building responseMapping')
        return source_DF

    def getMaxPartitionData_target(glue_client, glueContext, databasename, sourcetable):
        ### using boto client to read partitions
        response = glue_client.get_partitions(DatabaseName=databasename, TableName=sourcetable)
        partitions = [int(p['Values'][0]) for p in response['Partitions']]
        set_partitions = list(set(partitions))
        set_partitions.sort()
        set_partitions = set_partitions[0]
        logger.info(set_partitions)
        loaddate = str(set_partitions)
        push_down_predicate = "(start_target_year = " + loaddate + ")"
        # source_DF = glueContext.create_dynamic_frame.from_catalog(database = databasename, table_name = sourcetable, transformation_ctx = "main_query", push_down_predicate = push_down_predicate)
        source_DF = glueContext.create_dynamic_frame.from_catalog(database=databasename, table_name=sourcetable,
                                                                  transformation_ctx="main_query")
        responseGetTable = glue_client.get_table(DatabaseName=databasename, Name=sourcetable)
        logger.info('\n after getting responseGetTable')
        responseTable = responseGetTable['Table']
        logger.info('\n after getting responseTable')
        responseStorage = responseTable['StorageDescriptor']
        logger.info('\n after getting responseStorage')
        responseColumns = responseStorage['Columns']
        logger.info('\n after getting responseColumns')
        responseMapping = []
        logger.info('\n building responseMapping')
        return source_DF

    def getUnPartitionData(glue_client, glueContext, databasename, sourcetable):
        source_DF = glueContext.create_dynamic_frame.from_catalog(database=databasename, table_name=sourcetable,
                                                                  transformation_ctx="main_query")
        responseGetTable = glue_client.get_table(DatabaseName=databasename, Name=sourcetable)
        logger.info('\n after getting responseGetTable')
        responseTable = responseGetTable['Table']
        logger.info('\n after getting responseTable')
        responseStorage = responseTable['StorageDescriptor']
        logger.info('\n after getting responseStorage')
        responseColumns = responseStorage['Columns']
        logger.info('\n after getting responseColumns')
        responseMapping = []
        logger.info('\n building responseMapping')
        return source_DF

    def getArchiveRdsTables(self, source_s3_location):
        # drop 's3://'
        path = source_s3_location[5:]
        path_list = path.split('/')
        # get bucket name and prefix
        bucket_name = path_list[0]
        prefix_name = '/'.join(path_list[1:])
        logger.info([bucket_name,prefix_name])
        # connect s3 bucket
        s3_client = boto3.client('s3')
        objects_list = s3_client.list_objects(Bucket=bucket_name, Prefix=prefix_name, Delimiter='/')
        result = []
        # get all sub-dir in source archive_rds dir
        if "CommonPrefixes" in objects_list:
            for object in objects_list.get('CommonPrefixes'):
                table_full_path = object.get('Prefix')
                table = table_full_path.split('/')[-2]
                result.append(table)

            return set(result)
        else: return set()





