from awsglue.transforms import *
from utils import Utils
import logging

logger = None
#global str

class DseRecordClassTrans:
    def __init__(self, glue_context, customer, s3_destination):
        self.glueContext = glue_context
        self.customer = customer
        self.s3_destination = s3_destination
        self.utils = Utils(glue_context, customer)
        self.spark = self.glueContext.spark_session

        global logger
        logger = logging.getLogger("ADL_DEV_LOGGER")
        pass


    def run(self):
        try:
            df_rc = self.utils.dataLoadDelta(self.s3_destination + 'data/silver/recordclasses')
            #df_rc = self.spark.read.parquet(self.s3_destination + 'data/silver/recordclasses')
            df_rc.cache()
            df_rc.createOrReplaceTempView("rc")
            logger.info("rc cached")

            df_dse = self.utils.dataLoadDelta(self.s3_destination + 'data/bronze/dse_score')
            df_dse.cache()
            df_dse.createOrReplaceTempView("dse")
            logger.info("df_dse cached")

            df_rst = self.utils.dataLoadDelta(self.s3_destination + 'data/bronze/rep_team_rep')
            df_rst.cache()
            df_rst.createOrReplaceTempView("rst")
            logger.info("df_rst cached")

            #DSE data processing
            spark_str="""
              select t1.* from dse t1 
            inner join (
            select ss.suggestionReferenceId,max(ss.runId) as runId,max(ss.startDateTime) as startDateTime,rd.suggestedDate,ss.repId,ss.accountId
            from rc rd
            inner join dse ss 
            on rd.suggestionReferenceId=ss.suggestionReferenceId and rd.suggestedDate=ss.suggestedDate
            and rd.suggestion_accountId=ss.accountId
            group by ss.suggestionReferenceId,rd.suggestedDate,repId,accountId
            ) t2 
            on t1.suggestionReferenceId=t2.suggestionReferenceId and t1.suggestedDate=t2.suggestedDate
            and t1.runId=t2.runId and t1.startDateTime=t2.startDateTime and t1.repId=t2.repId and t1.accountId=t2.accountId
            """
            df=self.spark.sql(spark_str)
            df.createOrReplaceTempView("dse_2")


            spark_str="""
            select rd.*,t.runId as DSE_runId,t.isSuggestionCritical as DSE_isAccountCritical,
            '' as DSE_numWorkDaysOverdue, '' as DSE_currentCompletionsPeriodTargetsProgressStatusId, '' as DSE_evaluatedCompletionsPeriodTargetsProgressStatusId,
            '' as DSE_allTargetsProgressText,'' as DSE_targetDrivenEvaluationInfo,
            t.startDateTime as DSE_Run_StartDateTime,t.startDateLocal as DSE_Run_StartDateLocal,t.numSuggestibleReps as DSE_numSuggestibleReps,
            t.numSuggestibleAccounts as DSE_numSuggestibleAccounts,t.numEvaluatedAccounts as DSE_numEvaluatedAccounts,
            t.runRepDateSuggestionId as DSE_runRepDateSuggestionId,t.suggestionPriorityScore as DSE_suggestionPriorityScore,t.isSuggestionCritical as DSE_isSuggestionCritical,t.isSupersededByTrigger as DSE_isSupersededByTrigger
            from rc rd 
            left join 
            (
            select * from dse_2
            ) t 
            on rd.suggestionReferenceId=t.suggestionReferenceId and rd.suggestedDate=t.suggestedDate
            and rd.suggestion_accountId=t.accountId
            """
            df_rcv2=self.spark.sql(spark_str)
            logger.info(df_rcv2.count())
            df_rcv2.createOrReplaceTempView("rc2")
            #add dates information
            df_dates= self.spark.read.csv("s3://" + self.s3_destination.split('/')[2] + "/adl/common/data/dates.csv", sep='|', header='true')
            df_dates.createOrReplaceTempView("dates")
            spark_str="""
            
            select *,COALESCE(suggestion_YearMonth,Interaction_YearMonth) as All_effectiveyearmonth,CONCAT(`All_repId`, '_', `All_accountId`) as All_rep_account,year(All_effectivedate) as All_year,
            month(All_effectivedate) as All_month,DAYOFMONTH(All_effectivedate) as All_day from (
            select 
            recordclassId, recordclass, interactionId, externalId, Interaction_timeZoneId, interactionyear, interactionmonth, interactionday, Interaction_startDateTime, Interaction_startDateLocal, 
            d.WeekDayName as Interaction_WeekDayName,d.IsWeekend Interaction_IsWeekend,d.DOWInMonth as Interaction_DOWInMonth,
            d.DayOfYear as Interaction_DayOfYear,d.WeekOfMonth as Interaction_WeekOfMonth,d.WeekOfYear as Interaction_WeekOfYear,
            concat(left(SUBSTRING_INDEX(d.`Date`,'-',2),4),right(SUBSTRING_INDEX(d.`Date`,'-',2),2)) Interaction_YearMonth,
            d.Quarter as Interaction_Quarter, 
            interactionTypeId, interactionTypeName, repActionTypeId, repActionTypeName, Interaction_Duration, Interaction_wasCreatedFromSuggestion, 
            Interaction_isCompleted, Interaction_isDeleted, Interaction_createdAt, Interaction_updatedAt, Interaction_repid, Interaction_accountId, CONCAT(`Interaction_repid`, '_', `Interaction_accountId`) as Interaction_rep_account,
            Interaction_facilityId, interaction_account_isdeleted, raw_cs_id, Email_Email_Sent_Date_vod__c, Email_Capture_Datetime_vod__c, 
            Email_accountuid, Email_Account_Email_vod__c, Email_Email_Config_Values_vod__c
            ,Email_Email_Subject__c
            ,Email_Email_Content_vod__c
            --, Lilly_Content1__c
            ,Email_Opened_vod__c, Email_Open_Count_vod__c, 
            Email_Click_Count_vod__c, Email_Product_Display_vod__c, Email_Product_vod__c, Email_Sender_Email_vod__c, 
            Email_Status_vod__c, email_Message_Id,email_messageChannelId,email_messageTopicId,email_messageTopicName,email_messagename,email_messagedescription,Email_Call2_vod__c, Call_Call_Datetime_vod__c, Call_Call_Date_vod__c, Call_accountuid, Call_Address_Line_1_vod__c, Call_State_vod__c, Call_City_vod__c, Call_Zip_4_vod__c, Call_Zip_vod__c, Call_Address_Line_2_vod__c, 
            Call_Next_Call_Notes_vod__c,
             Call_Call_Type_vod__c, Call_Attendee_Type_vod__c, 
            Call_Detailed_Products_vod__c, 
            Call_Submitted_By_Mobile_vod__c, productInteractionTypeId, productInteractionTypeName, messageId, messageTopicId, 
            -- messageTopicName, 
            messageReaction, physicalMessageDesc, physicalMessageUID, quantity,  
            suggestionReferenceId, suggestionUID, suggestedDate, ds.WeekDayName as Suggestion_WeekDayName,ds.IsWeekend as Suggestion_IsWeekend,ds.DOWInMonth as Suggestion_DOWInMonth,
            ds.DayOfYear as Suggestion_DayOfYear,ds.WeekOfMonth as Suggestion_WeekOfMonth,ds.WeekOfYear as Suggestion_WeekOfYear
            ,concat(left(SUBSTRING_INDEX(ds.`Date`,'-',2),4),right(SUBSTRING_INDEX(ds.`Date`,'-',2),2)) suggestion_YearMonth,ds.Quarter as Suggestion_Quarter ,suggestion_channel, suggestion_actionTaken, suggestion_dismissReason, 
            suggestion_interactionraw, suggestion_interactionId, suggestion_repId,suggestion_accountId,CONCAT(`suggestion_repId`, '_', `suggestion_accountId`) as Suggestion_rep_account, suggestion_detailRepActionTypeId, suggestion_detailRepActionName, 
            suggestion_messageId, suggestion_messageName, suggestion_repTeamId, suggestion_productId, suggestedyear, 
            suggestedmonth, suggestedday, DSE_runId, DSE_isAccountCritical, DSE_numWorkDaysOverdue, DSE_currentCompletionsPeriodTargetsProgressStatusId, 
            DSE_evaluatedCompletionsPeriodTargetsProgressStatusId, DSE_allTargetsProgressText, DSE_targetDrivenEvaluationInfo, DSE_Run_StartDateTime, 
            DSE_Run_StartDateLocal, DSE_numSuggestibleReps, DSE_numSuggestibleAccounts, DSE_numEvaluatedAccounts, DSE_runRepDateSuggestionId, 
            DSE_suggestionPriorityScore, DSE_isSuggestionCritical, DSE_isSupersededByTrigger,
            COALESCE(rdd.Interaction_repid,suggestion_repId) as All_repId,
            COALESCE(rdd.Interaction_accountId,suggestion_accountId) as All_accountId,
            cast(COALESCE(suggestedDate,rdd.Interaction_startDateLocal) as date) All_effectivedate
             from rc2 rdd
            left join dates d
            on rdd.Interaction_startDateLocal=d.`Date`
            left join dates ds
            on rdd.suggestedDate=ds.`Date`
            ) t 
            """

            df_rcv3=self.spark.sql(spark_str)
            df_rcv3.createOrReplaceTempView("rc3")
            #df_rcv3.repartition(40).write.mode('overwrite').partitionBy("All_year").format("delta").save(self.s3_destination+"data/bronze/dse_score_final/")
            df_rcv3.repartition(80).write.mode('overwrite').format("delta").save(self.s3_destination + "data/bronze/dse_score_final/")

            #finally check repteam alignment and get repids
            df_rcv4 = df_rcv3.join(df_rst,((df_rcv3.All_repId == df_rst.repId) & (df_rcv3.All_effectivedate.between(df_rst.startDate,df_rst.endDate))),'left_outer').select(df_rcv3["*"],df_rst["repTeamId"],df_rst["cluster"]).dropDuplicates()
            df_rcv4.createOrReplaceTempView("rcv4")
            #df_rcv4.repartition("All_year", "All_month", "All_day").write.mode('overwrite').partitionBy("All_year", "All_month", "All_day").format("delta").save(self.s3_destination + "data/silver/recordclasses_dse/")
            df_rcv4.repartition(40).write.mode('overwrite').partitionBy("All_year","recordclassId").format(
                "delta").save(self.s3_destination + "data/silver/recordclasses_dse/")
        except Exception as e:
            logger.error("Error in dseRecordClassTrans: {}".format(e))
            raise
