#%pyspark
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job
from pyspark.sql import SQLContext
import pandas as pd
import numpy as np
pd.set_option('display.max_columns', 20)
pd.set_option('display.max_rows', 100)
import boto3
import os
import string
from datetime import datetime, timedelta


partition_columns={"interaction":["interactionId","repId"],
"interactiontype":["interactionTypeId"],"repactiontype":["repActionTypeId"],
"call2_vod__c":["Id"],"interactionaccount":["interactionId","accountId"],
"interactionproduct":["interactionId","productId"],"messagetopic":["messageTopicId"],
"message":["interactionId","repId"],"sent_email_vod__c":["Id"],
"product":["productId"],"productinteractiontype":["productInteractionTypeId","repId"],
"physicalmessage":["physicalMessageId","repId"],"dserun":["runId"],
"dserunrepdate":["runRepDateId"],"dserunaccount":["runAccountId"],
"dserunrepdatesuggestion":["runAccountId"],"dserunrepdatesuggestiondetail":["runRepDateSuggestionDetailId"],
"repaccountassignment":["repId","accountId"],"account_dse":["accountId"],
"account_cs":["Id"],"facility":["facilityId"],
"rep":["repId"],"replocation":["repId"],
"repteam":["repTeamId"],"akt_replicense_arc":["cluster"],
"approved_document_vod__c":["Id"],"accountproduct":["accountId","repId"],
"strategytarget":["strategyTargetId"],"targetsperiod":["targetsPeriodId"],
"targetinglevel":["targetingLevelId"],"repproductauthorization":["repProductAuthorizationId"],
"interaction":["interactionId","repId"],"interaction":["interactionId","repId"],
"interaction":["interactionId","repId"],"interaction":["interactionId","repId"]
}


ordered_columns={"interaction":["updatedAt"],
"interactiontype":["updatedAt"],"repactiontype":["updatedAt"],
"call2_vod__c":["LastModifiedDate"],"interactionaccount":["updatedAt"],
"interactionproduct":["updatedAt"],"messagetopic":["updatedAt"],
"message":["updatedAt"],"sent_email_vod__c":["LastModifiedDate"],
"product":["updatedAt"],"productinteractiontype":["updatedAt"],
"physicalmessage":["updatedAt"],"dserun":["updatedAt"],
"dserunrepdate":["updatedAt"],"dserunaccount":["updatedAt"],
"dserunrepdatesuggestion":["updatedAt"],"dserunrepdatesuggestiondetail":["updatedAt"],
"repaccountassignment":["updatedAt"],"account_dse":["updatedAt"],
"account_cs":["LastModifiedDate"],"facility":["updatedAt"],
"rep":["updatedAt"],"replocation":["updatedAt"],
"repteam":["updatedAt"],"akt_replicense_arc":["updatedAt"],
"approved_document_vod__c":["LastModifiedDate"],"accountproduct":["updatedAt"],
"strategytarget":["updatedAt"],"targetsperiod":["updatedAt"],
"targetinglevel":["updatedAt"],"repproductauthorization":["updatedAt"]
}


from pyspark.sql import Window

def deDup(df,partitionColumns,orderByColumns):
    w1=Window.partitionBy(*partitionColumns).orderBy(F.desc(*orderByColumns))
    df=df.withColumn("filter_duplicates",F.row_number().over(w1)).filter("filter_duplicates=1").select(["*"])
    df=df.drop(df.filter_duplicates)
    return df 
    
#partitionColumns=["interactionId","repId"]
#orderByColumns=['updatedAt']
#print(interaction.select("interactionId").count())
interaction=deDup(interaction,partitionColumns,orderByColumns)
#print(interaction.select("interactionId").count())