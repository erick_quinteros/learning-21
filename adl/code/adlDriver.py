from pyspark import SparkConf
from pyspark.context import SparkContext
from awsglue.context import GlueContext
import pandas as pd
#from delta.tables import *
from dataLoading import Data_loading
from awsglue.utils import getResolvedOptions
from utils import Utils
from initRecordClass import InitRecordClass
from dseRecordClassTrans import DseRecordClassTrans
from stRecordClassTransCri import StRecordClassTransCri
from finalDataTrans import FinalDataTrans
from suggestionsProductTrans import SuggestionsProductTrans
from adlLogger import create_logger, copy_log_file_to_s3
import os
from datetime import datetime
import json
import sys
import logging


if __name__ == '__main__':
    #  Getting Glue, Spark, Sql context and setting conf for delta
    now = datetime.now()
    date_time = now.strftime("%Y-%m-%d")
    print("The script is running on ", date_time)
    # create a glue Context and connect that with spark
    conf = SparkConf(loadDefaults=True)
    conf.set("spark.delta.logStore.class", "org.apache.spark.sql.delta.storage.S3SingleDriverLogStore")
    conf.set("spark.sql.caseSensitive", "true")
    conf.set("spark.sql.execution.arrow.enabled", "true")
    # conf.set("spark.jars.packages", "io.delta:delta-core_2.12:0.7.0")
    # conf.set("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension")
    # conf.set("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog")
        #conf.set("spark.sql.shuffle.partitions",500)
    sc = SparkContext.getOrCreate(conf = conf)
    glueContext = GlueContext(sc)
    spark = glueContext.spark_session
    sc.addPyFile("s3://aktana-bdp-si/adl/common/lib/delta-core_2.11-0.5.0.jar")
    from delta.tables import DeltaTable
    ################################ Arguments / parameter ####################################################
    # get params from Config.json
    with open('config.json', 'r') as f:
        config = json.load(f)

    if  len(sys.argv) > 1:
        args = getResolvedOptions(sys.argv, ['customer','environment','rptS3Location','s3CustomerAdl','mappingLocation'])
        customer = args['customer']
        s3_destination = args['s3CustomerAdl']
        s3_map_csv = args['mappingLocation'] + "data/mapping.csv"
        s3_load_csv = args['mappingLocation'] + "data/loading.csv"
        source_s3_location = args['rptS3Location'] + "data/archive_rds/"
        environment = args['environment']
        level = logging.DEBUG
    else:
        customer = config["customer"]
        level = logging.DEBUG
        s3_destination = config['s3_destination']
        s3_map_csv = config['s3_map_csv']
        s3_load_csv = config['s3_load_csv']
        source_s3_location = config['source_s3_location']
        environment = config['environment']
        target = config['strategy_target']
        # hasSpark = config['hasSpark'] == 'true'

    batch = config['batch'] == 'true'
    archive_folder = config['archive_folder']
    tables = config['tables']
    bronze_tables = config['bronze_tables']

    # setup logger
    adl_logger_name = "ADL_DEV_LOGGER"
    timestamp = datetime.now().strftime("%Y%m%d%H%M%S")
    script_path = os.path.realpath(__file__)
    script_dir = os.path.dirname(script_path)
    project_dir = os.path.dirname(script_dir)
    local_log_path = project_dir + 'adl_log_' + timestamp + '.txt'
    logger = create_logger(logger_name=adl_logger_name, log_file_path=local_log_path, log_level=level)
    logger.info("Successfully set logger")

    try:
        utils = Utils(glueContext, customer)
        tables_in_source = utils.getArchiveRdsTables(s3_destination + "data/bronze/")
        if len(tables_in_source) > 0:
            batch = False
            print("Versioning run")
        else:
            batch = True
            print("Batch run")
        # version the data mapping and loading files
        df_load_pandas = utils.data_load_csv(glueContext, s3_load_csv)
        df_map_pandas = utils.data_load_csv(glueContext, s3_map_csv)
        df_map_spark = spark.createDataFrame(df_map_pandas)
        utils.df_to_delta(df_map_spark, s3_destination + "config/deltaMapping")
        logger.info("The data mapping is versioned in Delta at "+s3_destination+"config/")
        df_load_spark = spark.createDataFrame(df_load_pandas)
        utils.df_to_delta(df_load_spark, s3_destination + "config/deltaLoading")
        logger.info("The data loading is versioned in Delta at "+s3_destination+"config/")


        # load data from rpt S3 source and version in destination S3
        dataLoader = Data_loading(glueContext, customer,environment, df_load_pandas,df_map_pandas, batch, DeltaTable)
        dataLoader.run(source_s3_location, s3_destination,archive_folder, bronze_tables, tables)
        logger.info("################################# Bronze tables written ####################################")
        # #
        # target = "no"
        # sugProdTrans = SuggestionsProductTrans(glueContext,customer, s3_destination)
        # sugProdTrans.run()
        # logger.info("################################# Final product and suggestions tables written ####################################")
        # initRecordClass = InitRecordClass(glueContext, customer, s3_destination)
        # initRecordClass.run()
        # logger.info("################################# Initial recordclass table written ####################################")
        #
        # dseRecordClassTrans = DseRecordClassTrans(glueContext, customer, s3_destination)
        # dseRecordClassTrans.run()
        # logger.info("################################# DSE tables written ####################################")
        # #
        # stRecordClassTransCri = StRecordClassTransCri(glueContext, customer, s3_destination, target)
        # stRecordClassTransCri.run()
        # logger.info("################################# ST tables written ####################################")
        # #
        # finalDataTrans = FinalDataTrans(glueContext, customer, s3_destination, target)
        # finalDataTrans.run()
        # logger.info("################################# final dataset table written ####################################")

    except Exception as e:
        logger.error("Error when running adlDriver: {}".format(e))

    finally:
        s3_log_path = s3_destination + 'logs/adl_log_' + timestamp + '.txt'
        copy_log_file_to_s3(local_log_path, s3_log_path)





