
from pyspark.context import SparkContext
import string
from utils import Utils
from pyspark.sql.types import *
import logging
import pandas as pd
from pyspark.sql import Window
from pyspark.sql.functions import *


logger = None
DSERUN_TABLES = ["dserun","dserunrepdate","dserunrepdatesuggestion","dserunrepdatesuggestiondetail"]

class Data_loading:
    def __init__(self, glue_context, customer, environment, df_load_pandas,df_map_pandas, batch, DeltaTable):
        self.glueContext = glue_context
        self.customer = customer
        self.environment = environment
        self.utils = Utils(glue_context, customer)
        self.spark = glue_context.spark_session
        self.df_map_pandas = df_map_pandas
        self.df_load_pandas = df_load_pandas
        self.batch = batch
        self.lastDate = ""
        self.DeltaTable = DeltaTable
        self.target = True
        if not self.batch:
            self.condition = {
                "dse_score":"dse_score.runId = updates.runId and  dse_score.suggestionReferenceId = updates.suggestionReferenceId"
                ,"strategy_target":"strategy_target.strategyTargetId = updates.strategyTargetId"
                , "rep_account_assignment": "rep_account_assignment.repId = updates.repId and rep_account_assignment.accountId = updates.accountId"
                , "rep_product_assignment": "rep_product_assignment.repId = updates.repId and rep_product_assignment.productId = updates.productId"
                , "rep_product_authorization": "rep_product_authorization.repId = updates.repId and rep_product_authorization.productId = updates.productId "
                , "account": "account.accountId = updates.accountId"
                , "approved_document": "approved_document.Id = updates.Id"
                , "rep_team_rep": "rep_team_rep.repTeamId = updates.repTeamId and rep_team_rep.repId = updates.repId"
                , "rep": "rep.repId = updates.repId"
                , "account_product": "account_product.productId = updates.productId and account_product.accountId = updates.accountId"
            }
        self.partitionColumns = {"interaction": ["interactionId", "repId"],
                            "interactiontype": ["interactionTypeId"], "repactiontype": ["repActionTypeId"],
                            "call2_vod__c": ["Id"], "interactionaccount": ["interactionId", "accountId"],
                            "interactionproduct": ["interactionId", "productId"], "messagetopic": ["messageTopicId"],
                            "message": ["messageId", "messageChannelId"], "sent_email_vod__c": ["Id"],
                            "product": ["productId"], "productinteractiontype": ["productInteractionTypeId"],
                            "physicalmessage": ["physicalMessageId"], "dserun": ["runId"],"sparkdserun": ["runUID"],
                            "dserunrepdate": ["runRepDateId"],
                            "sparkdserunrepdate": ["runRepDateId"],
                            "dserunrepdatesuggestion": ["runRepDateSuggestionId"],
                            "sparkdserunrepdatesuggestion": ["runRepDateSuggestionId"],
                            "dserunrepdatesuggestiondetail": ["runRepDateSuggestionDetailId"],
                            "sparkdserunrepdatesuggestiondetail": ["runRepDateSuggestionDetailId"],
                            "repaccountassignment": ["repId", "accountId"], "account_dse": ["accountId"],
                            "account_cs": ["Id"], "facility": ["facilityId"],
                            "rep": ["repId"], "replocation": ["repId"],
                            "repteam": ["repTeamId"], "akt_replicense_arc": ["cluster"],
                            "approved_document_vod__c": ["Id"], "accountproduct": ["accountId", "productId"],
                            "strategytarget": ["strategyTargetId"], "targetsperiod": ["targetsPeriodId"],
                            "targetinglevel": ["targetingLevelId"],
                            "repproductauthorization": ["repProductAuthorizationId"],
                            "rpt_suggestion_delivered_stg": ["interactionId", "runRepDateSuggestionId", "externalId", "accountId", "repId", "productId"]
                            }

        self.orderByColumns = {"interaction": ["updatedAt"],
                          "interactiontype": ["updatedAt"], "repactiontype": ["updatedAt"],
                          "call2_vod__c": ["LastModifiedDate"], "interactionaccount": ["updatedAt"],
                          "interactionproduct": ["updatedAt"], "messagetopic": ["updatedAt"],
                          "message": ["updatedAt"], "sent_email_vod__c": ["LastModifiedDate"],
                          "product": ["updatedAt"], "productinteractiontype": ["updatedAt"],
                          "physicalmessage": ["updatedAt"], "dserun": ["updatedAt"],
                          "dserunrepdate": ["updatedAt"], "dserunaccount": ["updatedAt"],
                          "dserunrepdatesuggestion": ["updatedAt"], "dserunrepdatesuggestiondetail": ["updatedAt"],
                          "repaccountassignment": ["updatedAt"], "account_dse": ["updatedAt"],
                          "account_cs": ["LastModifiedDate"], "facility": ["updatedAt"],
                          "rep": ["updatedAt"], "replocation": ["updatedAt"],
                          "repteam": ["updatedAt"], "akt_replicense_arc": ["updatedAt"],
                          "approved_document_vod__c": ["LastModifiedDate"], "accountproduct": ["updatedAt"],
                          "strategytarget": ["updatedAt"], "targetsperiod": ["updatedAt"],
                          "targetinglevel": ["updatedAt"], "repproductauthorization": ["updatedAt"],
                          "rpt_suggestion_delivered_stg": ["updatedAt"]
                          }
        global logger
        logger = logging.getLogger("ADL_DEV_LOGGER")

    def sql_to_template(self, sqlcommand, values):
        '''
        Templated sqlstring to be replaced by the values
        :param sqlcommand:
        :param values:
        :return:
        '''

        template = string.Template('%s' % (sqlcommand))
        sqlstr = template.substitute(values);
        sqlstr = sqlstr.replace('\n', ' ');
        sqlstr = sqlstr.strip()
        return sqlstr




    def get_column_mapping(self, df_map_pandas, values_database):
        '''
        Reads mapping dictionary and populates the mapping values for customer
        :param df_map_pandas: mapping dictionary from column mapping file
        :param values_database: holds the mapping values for customer
        :param customer: actual customer from config file
        :return:  mapping values for customer
        '''
        for index, row in df_map_pandas.iterrows():
            if row['Customer_name'] == self.customer:
                if len(row['Masked_name']) != 0:
                    name_split = row['Customer_attr_watermark'].split(' ')
                    name_split.append(name_split.pop())
                    #print(row['Masked_name'], ' '.join(each for each in name_split))
                    values_database[row['Masked_name']] = ' '.join(each for each in name_split)

        return values_database


    def preparesql(self, df_load_pandas, values_database, tablename):
        '''

        :param date_time:
        :param region:
        :param customer:
        :param environment:
        :param glueContext:
        :param df_load_pandas:
        :param values_database:
        :param tablename:
        :return:
        '''
        partition_columns_list = []
        for index, row in df_load_pandas.iterrows():
            # print(row['table_name'],tablename)
            if row['table_name'] == tablename:
                delta_col = row['delta_column']
                partition_columns_list = row['parition_keys_columns'].split(',')
                logger.info("tablename is matched --> " + tablename)
                values_database['otherfilter'] = row['full']
                logger.info("Below is the Sql Query for table Name: "  + tablename)
                sql_query = self.sql_to_template(row['sql_query'], values_database)
                logger.info(sql_query + " " + ' '.join([str(elem) for elem in partition_columns_list]))
                return sql_query, partition_columns_list



    def deDup(self, df, partitionColumns, orderByColumns):
        '''
        :param df:
        :param partitionColumns:
        :param orderByColumns:
        :return:
        '''
        w1 = Window.partitionBy(*partitionColumns).orderBy(desc(*orderByColumns))
        df = df.withColumn("filter_duplicates", row_number().over(w1)).filter("filter_duplicates=1").select(["*"])
        df = df.drop(df.filter_duplicates)
        return df


        # derive a strategy for empty tables from reporting, similar to null columns in the mapping
    def load_tables_from_s3(self,tables, source_s3_location, archive_folder):
        '''
        :param tables:
        :param source_s3_location:
        :param environment:
        :param archive_folder:
        :return:
        '''
        if self.batch and self.environment == "qa":
            testDate = self.spark.read.parquet(source_s3_location + "rpt_suggestion_delivered_stg")\
                                            .agg({"suggestedDate": "max"}).collect()[0][0] - pd.DateOffset(months=2)
            logger.info(["Test date is ", testDate])
        tables_found = []
        tables_not_found = []
        # get tables in source s3
        logger.info("start getting table names from archive")
        tables_in_source = self.utils.getArchiveRdsTables(source_s3_location)
        logger.info("successfully got tables")
        for table in tables:
            path = None
            sparkPath = None
            # if table == 'suggestions':
            #     table = 'rpt_suggestion_delivered_stg'
            #     path = 's3://aktana-bdp-adl/'+self.customer+'/prod/suggestions/'
            if table in DSERUN_TABLES:
                sparkTable = 'spark' + table
                if self.batch:
                    if table in tables_in_source:
                        path = source_s3_location + table
                    if sparkTable in tables_in_source:
                        # if no dserun*, just use 'path' to read sparkdserun*
                        if not path:
                            path = source_s3_location + sparkTable
                        else:
                            sparkPath = source_s3_location + sparkTable
                else:
                    if sparkTable in tables_in_source:
                        path = source_s3_location + sparkTable
                    else:
                        path = source_s3_location + table
            else:
                path = source_s3_location + table

            logger.info(path)
            logger.info("Loading the table from Parquet")
            df = self.utils.data_load_parquet(self.glueContext, path)
            if not df.rdd.isEmpty():
                df = self.deDup(df, self.partitionColumns[table], self.orderByColumns[table])
            if sparkPath:
                sparkDF = self.utils.data_load_parquet(self.glueContext, sparkPath)
                if self.batch and self.environment == "qa":
                    sparkDF = sparkDF.filter(sparkDF.updatedAt < testDate)
                if len(sparkDF.columns) != len(df.columns):
                    common_cols = list(set(sparkDF.columns).intersection(set(df.columns)))
                    df = df.select(common_cols)
                    sparkDF = sparkDF.select(common_cols)
                df = df.union(sparkDF)

            if df.rdd.isEmpty():
                logger.info([table, "Table does not exist"])
                tables_not_found.append(table)
            else:
                if (not self.batch and table not in ["interactiontype", "repactiontype", "message", "sent_email_vod__c",
                                                     "messagetopic",
                                                     "interactionaccount", "interactionproduct", "call2_vod__c",
                                                     "approved_document_vod__c", "account_cs", ]):
                    df = df.filter(df.updatedAt >= self.lastDate)

                if (self.batch and self.environment == "qa"):
                    if table == "call2_vod__c":
                        df = df.filter(df.LastModifiedDate < testDate)
                    elif table == "approved_document_vod__c":
                        df = df.filter(df.Document_Last_Mod_DateTime_vod__c < testDate)
                    elif table == "account_cs":
                        df = df.filter(df.ATL_Last_Update_Date_Time_vod__c < testDate)
                    elif table == "sent_email_vod__c":
                        df = df.filter(df.LastModifiedDate < testDate)
                    else:
                        df = df.filter(df.updatedAt < testDate)

                df = df.repartition(40)
                df.createOrReplaceTempView(table)
                logger.info([table, " view has been created"])
                tables_found.append(table)
        return tables_found, tables_not_found

    def upsert(self, tablename, result_sql, s3_destination):
        try:
            if tablename == "emails":
                deltaTable = self.DeltaTable.forPath(self.spark, s3_destination + "data/bronze/" + tablename)
                updates = self.spark.sql(result_sql)
                deltaTable.alias("emails").merge(
                    updates.alias("updates"),
                    "emails.interactionId = updates.interactionId and emails.repId = updates.repId and emails.accountId = updates.accountId and emails.productId = updates.productId") \
                    .whenMatchedUpdate(set={"isCompleted": "updates.isCompleted"
                    , "isDeleted": "updates.isDeleted"
                    , "updatedAt": "updates.updatedAt"
                    , "Opened_vod__c": "updates.Opened_vod__c"
                    , "Open_Count_vod__c": "updates.Open_Count_vod__c"
                    , "Click_Count_vod__c": "updates.Click_Count_vod__c"
                    , "Status_vod__c": "updates.Status_vod__c"}) \
                    .whenNotMatchedInsertAll() \
                    .execute()
            elif tablename == "visits":
                deltaTable = self.DeltaTable.forPath(self.spark, s3_destination + "data/bronze/" + tablename)
                updates = self.spark.sql(result_sql)
                deltaTable.alias("visits").merge(
                    updates.alias("updates"),
                    "visits.interactionId = updates.interactionId and visits.repId = updates.repId and visits.accountId = updates.accountId and visits.productId = updates.productId") \
                    .whenMatchedUpdate(set={"isCompleted": "updates.isCompleted"
                    , "isDeleted": "updates.isDeleted"
                    , "updatedAt": "updates.updatedAt"}) \
                    .whenNotMatchedInsertAll() \
                    .execute()
            elif tablename == "product":
                deltaTable = self.DeltaTable.forPath(self.spark, s3_destination + "data/bronze/" + tablename)
                updates = self.spark.sql(result_sql)
                deltaTable.alias("product").merge(
                    updates.alias("updates"),
                    "product.interactionId = updates.interactionId and product.productId = updates.productId") \
                    .whenMatchedUpdate(set={"isCompleted": "updates.isCompleted"
                    , "updatedAt": "updates.updatedAt"
                    , "actionOrder": "updates.actionOrder"}) \
                    .whenNotMatchedInsertAll() \
                    .execute()
            elif tablename == "suggestions":
                deltaTable = self.DeltaTable.forPath(self.spark, s3_destination + "data/bronze/" + tablename)
                updates = self.spark.sql(result_sql)
                deltaTable.alias("suggestions").merge(
                    updates.alias("updates"),
                    "suggestions.suggestionReferenceId = updates.suggestionReferenceId and suggestions.suggestedDate = updates.suggestedDate and suggestions.reasonRank = updates.reasonRank") \
                    .whenMatchedUpdate(set={"reportedInteractionUID": "updates.reportedInteractionUID"
                    , "inferredInteractionUID": "updates.inferredInteractionUID"
                    , "interactionUID" : "updates.interactionUID"
                    , "interactionId": "updates.interactionId"
                    , "updatedAt": "updates.updatedAt"
                    , "actionTaken": "updates.actionTaken"
                    , "actionTaken_dt": "updates.actionTaken_dt"
                    , "isSuggestionCompleted": "updates.isSuggestionCompleted"
                    , "isCompleted": "updates.isCompleted"
                    , "isSuggestionCompletedDirect": "updates.isSuggestionCompletedDirect"
                    , "isSuggestionCompletedInfer": "updates.isSuggestionCompletedInfer"
                    , "isSuggestionDismissed" : "updates.isSuggestionDismissed"
                    , "dismissReason" :"updates.dismissReason"
                    , "dismissReasonType":"updates.dismissReasonType"
                    , "dismissReason_dt":"updates.dismissReason_dt"
                    , "isSuggestionActive":"updates.isSuggestionActive"}) \
                    .whenNotMatchedInsertAll() \
                    .execute()
            elif tablename == "strategy_target":
                if self.target:
                    deltaTable = self.DeltaTable.forPath(self.spark, s3_destination + "data/bronze/" + tablename)
                    updates = self.spark.sql(result_sql)
                    deltaTable.alias(tablename).merge(
                        updates.alias("updates"),
                        self.condition[tablename]) \
                        .whenNotMatchedInsertAll() \
                        .execute()
            else:
                deltaTable = self.DeltaTable.forPath(self.spark, s3_destination + "data/bronze/" + tablename)
                updates = self.spark.sql(result_sql)
                deltaTable.alias(tablename).merge(
                    updates.alias("updates"),
                    self.condition[tablename]) \
                    .whenNotMatchedInsertAll() \
                    .execute()
        except Exception as e:
            logger.error("Error in upsert: {}".format(e))


    def run (self, source_s3_location, s3_destination,archive_folder, bronze_tables, tables):
        if self.batch:
            self.lastDate = "batch"
        else:
            # self.lastDate = self.spark.read.parquet(s3_destination + "data/silver/final_dataset")\
            #                                 .agg({"All_effectivedate": "max"}).collect()[0][0]
            self.lastDate = self.spark.read.parquet(s3_destination + "data/bronze/emails") \
                                                .agg({"updatedAt": "max"}).collect()[0][0]
        logger.info("The data loading has been started from S3 at " + str(self.lastDate))
        tables_found, tables_not_found = self.load_tables_from_s3(tables, source_s3_location, archive_folder)
        if "strategytarget" in tables_not_found:
            self.target = False
        logger.info([tables_found, "These are the tables in memory."])
        logger.info([tables_not_found, "These are the tables not found in memory"])

        # validating the sql queries from data loading file and prep data to write them in bronze area.
        values_database = {}
        for tablename in bronze_tables:
            if (self.batch and tablename == "strategy_target" and "strategytarget" in tables_not_found):

                field = [StructField("strategyTargetId", IntegerType(), True), \
                         StructField("targetsPeriodId", IntegerType(), True),
                         StructField("targetingLevelId", IntegerType(), True),
                         StructField("facilityId", StringType(), True),
                         StructField("accountGroupId", StringType(), True),
                         StructField("accountId", IntegerType(), True),
                         StructField("repTeamId", IntegerType(), True),
                         StructField("repId", IntegerType(), True),
                         StructField("productId", IntegerType(), True),
                         StructField("messageTopicId", StringType(), True),
                         StructField("messageId", StringType(), True),
                         StructField("interactionTypeId", IntegerType(), True),
                         StructField("productInteractionTypeId", IntegerType(), True),
                         StructField("target", IntegerType(), True),
                         StructField("targetMin", IntegerType(), True),
                         StructField("targetMax", IntegerType(), True),
                         StructField("visitActionOrderMin", IntegerType(), True),
                         StructField("relativeValue", IntegerType(), True),
                         StructField("createdAt", TimestampType(), True),
                         StructField("updatedAt", TimestampType(), True)]
                schema = StructType(field)
                df = self.spark.createDataFrame(self.spark.sparkContext.emptyRDD(), schema)
                logger.info(df.count())
                df.repartition(1).write.format("delta").save(s3_destination + "data/bronze/strategy_target/")
                logger.info("empty strategy target table was created")
            elif not self.batch:
                logger.info("Starting versioning " + tablename)
                values_database = self.get_column_mapping(self.df_map_pandas, values_database)
                result_sql, partition_columns_list = self.preparesql(self.df_load_pandas, values_database, tablename)
                self.upsert(tablename,result_sql, s3_destination)
            else:
                values_database = self.get_column_mapping(self.df_map_pandas, values_database)
                result_sql, partition_columns_list = self.preparesql(self.df_load_pandas, values_database, tablename)
                if partition_columns_list[0] !="":
                    self.utils.df_topartition_delta(self.spark.sql(result_sql), partition_columns_list,
                                           s3_destination + "data/bronze/" + tablename)
                else: self.utils.df_to_delta(self.spark.sql(result_sql), s3_destination + "data/bronze/" + tablename)

            # print(spark.sql(result_sql))