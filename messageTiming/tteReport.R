##########################################################
##
##
## Aktana Machine Learning Module.
##
## Description: TTE Reporting Code
##   1. calculate best days to engage in hcp level 
##   2. calculate best days to engage in segment level 
##
## created by : wendong.zhu@aktana.com
##
## created on : 2018-08-15
##
## Copyright AKTANA (c) 2018.
##
##
##########################################################

DAYS_AFTER_SEND <- 1      # default number of days to visit after send
DAYS_AFTER_VISIT <- 1     # default number of days to send after visit
VAL_NO_DATA   <- 999      # default value representing no data
MAX_RECOMMEND_DAYS <- 60  # max days to recommend
DAYS_IN_WEEK <- 7

# TTE Reporting methods:
#   B - Covariates + Intervals
#   C - Covariates + DoW

ttePredictReport <- function(con, con_l, dynamic.model, segs, models.B, models.C, AP, ints,
                             RUN_UID, whiteList, tteParams)
{
    library(h2o)
    library(data.table)

    flog.info("Entering ttePredictReport...")

    # retrieve tte parameters
    predictAhead <- tteParams[["predictAhead"]]
    LOOKBACK_MAX <- tteParams[["LOOKBACK_MAX"]]
    predictRunDate <- tteParams[["predictRunDate"]]
    segs <- tteParams[["segs"]]
    channels <- tteParams[["channels"]]

    ints.acct <- ints[, .(accountId)]
    ints.acct <- ints.acct[!duplicated(ints.acct[, .(accountId)])]

    ints.acct$ctr <- 1
    dup <- copy(ints.acct)

    dup <- rbind(dup, ints.acct[, ctr := 2])
    
    ints.acct <- dup
    
    ints.acct[ctr == 1, type := "SEND"]
    ints.acct[ctr == 2, type := "VISIT"]
    
    setorder(ints.acct, accountId, type)
    ints.acct$ctr <- NULL

    date.back <- predictRunDate - LOOKBACK_MAX - 1

    # pick the last row in each accountId group
    eventNames <- unique(ints[['type']])
    preEventCols <- c()
    for (t in eventNames){
      preEventCols <- c(preEventCols, paste0('pre', t))
    }
    preEventCols <- preEventCols[preEventCols %in% names(dynamic.model)]
    dm.s <- subset(dynamic.model[event == "S", .SD[.N], by=accountId], select=c('accountId', 'event', preEventCols, 'preTARGET', 'pre2S', 'pre2V', 'numS', 'numV', 'date'))
    dm.v <- subset(dynamic.model[event == "V", .SD[.N], by=accountId], select=c('accountId', 'event', preEventCols, 'preTARGET', 'pre2S', 'pre2V', 'numS', 'numV', 'date'))
    
    dm <- rbind(dm.s, dm.v)
    setorder(dm, accountId, event)
  
    dm <- dm[accountId %in% whiteList]  # filering by whiteList

    dm[event == "S", type := "SEND"]
    dm[event == "V", type := "VISIT"]
    dm$event <- NULL
    
    ints <- merge(dm, ints.acct, by=c("accountId", "type"), all = T)
  
    # ints[is.na(preS), preS := LOOKBACK_MAX]
    # ints[is.na(preV), preV := LOOKBACK_MAX]
    for (t in preEventCols){
      ints[[t]][is.na(ints[[t]])] <- LOOKBACK_MAX
    }
    ints[is.na(preTARGET), preTARGET := VAL_NO_DATA]
    ints[is.na(numS), numS := 1]
    ints[is.na(numV), numV := 1]
    
    ints[is.na(pre2S), pre2S := VAL_NO_DATA]
    ints[is.na(pre2V), pre2V := VAL_NO_DATA]
    
    ints[is.na(date), date := date.back]

    ints[type == "SEND",  event := "S"]
    ints[type == "VISIT",  event := "V"]
    ints$type <- NULL
    ints$intervalOrDoW <- 1
    
    ints.bak <- copy(ints)
    dup  <- copy(ints)
    
    for(i in 1:(predictAhead-1)) {
      # Duplicate ints in order to predict future days
      # in each loop (one append), increase preS, preV, preT, etc. and date by 1
        for (j in names(ints)[-1]){
              if (stringr::str_detect(j, "pre") | j == "date" | j == "intervalOrDoW"){
                set(ints, j=j, value=ints[[j]]+1)
              }
            }
        dup <- rbind(dup, ints)
    }
    
    setorder(dup, accountId, event, date)    
    method <- "B"
    
    ## calculate scores for reporting
    scores <- tteMethod(models.B, AP, dup, RUN_UID, method)
    scores$method <- "Covariates + Intervals"

    rm(dup)
    gc()

    # now processing data for DoW method
    ints <- ints.bak[, c("accountId", "event", "date")]
    dup  <- copy(ints)
    
    for(i in 1:(DAYS_IN_WEEK - 1)) {
        # Duplicate ints in order to predict future 7 days
        # in each loop (one append), increase date by 1
        dup <- rbind(dup, ints[, c("date") := list(date+1)])
    }

    setorder(dup, accountId, event, date)    
    method <- "C"
    
    scores.C <- tteMethod(models.C, AP, dup, RUN_UID, method)
    
    scores.C$method <- "Covariates + DoW"
    scores.C[, intervalOrDoW := as.POSIXlt(scores.C$date)$wday]  # 0, 1~6 for Sun, Mon~Sat
    scores.C <- scores.C[!intervalOrDoW %in% c(0,6)]
    
    scores <- rbind(scores, scores.C)

    ## save reporting results
    saveTimingScoreReport(con, con_l, scores, tteParams)
}
