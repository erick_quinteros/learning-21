
##########################################################
##
##
## aktana-learning Install Aktana Learning Engines.
##
## description: Driver Code
## 1. analyze message opens/click likelihoods
##
## created by : marc.cohen@aktana.com
##
## created on : 2015-11-03
##
## Copyright AKTANA (c) 2015.
##
##
##########################################################

# TTE methods:
#   A - Covariates + Intervals + DoW (full model for DSE integration)
#   B - Covariates + Intervals (for reporting)
#   C - Covariates + DoW (for reporting)

METHODS <- c("A", "B", "C")  
ProductInteractionTypes <- c("SEND", "VISIT_DETAIL")
NA_VALUE <- 999

messageTiming <- function(con, con_l, tteParams)
{
    library(h2o)
    library(data.table)
    library(reticulate)
  
    use_python("/usr/local/bin/python")
  
    source_python(sprintf("%s/messageTiming/build_dynamic_model.py",homedir))
    
    flog.info("Reading configuration")
# read parameters from the configuration file
    GBMLearnRate <- tteParams[["GBMLearnRate"]]
    GBMntrees <- tteParams[["GBMntrees"]]
    RFtreeDepth <- tteParams[["RFtreeDepth"]]
    RFtreeNo <- tteParams[["RFtreeNo"]]
    AnalysisUseML <- tteParams[["AnalysisUseML"]]

    modelName <- BUILD_UID

    channels <- tteParams[["channels"]]

    EventTypes <- tteParams[["EventTypes"]]

    prods <- tteParams[["prods"]]
    if(length(prods)==0)prods <- ""  # this is a kludge to fix defect in getConfigurationValue
    if(prods=="") { prods <- "accountId" } else { prods <- c("accountId",prods) }

    prods <- unique(prods) # need to remove duplicated predictors from AccountProduct table

    pNameUID <- tteParams[["pNameUID"]]
    pName <- products[externalId==pNameUID]$productName
    
    predictRunDate <- tteParams[["predictRunDate"]]
    LOOKBACK <- tteParams[["LOOKBACK"]]

    target <- tteParams[["target"]]
    segs <- tteParams[["segs"]]

    predictorNamesAPColMap <- list()
    if (length(prods)!=0) {
      # remove extra unnessary string <repAccountAttribute\:> added to learning.properties <LE_MS_addPredictorsFromAccountProduct> by DSE API & and save its original mapping for UI display
      predictorNamesAPColMap <- as.list(prods)
      prods <- gsub("repAccountAttribute\\:", "", prods, fixed=TRUE)
      predictorNamesAPColMap <- setNames(predictorNamesAPColMap, as.vector(prods))
      predictorNamesAPColMap <- list2env(predictorNamesAPColMap) # convert to hash for faster access
      
      # remove predictors not in AP
      prods <- prods[prods %in% colnames(accountProduct)]
    }
        
    flog.info("Analyzing account product data")
##### first prepare the accountProduct table and build design
    AP <- accountProduct[, prods, with=F]
    print(names(AP))
    AP <- AP[,sapply(AP,function(x)length(unique(x))>1),with=F]
    str(AP)
    tt<-data.table(sapply(AP,class))
    print(tt)
    tt$names<-names(AP)
    chrV <- tt[V1=="character"]
    chrV <- chrV[names!="productName"]
    numV <- tt[V1!="character"]

    flog.info("Starting to analyze AP variable classes")
    print(chrV$names)
    print("AP before replacing na")
    print(names(AP))
    for(i in chrV$names)                                          # for the catgorical variables, assing missing values with 'NA'
    {                                                          
        eval(parse(text=sprintf("AP[is.na(%s),%s:='NA']",i,i)))
        eval(parse(text=sprintf("AP[(%s)=='', %s:='NA']",i,i)))
    }


    for (i in numV$names[-1]){
      AP[[i]][is.na(AP[[i]])] <- NA_VALUE
    }
    print("AP after replacing na")
    print(names(AP))
    # only pick up first observation of same accountId
    AP <- subset(AP, !duplicated(accountId))
    print("AP after subsetting")
    print(names(AP))
    
##### clean up memory
    rm(accountProduct,envir=parent.frame())                       # delete the accountProduct table since it's big to free up some memory

##### done with static design

    flog.info("Build Design Matrix")

#### next prepare to build design for dynamic predictors

    ints <- interactions

    allTypes <- c(ProductInteractionTypes, EventTypes)
    ints <- ints[type %in% allTypes]                         # filtering interactions using allTypes
    ints <- unique(ints)                                     # remove duplicated rows in interactions
    
    flog.info("Size of ints %s %s",dim(ints)[1],dim(ints)[2])

    # filter training data using LOOKBACK
    ints <- ints[date >= (predictRunDate - LOOKBACK)]
    
    if (target == "OPEN") {
        flog.info("Using target: OPEN")
        ints[type %in% c("RTE_OPEN", "EMAILOPEN"), type:="TARGET"] # target: pfizerus data (OPEN); pfizerja data (EMAILOPEN)
        ints <- ints[type != "RTE_CLICK"]                    # remove RTE_CLICK interactions
    } else if (target == "CLICK") {
        flog.info("Using target: CLICK")
        ints[type %in% c("RTE_CLICK"), type:="TARGET"]       # target for pfizerus data (OPEN | CLICK)
        ints <- ints[!type %in% c("RTE_OPEN", "EMAILOPEN")]  # remove RTE_OPEN|EMAILOPEN interactions
    } else {
        flog.info("Unknown target. Default to OPEN")
        ints[type %in% c("RTE_OPEN", "EMAILOPEN"), type:="TARGET"]
    }

    ints[type=="VISIT_DETAIL",type:="VISIT"]
    ints[type=="APPOINTMENT_DETAIL",type:="APPOINTMENT"]
    
    # check number of OPEN|CLICK interactions
    if (nrow(ints[type=="TARGET"]) == 0) {
        flog.warn("No target (OPEN or CLICK) found in interactions! Return.")
        status <- 90     # user-defined error code
        return (status)
    }

    accts <- unique(ints$accountId) 

    setorder(ints, accountId, date)     # order by accountId and date
    
    # pick up relavant columns. Note that account could have multi visits/sends on same date for different products
    ints <- ints[, c("accountId", "type", "date")] 
    
    flog.info("Start building dynamic model ......")
    
    # build dynamic model using interaction data
    dynamic.model <- build_dynamic_model(ints) 
    dynamic.model <- setDT(dynamic.model)    # convert dateframe to data.table

    flog.info("Finished building dynamic model.")
    
    dynamic.model <- dynamic.model[TARGET != -1]  # remove noisy data
    
    if (!"EVENT" %in% channels) {
        # no events to listen to, remove the features in dymanic model
        col2remove = vector()
        for (i in EventTypes){
            if (! i %in% c('SEND','VISIT','TARGET')){
                col2remove <- c(col2remove, paste('pre',i,sep=""))
            }
        }
        dynamic.model[, (col2remove) := NULL]
    } else {
        for (i in EventTypes){
            if (nrow(ints[type==i]) == 0){
                dynamic.model[, c(i) := NULL]
            }
        }
    }
    
    dynamic.model[, c("numT") := NULL]
    
    fwrite(dynamic.model, sprintf("%s/dynamic.model.csv", runDir), nThread = getDTthreads())
    
    dynamic.model[, c("date") := NULL]

    dynamic.model.dow <- dynamic.model[, c("accountId", "event", "dow", "TARGET")]

    #### Method A for predictions: Covariates + days to previous send & vist + Day of Week
    if (ncol(AP) > 1) 
      # allModel.A <- merge(AP, dynamic.model, by="accountId", all.x = T, allow.cartesian = TRUE)
      allModel.A <- merge(AP, dynamic.model, by="accountId", all.x = T)
    else
      allModel.A <- dynamic.model

    allModel.A <- allModel.A[accountId %in% accts]

    allModel.A[is.na(allModel.A)] <- 0
    allModel.A[, c("accountId") := NULL]
    print("allModel.A before fed to model")
    print(names(allModel.A))
    # print(length(unique(allModel.A[['sampleAO_akt']])))
    flog.info("Finished building allModel.A")
    
    #### Method B for reporting (no DoW feature): Covariates + days to previous send & vist
    dynamic.model[, c("dow") := NULL]

    if (ncol(AP) > 1)
      # allModel.B <- merge(AP, dynamic.model, by="accountId", all.x = T,allow.cartesian = TRUE)
      allModel.B <- merge(AP, dynamic.model, by="accountId", all.x = T)
    else 
      allModel.B <- dynamic.model    
    
    allModel.B <- allModel.B[accountId %in% accts]

    flog.info("Finished building allModel.B")

    allModel.B[is.na(allModel.B)] <- 0
    allModel.B[, c("accountId") := NULL]

    #### Method C for DoW output
    if (ncol(AP) > 1)
      # allModel.C <- merge(AP, dynamic.model.dow, by="accountId", all.x = T,allow.cartesian = TRUE)
      allModel.C <- merge(AP, dynamic.model.dow, by="accountId", all.x = T)
    else 
      allModel.C <- dynamic.model.dow
    
    allModel.C <- allModel.C[accountId %in% accts]

    allModel.C[is.na(allModel.C)] <- 0
    allModel.C[, c("accountId") := NULL]

    #### loop through methods to build models
    for (m in METHODS) 
    {
        print(m)
        flog.info("looping through method-%s...", m)
        
        m.name <- sprintf("allModel.%s", m)  # pick up the right allModel
        allModel <- eval(parse(text=m.name)) # reference (assign) object by character string; same as: assign("allModel", get(m.name)) 
        
    #### common for all methods
        allCols <- 1:(dim(allModel)[2])

        targetName <- "TARGET"
        i <- which(names(allModel)==targetName)

        flog.info("Number of design matrix records: %s",dim(allModel)[1])

    ## additional info to print (but not working for regression; only for classification)
    #    ttt <- melt(allModel,id.vars="TARGET")
    #    ttt <- ttt[value>0]
    #    uni<-unique(ttt[,totals:=sum(value),by=c("TARGET","variable")])
    #    uni$value <- NULL
    #    tbl <- data.table(dcast(uni,variable~TARGET))
    #    names(tbl) <- c("covariate","Target_0","Target_1")
    #    tbl[is.na(tbl)] <- 0
    #    tbl[,total:=Target_0+Target_1]
    #    tbl[,c("prob_Open","prob_Not"):=list(Target_1/total,Target_0/total)]
    #    print(tbl)
        
        flog.info("Number of design matrix records: %s > 20?",dim(allModel)[1])

        # Requirements for training model:
        #   1. number of design matrix records > 20
        #   2. number of target (Open/Click) > 5
        #   3. number of unque target values >= 2

        if(dim(allModel)[1]>20)
        {
            flog.info("Target %s > 5 in input data",targetName)
            flog.info("Counts %s need to be > 5 to continue",sum(allModel[,targetName,with=F]))
            if(sum(allModel[,targetName,with=F])>5)
            {
                if(dim(unique(allModel[,targetName,with=F]))[1]<2)
                {
                    flog.info("Only 1 type of target value = %s",allModel[1,targetName,with=F])
                    next
                }
                                            # next line needed for sends, opens, and account attributes to be included as predictors
                xVars <- allCols

                # convert allModel to h2o object
                fwrite(allModel, "/tmp/allModel.csv", col.names=FALSE, nThread = getDTthreads())
                tmp.hex = h2o.importFile(path="/tmp/allModel.csv", destination_frame="tmp.hex", header=FALSE, col.names=names(allModel))
                
                flog.info("Check tmp.hex rows : %s", dim(tmp.hex)[1])
                flog.info("Start training model-%s ......", m)

                if(AnalysisUseML=="GLM")
                {
                    rf.hex <- h2o.glm(x=xVars,y=i,training_frame=tmp.hex,lambda=0)

                    print(sprintf("Message: %s",targetName))
                    print(rf.hex)

                    output <- rf.hex@model$coefficients_table
                    output <- data.table(output$names, output$coefficients)
                }
                else if(AnalysisUseML=="RF")
                {
                    cols <- data.table(sapply(allModel,class))
                    cols$names<-names(allModel)
                    chrV <- cols[V1=="character"]
                    
                    # for(col in chrV$names)   # for the catgorical variables
                    # { 
                    #     print(sprintf("handling factor: %s, class: %s", col, class(tmp.hex[col])))
                    #     tmp.hex[col] <- as.factor((tmp.hex[col]))
                    # }
                    
                    encoding = "OneHotExplicit"    # for categorical_encoding param
                      
                    rf.hex <- h2o.randomForest(x=xVars,y=i,training_frame=tmp.hex,ntrees=RFtreeNo,max_depth=RFtreeDepth,nfolds = 2, ignore_const_cols=FALSE)
                    
                    print(sprintf("Message: %s",targetName))
                    print(rf.hex)

                    output <- rf.hex@model$variable_importances
                    output <- data.table(output$variable, output$scaled_importance)
                }
                else if(AnalysisUseML=="GBM")
                {
                    cols <- data.table(sapply(allModel,class))
                    cols$names<-names(allModel)
                    chrV <- cols[V1=="character"]
                  
                    for(col in chrV$names)   # for the catgorical variables
                    { 
                        tmp.hex[col] <- as.factor((tmp.hex[col]))
                    }
                      
                    rf.hex <- h2o.gbm(x=xVars, y=i, training_frame=tmp.hex, learn_rate=GBMLearnRate, ntrees=GBMntrees, nfolds=2)
                    
                    print(sprintf("Message: %s",targetName))
                    print(rf.hex)

                    output <- rf.hex@model$variable_importances
                    output <- data.table(output$variable, output$scaled_importance)
                }            
                else { flog.error("Unknown ML Method") }

                rm(tmp.hex)
                gc(); gc(); gc()

                flog.info("Finished training model-%s.", m)
                setnames(output, c("V1","V2"), c("names", targetName))

                mName <- h2o.saveModel(object=rf.hex,path=sprintf("%s/models_%s_%s",runDir,m,runStamp),force=T)

                perf <- h2o.performance(rf.hex)
                mse  <- h2o.mse(perf)
                rmse <- h2o.rmse(perf)
                mae  <- h2o.mae(perf)
                r2   <- h2o.r2(perf)
                
                cm <- NULL
                cm[1] <- mse
                cm[2] <- rmse
                cm[3] <- r2

                names(cm) <- c("MSE", "RMSE", "r2")

                models <- cbind(data.table(t(cm)),data.table(target=targetName,modelName=gsub(getwd(),".",mName)))
            }
        }

        sheetName <- sprintf("%s_%s", pName, m)
        addWorksheet(WORKBOOK, sheetName)
        writeData(WORKBOOK,sheetName,output,startRow=1)

        tName <- sprintf("%s_%s_reference", pName, m)
        addWorksheet(WORKBOOK,tName)
        writeData(WORKBOOK,tName,models,startRow=1)
        
        flog.info("finished training model %s.", m)
    }

    rm(allModel.A)
    rm(allModel.B)
    rm(allModel.C)
    rm(allModel)
    rm(ints)
    gc(); gc(); gc()
        
    return (0)
}
