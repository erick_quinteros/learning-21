#!/bin/bash

# Set the learning home location if not set already (use absolute path)
if [ -z "$LEARNING_HOME" ] ; then
   PRG="$0"
   LEARNING_HOME=`dirname "$PRG"`/..
   HOME_DIR_PARAM='homedir=\"'"$LEARNING_HOME"'\"'
fi

RCMD="Rscript"
RSCRIPT="$LEARNING_HOME/common/unitTest/dbSetup.R"
RARGS="$HOME_DIR_PARAM"

RFULLCMD="$RCMD $RSCRIPT $RARGS"
eval $RFULLCMD

rc=$?
echo $rc

case $rc in
  0) echo "done set up unit test DB";;
  *) echo "failed set up unit test DB";;
esac

exit $rc